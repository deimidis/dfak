---
layout: page
title: سكايپ
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/skype.md
parent: /ar/
published: true
---

فحوى الاتّصال و&nbsp;واقعة تواصلك مع الطّرف الآخر قد تكون معلومة للحكومة أو لجهات إنفاذ القانون.

