---
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: كل الأيام من 09:00 إلى 17:00 UTC+5 (PST)
response_time: 56 ساعة
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

مؤسسة الحقوق الرقميَّة (DRF) مؤسَّسة بحثية و&nbsp;حشدية أهليّة مُسجَّلة مقرُّها باكستان تأسَّست سنة 2012، و&nbsp;هي تركّز على تقنية المعلومات و&nbsp;الاتّصالات لدعم حقوق الإنسان و&nbsp;الشمول و&nbsp;السيرورات الديموقراطية و&nbsp;الحوكمة الرقميَّة. تعمل DRF على قضايا حريّة التعبير و&nbsp;الخصوصيَّة و&nbsp;حماية البيانات و&nbsp;مناهضة العنف السبراني ضد النساء.

