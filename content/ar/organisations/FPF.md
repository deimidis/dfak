---
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: من الاثنين إلى الجمعة أثناء ساعات الدوام، بتوقيت شرقي الولايات المتّحدّة
response_time: يوم
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

مؤسسة حرّيّة الصحافة مؤسّسة أهليّة غير هادفة للربح تحمي و&nbsp;تدافع و&nbsp;تقوّي صحافة الصالح العام في القرن الحادي و&nbsp;العشرين. و&nbsp;المؤسسة تدرّب الكافّة، من المؤسسات الصحافية الكبرى إلى الصحافيين المستقلّين، على أدوات متنوّعة للأمان و&nbsp;الخصوصيّة و&nbsp;تقنيات حماية أنفسهم على نحو أفضل، و&nbsp;مصادرَهم و&nbsp;مؤسّساتِهم.