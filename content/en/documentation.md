---
layout: sidebar.pug
title: "Documenting Digital Attacks"
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: en
summary: "Tips to document different kinds of digital emergencies."
date: 2023-05
permalink: /en/documentation/
parent: Home
sidebar: >
  <h3>Read more about how to document digital emergencies:</h3>
  <ul>
    <li><a href="https://acoso.online/barbados/report-to-law-enforcement/#general-tips">Acoso.online general tips, report a case and how to keep evidence</a></li>
    <li><a href="https://www.techsafety.org/documentationtips">National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking</a></li>
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/english">Chayn: How to Build a Domestic Abuse Case Without a Lawyer</a></li>
      </ul>
---

#  Documenting Digital Attacks

Documenting digital attacks can serve a number of purposes. Recording what is happening in an attack can help you:

* understand the situation better and take action to protect yourself.
* provide evidence that technical or legal helpers will need in order to support you.
* understand the pattern of attacks better, and identify additional threats.
* support your own peace of mind and understand how the attacks impact you emotionally.

Whether you are documenting for your own understanding, or to seek legal or technical support, a structured record of attacks can help you better understand:

* The scale and scope of the attack: whether it is a one-time attack or a repeated pattern, targeted at you or a larger group of people.
* The attacker’s habits: whether they are isolated or part of an organized group, using information available on the internet or accessing your private personal information, the tactics and technologies they may use, etc.
* Whether the responses you plan to implement will lessen physical or online threats to you, or make them worse.

As you begin gathering information, consider the following:

* Are you primarily documenting for your own peace of mind and/or to understand what is happening?
* Do you want to seek technical assistance to stop the attacks? Please consider visiting the [Digital First Aid Kit support section](../support/) to see which CiviCERT organisations can help you.
* Do you also want to pursue a legal case?

Depending on your goals, you may need to document the attacks in slightly different ways. If you need evidence for a legal case, you will need to gather specific kinds of evidence that are legally admissible in court, like phone numbers and timestamps. Technical assistants will need to see additional evidence, like web addresses (URLs), user names, and screenshots. See the sections below for more details on the information you should capture for each of these possible paths.

## Protect your mental and emotional well-being while documenting

Dealing with digital attacks is stressful, and your physical and emotional well-being should be your first priority, no matter what kind of documentation you want to pursue. Documentation should not add to your stress. Consider the following as you begin to document an attack:

* There are many ways to document and record. Choose the ones you are comfortable with and support your goals for documentation.
* Documenting digital attacks can be emotionally demanding and trigger traumatic memories. If you feel overwhelmed, consider delegating the documentation to a colleague, friend, other trusted person, or monitoring group who can keep documentation of the attacks updated without being re-traumatized.
*  If the attack is on a collective, consider rotating documentation responsibilities among different people.
* Consent is essential. Documentation should continue as long as the person or group involved agrees.
* [Here are some more tips on how to get your friends or family to help you.](https://onlineharassmentfieldmanual.pen.org/guidelines-for-talking-to-friends-and-loved-ones/)

Documenting digital attacks or gender-based violence online means gathering information on what you or your collective are facing. This documentation does not only have to be rational and technical. It can also help you process the violence by recording how you feel about each attack in text, image, audio, video, or even artistic expression. We recommend doing this offline and taking the steps described below to protect your privacy while processing these feelings.

## Get ready to document an attack

After ensuring your own well-being, but before you document individual pieces of evidence, **making a “map” of all relevant information** about the attack can guide you in identifying all the evidence you might want to gather.

The most important part of documentation is **keeping an organized record (or “log”)**. For legal or technical documentation, a log that only contains key pieces of data is important to help demonstrate clear patterns of attacks. When documenting for your own peace of mind, a regular text document with less structure may be enough for you. A more structured log could also help you think about patterns in the attacks you face. Your digital log could be a text document or a spreadsheet. You could also keep a journal on paper. The choice is up to you.

**All digital interactions leave traces (metadata) that describe them**: Time, duration, addresses, sending and receiving accounts, etc. These traces are saved in logs by your own devices, by phone and social media companies, by internet providers, and by others. Analyzing them can give legal and technical experts vital information about who is behind an attack. For example, a record of the phone number(s) used in 20 calls in a given period of time could support a report of harassment.

Keeping an organized, structured record of this data will help you demonstrate **patterns in attacks** in ways that can support a legal case, or help a digital security expert block the attacks and protect your devices.

The following information will be useful in most cases, but each case may vary. You could copy and paste the fields below into the top of a spreadsheet or document table to structure your reports on each attack, adding more fields as you see necessary.

| Date | Time | Email addresses or phone numbers used by the attacker | Links related to the attack | Names (including account names) used by the attacker | Types of digital attack, harassment, misinformation, etc. |   Techniques used by the attacker |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |

Here are a few examples of other log templates from our community you could use:

* [Incident log template from Access Now’s Digital Security Helpline ](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md)
* [Gender-based violence template from Acoso.online](https://acoso.online/site2022/wp-content/uploads/2019/01/reactingNCP.pdf)

In additional to a log file, **store everything related to the event or incident** in a folder or folders — including your log document and any additional digital evidence you have exported, downloaded, or captured in a screenshot. Even evidence that does not carry legal weight can be useful to show the magnitude of an attack and to help you plan response strategies.

**Store the information you gather in a secure way**. It is safer to store backups on your own devices (not just online or “in the cloud”). Protect these files with encryption, and in hidden folders or drive partitions if possible. You don’t want to lose these records or, worse, have them exposed.

On social media, **avoid blocking, muting, or reporting the accounts attacking you** until you have gathered the documentation you need, as this may stop you from saving evidence from those accounts. If you have already blocked, muted, or reported accounts on social media, don’t worry. You may be able to un-block or un-mute accounts you can no longer see, or documentation may still be possible by looking at that account from a friend or colleague’s account.

## Take Screenshots

Don’t underestimate the importance of taking screenshots of all attacks (or getting someone you trust to do it for you). Many digital messages are easy to delete or lose track of. Some, like Tweets or WhatsApp messages, can only be recovered with the help of platforms, who often take a long time to respond to reports of attacks, if they respond at all.

If you don’t know how to take a screenshot on your device, do an online search for “how to screenshot” with the make, model and operating system of your device (for example, “Samsung Galaxy S21 Android how to screenshot”).

If you're taking a screenshot of a message in a web browser, it is important to **include the address (URL) of the page in your captured image**. This is in the address bar at the top of the browser window; sometimes browsers hide and show it as you scroll. Addresses help verify where the harassment occurred and help technical and legal professionals locate it more easily. Below you can see an example of a screenshot showing the URL.

<img src="../../images/Screenshot_with_URL.png" alt="Here is a sample view of a screenshot showing the URL." width="80%" title="Here is a sample view of a screenshot showing the URL.">

<a name="legal"></a>
## Documenting for a legal case

Consider whether you want to take legal action. Will it expose you to additional risk? Can you afford the time and effort involved? Starting a legal case is not always obligatory or necessary. Also consider if legal action will feel restorative to you.

If you decide to take the case to court, seek advice from a lawyer or legal organization that you trust. Poor or bad legal advice can be stressful and damaging. Do not take legal action on your own unless you really know what you are doing.

In most jurisdictions, a lawyer will need to demonstrate that the evidence is relevant to the case, how it was obtained and verified, that it was collected in a lawful way, that collecting it was necessary to build the case, and other facts that make the evidence acceptable to a court. Once the lawyer has demonstrated this, the evidence will be evaluated by a judge or court.

Things to consider when documenting for a legal case:

* It is crucial to preserve evidence quickly, from the start of an attack, because content can be quickly deleted by a poster or social media site, making it harder to save evidence for legal action later.
* Even if the attack seems minor, preserving all evidence is important. It might be part of a pattern that will only prove to be criminal when you see it from start to finish.
* You may want to ask platforms or ISPs to keep information about the digital attack for you. This may only be possible for a short time (weeks or a month) after the event took place. See [Without My Consent’s information on filing a litigation hold request](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation-hold-request) for more information on how to do this.
* Timestamps and email and web addresses are crucial for evidence to be accepted in court. Lawyers have to demonstrate that a message went from one device to another on a particular day at a particular time using these tools.
* Lawyers also need to verify the evidence has not been tampered with, that it has been in safe hands since you stored it, and that it is authentic. Screenshots and printouts of emails are not considered strong enough evidence in this regard.

For these legal reasons, it may be best for you to hire an expert like a notary or digital certification company that can testify in court if needed. Digital certification companies are often less expensive than notarized certificates. Any notary or certification company you hire should have enough technical background to perform tasks like these if needed:

* confirm timestamps
* verify the existence or non-existence of certain content, like comparing your screenshots and other gathered evidence to the original source, to confirm your evidence has not been tampered with
* offer evidence that digital certificates match URLs
* verify identities, such as checking if a person appears in a list of profiles on a social media platform and that they own a profile with the nickname attacking you
* confirm the ownership of a phone number in a WhatsApp conversation

Be aware that not all the evidence you collected will be accepted by the court. Research the legal procedures regarding digital threats in your country and region to decide what you gather.

### Documenting human rights violations

If you need to document human rights violations, and download or copy materials that social media platforms are actively removing, you might want to contact organizations like [Mnemonic]( https://mnemonic.org/en/our-work) that can help preserve these materials.

## How to save evidence for legal and digital forensic reports

Beyond saving screenshots, here are some further instructions on how to save evidence more thoroughly to support your case and the work of someone giving you legal or technical support.

* **Call logs.** The numbers in incoming and outgoing calls are stored in a database on your mobile phone.
    * You can take screenshots of the log.
    * It is also possible to save these logs by backing up your phone’s system or using specific software to download the logs.
* **Text messages (SMS)**: Most mobile phones allow you to back up your SMS messages to the cloud. Alternately, you can take screenshots of them. Neither of these methods counts as legal evidence without additional verification, but they are still important to documenting attacks.
    * Android: if your phone does not come with the ability to back up SMS messages, you can use an app such as [SMS Backup & restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore).
    * iOS: Turn on message backup here: iCloud in Settings > [your user name] > Manage storage > Backup. Tap the name of the device you are using, and activate message backups.
* **Call recording**. Newer mobiles come with a call recorder installed. Check what the legal rules are in your country or local jurisdiction about recording calls with or without the consent of both parties on a call; in some cases, it may be illegal. If it is legal to do so, you can set the call recorder to activate when you answer the call, without alerting the caller that they are being recorded.
    * Android: in Settings > Phone > Call recording settings you can activate automatic recording of all calls, calls from unknown numbers, or from specific numbers. If this option is not available on your device, you can download an app such as [Call Recorder](https://play.google.com/store/apps/details?id=com.lma.callrecorder).
    * iOs: Apple is more restrictive with call recording and has blocked the option by default. You would need to install an app such as [RecMe](https://apps.apple.com/us/app/call-recorder-recme/id1455818490).
* **Emails**. Every email has a “header” that contains information about who sent the message and how, like the addresses and postmark on a paper letter.
    * For instructions on viewing and saving email headers, you can use [this guide by the Computer Incident Response Center Luxembourg (CIRCL)](https://www.circl.lu/pub/tr-07/).
    * If you have more time, you may also want to set your email up to download all messages to a desktop client such as Thunderbird using [POP3](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3#w_changing-your-account). Instructions for setting up an email account on Thunderbird can be found in the [official documentation](https://support.mozilla.org/en-US/kb/manual-account-configuration).
* **Photos**. All pictures have “EXIF tags” - metadata that can tell you where and when a photo was taken.
    * You may be able to save images from a website or messages you have received in a way that saves EXIF tags that would be lost if you just took a screenshot. Press and hold the image on a mobile device, or right-click the image on a computer (control-click on a Mac, use the menu key in Windows).
* **Websites**. Downloading or backing up complete web pages can be useful for those trying to help you.
    * If the page where the harassment occurred is public (in other words, you don’t have to log in to see it), you can enter the page address into the Internet Archive’s [Wayback Machine](https://web.archive.org/) to save the page, record the date when you saved it, and return to the saved page for that date later.
    * Screenshots provide less detail, but can also save important information.
* **WhatsApp**. WhatsApp conversations can be downloaded in text format or with media attached; they are also backed up by default to iCloud or Google Drive.
    * Follow the instructions for exporting the history of an individual or group chat [here](https://faq.whatsapp.com/1180414079177245/?helpref=uf_share).
* **Telegram**. To export conversations in Telegram you will need to use the desktop application. You can choose the format and time period you want exported, and Telegram will generate an HTML file. See instructions [here](https://telegram.org/blog/export-and-more).
* **Facebook Messenger**. Log in to your Facebook account. Go to Settings > Your information on Facebook > Download a copy of your information. Many options will appear: select "Messages". Facebook takes time to process the file, but it will notify you when it is available for download.
* If you need to save **videos** as evidence, have an external drive (a hard disk, USB stick, or SD card) with enough storage space. You may need to use a video screen capture tool. The Camera app, XBox Game Bar, or Snipping Tool can be used for this on Windows, Quicktime on Mac, or you can use a browser plugin like [Video DownloadHelper](https://www.downloadhelper.net/) to save videos.
* In the case of large-scale organized attacks — for example, everything posted with a **specific hashtag, or a large number of online comments** — you may need help from a security team, digital forensics lab, or university to gather and process a lot of data. Organizations you might reach out to for help include [Citizen Lab](https://citizenlab.ca/about/), [Fundacion Karisma’s K-Lab](https://web.karisma.org.co/klab/) (working in Spanish), [Meedan](https://meedan.com/programs/digital-health-lab), the Stanford Internet Observatory and the Oxford Internet Institute.
* **Files attached to malicious messages** are valuable evidence. Under no circumstances should you click or open them. A trusted technical advisor should be able to help you safely contain these attachments and send them to people who can analyze them.
    * You can use [Danger Zone](https://dangerzone.rocks/) for converting potentially dangerous PDFs, office documents, or images to safe PDFs.
* For more targeted threats, such as **spyware** on your devices or **malicious emails** sent specifically to you, gathering deeper technical evidence can help your case. It is important to know when to gather evidence yourself and when to leave this to experts. If you are unsure about how to proceed, contact a [trusted advisor](../support).
    * **If the device is on, leave it on. If it is off, leave it off.** Turning it off or on, you risk losing important information. For highly sensitive and legal investigations, it is best to involve forensic experts before turning the device off or on again.
    * Take pictures of the device that you think contains malicious software. Document its physical condition and the place where you found it when you began to suspect it was tampered with. Are there any dents or scratches? Is it wet? Are there tools nearby that could have been used to tamper with it?
    * Keep both the device and the data you have copied off of it in a secure location.
    * The exact method of extracting the data depends on the device. Extracting data from a laptop is different from extracting data from a smartphone. Each device requires specific tools and knowledge.
* Data collection for targeted threats often involves copying **system log files** that your phone or computer collect automatically. Technical experts helping you with data collection may ask for these. They should help you safely contain metadata and attachments and send them to people who can analyse them.
    * To preserve this metadata, keep the device isolated from other storage systems, turn off Wi-Fi and Bluetooth, unplug wired network connections, and never edit the log files.
    * Do not plug in a thumb drive and attempt to copy log files onto it.
    * These files may include information about the state of files on the device (like how files were accessed) or about the device (like whether a shutdown or delete command was issued, or whether someone attempted to copy files to another device).
