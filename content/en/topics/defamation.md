---
layout: page
title: "I'm being targeted by a defamation campaign"
author: Inés Binder, Florencia Goldsman, Erika Smith, Gus Andrews
language: en
summary: "What to do when someone is trying to damage your reputation online."
date: 2023-04
permalink: /en/topics/defamation
parent: Home
---

# I'm being targeted by a defamation campaign

When someone deliberately creates and disseminates false, manipulated or misleading information to cause harm by undermining someone's reputation, that is a defamation campaign. These campaigns can be targeted at individuals or organizations with some kind of public exposure. They can be orchestrated by any actor, governmental or private.

Although this is not a new phenomenon, the digital environment spreads these messages fast, increasing the scale and speed of the attack and deepening its impact.

There is a gender dimension to defamation campaigns when the aim is to exclude women and LGBTQIA+ people from public life, disseminating information that "exploits gender inequalities, promotes heteronormativity, and deepens social cleavages" [[1]](#note-1).

This section of the Digital First Aid Kit will walk you through some basic steps to plan how to respond to a defamation campaign. Follow this questionnaire to identify the nature of your problem and find possible solutions.

<a name="note-1"></a>
[1] [Countering Disinformation](https://counteringdisinformation.org/node/13/)

## Workflow

### physical-wellbeing

Do you fear for your physical safety or well-being?

- [Yes](#physical-risk_end)
- [No](#no-physical-risk)

### no-physical-risk

> A defamation campaign aims to attack your reputation by questioning it, casting doubt, framing the target, alleging falsehoods, or exposing contradictions to erode public trust.
>
> Such attacks particularly affect people with public exposure whose work depends on their prestige and trust: activists and human rights defenders, politicians, journalists and people who make data available to the public, artists, etc.

Is this attack trying to undermine your reputation?

 - [Yes](#perpetrators)
 - [No](#no-reputational-damage)

### no-reputational-damage

 > If you are facing an attack not directed at undermining your reputation, you might be facing an emergency of another nature than a defamation campaign.

 Do you want to go through the questions to diagnose online harassment?

 - [Yes, I might be facing online harassment](../../../harassed-online)
 - [No, I still think it is a defamation campaign](#perpetrators)
 - [I need support to understand the issue I'm facing](/../../support)


### perpetrators

Do you know who is behind the defamation campaign?

 - [Yes](#respond-defamation)
 - [No](#analyze-messages)

### respond-defamation

> There are many ways to respond to a defamation campaign, depending on the impact and the spread of the messages, the actors involved, and their motivations. If the campaign has a low impact and spread, we recommend ignoring it. If, on the contrary, it has a high impact and spread, you might consider reporting and taking the content down, setting the record straight, and silencing it.
>
> Be careful not to amplify the defamatory messages when trying to address the defamation campaign. Quoting a message, even just to expose the people or motivations behind it, can increase its spread. Take into consideration the ["truth sandwich technique"](https://en.wikipedia.org/wiki/Truth_sandwich) in order to cover false information without unintentionally furthering its spread. This technique "entails presenting the truth about a subject before covering misinformation, then ending a story by again presenting truth".
>
> Choose the strategies that feel right to you to contain the attack, rebuild trust and re-establish credibility in your community. Remember that no matter what strategy you choose, your [self-care](/../../self-care/) has to be the top priority. Also consider [documenting](/../../documentation) content or profiles that are attacking you before responding.

How do you want to respond to the defamation campaign?

 - [I want to notify social media platforms and take down defamatory content](#notify-take-down)
 - [I want to ignore it and silence notifications](#ignore-silence)
 - [I want to set the record straight](#set-record-straight)
 - [I want to file a legal complaint](#legal-complaint)
 - [I need to analyze the defamation messages further so I can make a decision](#analyze-messages)

### analyze-messages

> When you or your organization are the targets of a defamation campaign, understanding standard methods used to spread disinformation can help you weigh risks and orient your next steps. Analyzing the messages you receive during this kind of attack could give you further information about perpetrators, motivation and resources employed to undermine your reputation. Attack techniques involve an array of tools and online coordination.
>
> You may want to first assess the level of risk you are facing. In the [Interaction Disinformation Toolkit](https://www.interaction.org/documents/disinformation-toolkit/), you will find a Risk Assessment Tool to help you evaluate the vulnerability of your media environment.
>
> A defamation campaign on social media will frequently employ hashtags to garner greater interest, but this is also helpful for monitoring the attack as you can search by hashtag and assess perpetrators and messaging. To measure hashtags' impact on social media, you may want to try these tools:
>
> - [Track my hashtag](https://https://www.trackmyhashtag.com/)
> - [Brand mentions](https://brandmentions.com/hashtag-tracker) aggregates mentions on hashtags from Twitter, Instagram and Facebook into a single stream of data.
> - [InVid Project](https://www.invid-project.eu/) is a knowledge verification platform to detect emerging stories and assess the reliability of newsworthy video files and content spread via social media.
> - [Metadata2go](https://www.metadata2go.com/view-metadata ) discovers metadata behind the files you are analysing.
> - [YouTube DataViewer](https://citizenevidence.amnestyusa.org/) can be used to do a reverse lookup of Amnesty International videos to see if the video is older and if it has been modified.
>
>**Manual vs Automated Attacks**
>
> State actors and other adversaries frequently employ coordinated botnets which are not that costly or technically difficult to set up. Recognizing that a barrage of attack messages are not from dozens or hundreds of people but are automated bots may reduce anxiety and make documentation easier, and will help you decide how you want to defend yourself. Use [Botsentinel](https://botsentinel.com/) to check the nature of the accounts attacking you. Some bot messages can be picked up and further disseminated by individuals too, so you may find that the score verifies some accounts transmitting disinformation messages, i.e. they do have followers, a unique profile picture and content, and other indicators that mean there is less probability they are a bot account. Another tool you can use is [Pegabot](https://es.pegabot.com.br/).
>
> **Internal Information vs Public Information**
>
> Skilled disinformation may base content on a kernel of truth or make content appear truthful. Reflecting on the type of information being shared and its sources may determine different courses of action.
>
> Is the information private or from internal sources? Consider changing your passwords and installing two-factor authentication to ensure that no one else is accessing your accounts but you. If this information is referenced in chats or in shared conversations or files, encourage friends and colleagues to review their security settings. Consider cleaning and closing up old chats so that information from the past is not easily accessible on your device or that of colleagues and friends.
>
> Adversaries may hijack an individual's or organization's accounts to spread [misinformation](https://en.wikipedia.org/wiki/Misinformation) and [disinformation](https://en.wikipedia.org/wiki/Disinformation) from that account, giving the information being shared much more legitimacy. To regain control of your accounts, check out the Digital First Aid Kit section [I cannot access my account](../../../account-access-issues/questions/what-type-of-account-or-service/).
>
> Is the information that is being disseminated sourced from public information available online? It may be helpful to follow the Digital First Aid Kit section [I have been doxxed or someone is sharing non-consensual images of me](../../../doxing) to search and narrow down where this information became available and what might be done.
> <br />
>
> **Creation of More Sophisticated Material** (YouTube videos, deepfakes, etc.)
>
> Sometimes attacks are more targeted and the perpetrators have invested time and effort into creating false imagery and news in YouTube videos or [deepfakes](https://en.wikipedia.org/wiki/Deepfake) in order to back up their fake storyline. Dissemination of such material may reveal perpetrators and give more fodder for legal and public denouncement of the attack and clear grounds for takedown requests. It can also mean that the attacks are highly personalised and increase one's feelings of vulnerability and risk. As when facing any attack, [self-care](/../../self-care/) and personal safety are top priorities.
>
> Another example is the use of fake domains in which an adversary creates a website or social network profile that looks similar to the authentic account. If you are facing such an issue, check out the Digital First Aid Kit section on [impersonation](../../../impersonated/).
>
> This type of attack can also indicate that there is greater organization, logistics, financing and time spent on part of the attacker, giving greater insight regarding the motivation of the attack and possible perpetrators.

Do you now have a better idea of who is attacking you and how they are doing it?

 - [Yes, now I would like to consider a strategy to respond](#respond-defamation)
 - [No, I need support to figure out how to respond](#defamation_end)

### notify-take-down

> ***Note:*** *Consider [documenting attacks](/../../documentation) before requesting that a platform take down defamatory content. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
> Terms of service of different platforms will explain when takedown requests will be considered legitimate, e.g. content in violation of copyright, for which you must be the owner. Copyright ownership happens by default from the moment you create an original piece of work: take a picture, write a text, compose a song, film a video, etc. It is not enough for you to appear in the piece; you must have created it.

Do you own the rights to the content used in the defamation campaign?

 - [Yes](#copyright)
 - [No](#report-defamatory-content)

### set-record-straight

> Your version of the facts is always important. Setting the record straight may be more urgent if the defamation campaign against you is widespread and highly harmful to your reputation. However, in these cases, you must evaluate the campaign's impact on you to assess whether a public response is your best strategy.
>
> In addition to reporting content and bad actors to platforms, you may want to build a counter-narrative, debunk the disinformation, publicly denounce the perpetrators or simply uplift the truth. How you respond will also depend on the community of support you have to help you and your own evaluation of the risk you might face in taking a public stance.

Do you have a team of people to support you?

 - [Yes](#campaign-team)
 - [No](#self-campaign)

### legal-complaint

> When facing a legal process, gathering, organizing, and presenting evidence to Court, you must follow a specific protocol so that it can be accepted by the judge as exhibits. When presenting online proof of an attack, taking a screenshot is not enough. Check out the Digital First Aid Kit section on [how to document an attack to be presented in a legal process](/../../documentation#legal) to learn more about this process before reaching out to a lawyer of your choice.


What would you like to do?

 - [These recommendations were helpful - I know what to do now](#resolved_end)
 - [These recommendations were helpful, but I would like to consider another strategy](#respond-defamation)
 - [I need support to proceed with my legal complaint](#legal_end)

### ignore-silence

> There are times, especially in cases where the defamation campaign has little impact or spread, when you may prefer to ignore the perpetrators and simply silence the messages. This can also be an important step for self-care, and friends or allies can be enlisted to monitor the content to alert you if other actions are merited. This does not eliminate content, should you wish to later document or review the information being disseminated to harm you.

Where is the defamatory content you would like to silence being disseminated?

 - [Email](#silence-email)
 - [Facebook](#silence-facebook)
 - [Instagram](#silence-instagram)
 - [Reddit](#silence-reddit)
 - [TikTok](#silence-tiktok)
 - [Twitter](#silence-twitter)
 - [Whatsapp](#silence-whatsapp)
 - [YouTube](#silence-youtube)

### copyright

> You can resort to copyright regulations, such as the [Digital Millennium Copyright Act (DMCA)](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), to take down content. Most social media platforms provide forms to report copyright infringement and may be more responsive to this type of request than other takedown considerations, because of its legal implications for them.
>
> ***Note:*** *Always [document](/../../documentation) before asking to take down content. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
> You can use these links to send a takedown request for copyright violation to the major social networking platforms:
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/en/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the content been taken down?

 - [Yes](#resolved_end)
 - [No, I need legal support](#legal_end)
 - [No, I need support contacting the platform](#harassment_end)

### report-defamatory-content

> Not all platforms have specialized processes for reporting defamatory content, and some do require a prior legal process. You can review the path each platform sets out for reporting defamatory content below.
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/contact/430253071144967)
> - [Instagram](https://help.instagram.com/contact/653100351788502)
> - [TikTok](https://www.tiktok.com/legal/report/feedback)
> - [Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=en_US)
> - [Twitter](https://help.twitter.com/en/rules-and-policies/twitter-report-violation#directly)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885?helpref=search&cms_platform=android)
> - [YouTube](https://support.google.com/youtube/answer/6154230)
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the reported content been taken down?

 - [Yes](#resolved_end)
 - [No, I need legal support](#legal_end)
 - [No, I need support contacting the platform](#harassment_end)

### silence-email

> Most email clients offer the option to filter messages and automatically tag them, archive them or delete them so they do not appear in your inbox. Usually you can create rules to filter messages by sender, recipient, date, size, attachment, subject, or keywords.
>
> Learn how to create filters in webmails:
>
> - [Gmail](https://support.google.com/mail/answer/6579?hl=en#zippy=%2Ccreate-a-filter)
> - [Protonmail](https://proton.me/support/email-inbox-filters)
> - [Yahoo](https://help.yahoo.com/kb/SLN28071.html)
>
> Learn how to create filters in email clients:
>
> - [MacOS Mail](https://support.apple.com/guide/mail/filter-emails-mlhl1f6cf15a/mac)
> - [Microsoft Outlook](https://support.microsoft.com/en-us/office/set-up-rules-in-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41)
> - [Thunderbird](https://support.mozilla.org/en-US/kb/organize-your-messages-using-filters)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-twitter

> If you don't want to be exposed to specific messages, you can block users or silence users and messages. Remember that if you choose to block someone or some content, you won't have access to the defamatory content to [document the attack](/../../documentation). In that case, you might prefer having access to it through another account or just silencing it.
>
> - [Mute accounts so they don't appear on your timeline](https://help.twitter.com/en/using-twitter/twitter-mute)
> - [Mute words, phrases, usernames, emojis, hashtags and notifications for a conversation](https://help.twitter.com/en/using-twitter/advanced-twitter-mute-options)
> - [Snooze notifications for direct messages forever](https://help.twitter.com/en/using-twitter/direct-messages#snooze)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-facebook

> Although Facebook does not allow you to mute users or content, it does allow you to block user profiles and pages.
>
> - [Block a Facebook profile](https://www.facebook.com/help/)
> - [Block messages from a profile on Facebook](https://www.facebook.com/help/1682395428676916)
> - [Block a Facebook Page](https://www.facebook.com/help/395837230605798)
> - [Ban or block profiles from your Facebook Page](https://www.facebook.com/help/185897171460026/)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-instagram

> Here's a list of tips and tools to silence users and conversations on Instagram:
>
> - [Mute or unmute someone on Instagram](https://help.instagram.com/469042960409432)
> - [Mute or unmute someone's Instagram story](https://help.instagram.com/290238234687437)
> - [Restrict or unrestrict someone](https://help.instagram.com/2638385956221960)
> - [Turn off account suggestions for your Instagram profile](https://help.instagram.com/530450580417848)
> - [Hide suggested posts in your Instagram feed](https://help.instagram.com/423267105807548)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-youtube

> If you are accessing YouTube from your desktop browser, you can hide videos, channels, sections and playlists from your home page.
>
> - [Tune your YouTube recommendations](https://support.google.com/youtube/answer/6342839)
>
> You can also create a block list in your YouTube profile to block other viewers on your YouTube live chat.
>
> - YouTube: [myaccount.google.com/blocklist](https://myaccount.google.com/blocklist)
> - [Block other viewers on YouTube live chat](https://support.google.com/youtube/answer/7663906)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-whatsapp

> WhatsApp is a common platform for defamation campaigns. Messages run fast because of trusted relationships between senders and receivers.
>
> - [Archive or unarchive a chat or group](https://faq.whatsapp.com/1426887324388733)
> - [Exit and delete groups](https://faq.whatsapp.com/498814665492149)
> - [Block a contact](https://faq.whatsapp.com/1142481766359885)
> - [Mute or unmute group notifications](https://faq.whatsapp.com/694350718331007)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-tiktok

> TikTok allows you to block users, prevent users from commenting on your videos, create filters for comments on your videos and delete, mute and filter direct messages.
>
> - [Bloking users on TikTok](https://support.tiktok.com/en/using-tiktok/followers-and-following/blocking-the-users)
> - [Choose who can comment on your videos in your settings](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments)
> - [How to turn on comment filters for your TikTok videos](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments#3)
> - [Manage comment privacy settings for all your Tik Tok videos](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments#4)
> - [Manage comment privacy settings for one of your TikTok videos](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments#5)
> - [How to delete, mute, and filter direct messages](https://support.tiktok.com/en/account-and-privacy/account-privacy-settings/direct-message#6)
> - [How to manage who can send you direct messages](https://support.tiktok.com/en/account-and-privacy/account-privacy-settings/direct-message#7)

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)

### silence-reddit

> If defamation is circulating on Reddit, you can choose to exclude the communities where this is happening from showing up in your notifications, Home feed (including recommendations), and Popular feed on your Reddit or reddit.com desktop site. If you are a moderator, you can also ban or mute users that repeatedly break your community’s rules.
>
> - [Reddit Community Muting](https://reddit.zendesk.com/hc/en-us/articles/9810475384084-What-is-community-muting-)
> - [Banning and Muting Community Members](https://mods.reddithelp.com/hc/en-us/articles/360009161872-User-Management-banning-and-muting)
>

Do you want to silence defamatory content on another platform?

 - [Yes](#ignore-silence)
 - [No](#damage-control)


### campaign-team

> If the campaign against you is investing time and resources, this suggests the perpetrators have a deep motivation to attack you. You may need to use all of the strategies mentioned in this workflow to mitigate the damage and build a public campaign in your defence. Friends and allies can be beneficial and center your self-care if they support you with message analysis, attack documentation and reporting content and profiles.
>
> A campaign team can work together to decide the best way to dispute the narrative being used against you. Disinformation messaging is insidious, and we are often told not to repeat attackers' messaging. Finding a balance between uplifting your story and refuting false claims is important.
>
> It is vital to do a [risk assessment analysis](https://www.interaction.org/documents/disinformation-toolkit/) for yourself and any supporting individuals and organizations before embarking on a public campaign.
>
> In addition to analyzing attacking messages, you and your team might want to start by [mapping organizations and allies](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html) that recognize your background and will add to a positive narrative about your work, or make enough joyous noise to deflect or drown out a smear campaign.
>
> You and your organization may not be the only ones under attack. Consider building trust with other organizations and communities that have been discriminated against or targeted by the disinformation campaign. This might mean preparing community members to address controversial messaging or sharing resources so others can defend themselves.
>
> Take time in your team to identify what your messaging and content types want to provoke - also based on whether you are refuting the campaign or uplifting positive messaging without referring to the defamation tactics. You may want people to be more skilled at differentiating rumors from facts. Simple values-based messaging is particularly effective in any public campaign.
>
> In addition to issuing public or press statements, you may want to select and prepare different spokespeople for your campaign, which also helps deflect personalized attacks common in smear campaigns, especially in cases of gendered disinformation.
>
> Establishing a media monitoring system to track your messaging campaign and that of your attackers will help you see which messages hit their mark best to fine-tune your campaign.
>
> If the defamatory content is also published in a newspaper or magazine, your team may want to demand [Right of Response](#self-strategies) or activate your case with [fact-checking sites](#self-strategies).

Do you think you have been able to control the damage to your reputation?

 - [Yes](#resolved_end)
 - [No](#defamation_end)

### self-campaign

> If you do not have the support of a team, you must take into consideration any capacity or resource limitations you have as you plan your next steps, prioritizing your [self-care](/../../self-care). You may want to publish a statement on your social media profile or website. Make sure you are in [full control](../../../account-access-issues) of those accounts before you do. In your statement, you may want to refer people to your established trajectory and trustworthy sources that reflect that trajectory. Your [analysis of the defamation campaign](#analyze-messages) will provide insight into the type of statement you want to make. For example, you can review if any news sources are being referenced in the attack.

Is the defamatory content also published in a newspaper or magazine?

 - [Yes](#self-strategies)
 - [No](#damage-control)

### self-strategies

> If the defamatory information is published, the editorial policy or even law in a particular country may grant you the [right of reply or right of correction](https://en.wikipedia.org/wiki/Right_of_reply). This not only gives you another platform to publish your statement, beyond your own media - it also provides a place to link to other statements you circulate.
>
> If the news media containing defamatory information is not a credible news source or is frequently a source of unfounded news, this can be useful to point out in any statement.
>
> There are many local, regional and global fact-checkers that can help you debunk the information that is circulating about you.
>
> - [DW Fact Check](https://www.dw.com/en/fact-check/t-56584214)
> - [France24 LesObservateurs](https://observers.france24.com/fr/)
> - [AFP FactCheck](https://factcheck.afp.com/)
> - [EUvsDisinfo](https://euvsdisinfo.eu/)
> - [Latam Chequea](https://chequeado.com/latamchequea)
> - [Europa fact check ](https://eufactcheck.eu/)

Do you feel you need a bigger response to the defamation campaign?

 - [Yes](#campaign-team)
 - [No](#damage-control)

### damage-control

> Your analysis of the defamation campaign will provide insight into the type of actions you want to take. For example, if bot messages are being employed against you, you may want to point this out in a public statement, or suggest ulterior motives of perpetrators behind the campaign if you are aware of them. However, establishing your own trajectory and getting help from friends and allies to promote and uplift your truth may be your preference, rather than interacting and reacting to defamatory content.

Do you think you have been able to control the damage to your reputation?

 - [Yes](#resolved_end)
 - [No](#defamation_end)

### physical-risk_end

> If you are at physical risk, please contact the organizations below who can support you.

:[](organisations?services=physical_security)

### defamation_end

> If you are still experiencing reputational damage from a defamation campaign, please contact the organizations below who can support you.

:[](organisations?services=advocacy&services=individual_care&services=legal)

### harassment_end

> If you need support to take down content, you can reach out to the organizations below who can support you.

:[](organisations?services=harassment)

### legal_end

> If you are still experiencing reputational damage from a defamation campaign, and you need legal support, please contact the organizations below.

:[](organisations?services=legal)

### resolved_end

Hopefully this troubleshooting guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

An important way to control the harm of a defamation plan is to have good safety practices to keep your accounts safe from malicious actors and take the pulse of information circulating about you or your organization. You might want to consider some of the preventative tips below.

- Map your online presence. Self-doxing consists in exploring open-source intelligence on oneself to prevent malicious actors from finding and using this information to impersonate you.
- Set up Google alerts. You can get emails when new results for a topic show up in Google Search. For example, you can get information about mentions of your name or your organization/collective name.
- Activate two-factor authentication (2FA) for your most important accounts. 2FA offers greater account security by requiring you to use more than one method to log into your accounts. This means that even if someone were to get hold of your primary password, they could not access your account unless they also had your mobile phone or another secondary means of authentication.
- Verify your profiles on social networking platforms. Some platforms offer a feature for verifying your identity and linking it to your account.
- Capture your web page as it appears now for use as evidence in the future. If your website allows crawlers, you can use the Wayback Machine, offered by archive.org. Visit the Internet Archive Wayback Machine, enter your website's name in the field under the "Save Page Now" header, and click the "Save Page Now" button.

#### resources

- [Union of Concerned Scientists: How to Counter Disinformation: Communication Strategies, Best Practices, and Pitfalls to Avoid](https://www.ucsusa.org/resources/how-counter-disinformation)
- [Addressing Online Misogyny and Gendered Disinformation: A How-To Guide, by ND](https://www.ndi.org/sites/default/files/Addressing%20Gender%20%26%20Disinformation%202%20%281%29.pdf)
- [Verification Handbook For Disinformation And Media Manipulation](https://datajournalism.com/read/handbook/verification-3)
- [A Guide to Prebunking: A promising way to inoculate against misinformation](https://firstdraftnews.org/articles/a-guide-to-prebunking-a-promising-way-to-inoculate-against-misinformation/)
- [OverZero: Communicating During Contentious Times: Dos and Don’ts to Rise Above the Noise](https://overzero.ghost.io/communicating-during-contentious-times-dos-and-donts-to-rise-above-the-noise/)
- [Visual actor mapping](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html)
- [Disinformation toolkit](https://www.interaction.org/documents/disinformation-toolkit/)
- [Defusing Hate Workbook](https://www.ushmm.org/m/pdfs/20160229-Defusing-Hate-Workbook-3.pdf)
