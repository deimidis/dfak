---
layout: page
title: "I have been doxxed or someone is sharing my private info or media without my consent"
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa, Alex Argüelles
language: en
summary: "Private information about me or media featuring my likeness is being circulated. It is distressing to me or could potentially be harmful"
date: 2023-05
permalink: /en/topics/doxing
parent: Home
---

# Someone is sharing my private information or media without my consent

Harassers may sometimes publish information that personally identifies you or exposes aspects of your personal life that you have chosen to keep private. This is sometimes called "doxxing", or "doxing" (a shorthand term for "releasing documents").

The goal of doxxing is to embarrass the target or agitate against them. This can have severe negative effects on your psycho-social well-being, personal safety, relationships, and work.

Doxxing is used to intimidate public figures, journalists, human rights defenders, and activists, but also frequently targets people belonging to sexual minorities or marginalized populations. It is closely related to other forms of online and gendered violence.

The person doxxing you may have obtained your documents from various sources. This could include breaking into your private accounts and devices. But it is also possible that they are exposing and taking out of context information or media you thought you were sharing privately with your friends, partners, acquaintances, or co-workers. In some cases harassers may aggregate publicly-available information about you from social media or public records. Putting this information together in new, harmfully misconstrued ways could constitute doxxing too.

If you have been doxxed, you can use the following questionnaire to identify where the harasser has found your information, and find ways to limit the damage, including removing the documents from websites.

## Workflow

### be_supported

> Documentation is fundamental for assessing the impacts, identifying the origin of these attacks, and developing responses that restore your safety. Depending on what happened, going through the documentation process could enhance the emotional impacts this aggression has on you. To lessen the burden of this work and reduce potential emotional stress on yourself, you may need support from a close friend or other trusted person while you document these attacks.

Do you feel you have someone you trust who can help you in case you need it?

 - [Yes, a trusted person is ready to help me](#physical_risk)

### physical_risk

> Reflect: Who do you think may be responsible for these attacks? What are their capabilities? How determined are they to harm you? What other parties may be motivated to do you harm? These questions will help you assess threats that are ongoing and assess if you are exposed to physical risks.

Do you fear for your physical integrity or well-being or that you may be at legal risk?

 - [Yes, I think I am exposed to physical risks](#yes_physical_risk)
 - [I think I may be at legal risk](#legal_risk)
 - [No, I would like to troubleshoot my problem in the digital sphere](#takedown_content)
 - [I am not sure. I need help assessing my risks.](#assessment_end)

### yes_physical_risk

> If you feel that you could be targeted by in-person attacks or that your physical integrity or well-being is at risk, you can refer to the following resources to think through your immediate physical safety needs.
>
> - [Front Line Defenders - Workbook on Security](https://www.frontlinedefenders.org/en/workbook-security) (multiple languages)
> - [National Domestic Violence Hotline - Create a Safety Plan](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/)
> - [Coalition against Online Violence - Physical Security Support](https://onlineviolenceresponsehub.org/physical-security-support)
> - [Cheshire Resilience Forum - How to Prepare for an Emergency](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/)
> - [FEMA Form P-1094 Create Your Family Emergency Communication Plan](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html)
> - [Reolink - How to Cleverly Secure Your Home Windows — Top 9 Easiest Security Solutions](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/)
> - [Reolink - How to Make Your Home Safe from Break-ins](https://reolink.com/blog/make-home-safe-from-break-ins/)
> - [Seguridad integral para periodistas - Seguridad Física](https://seguridadintegral.articulo19.org/categorias-prevencion/seguridad-fisica/) (in Spanish)

Do you need more help to secure your physical integrity and well-being?

 - [Yes](#physical-risk_end)
 - [No, but I think I'm also at legal risk](#legal_risk)
 - [No, I would like to troubleshoot my problem in the digital sphere](#takedown_content)
 - [I am not sure. I need help assessing my risks.](#assessment_end)

### legal_risk

> If you are considering taking legal action, keeping evidence of the attacks you were subjected to will be very important. It is therefore strongly recommended to follow the [recommendations in the Digital First Aid Kit page on recording information on attacks](/../../documentation).

Do you need immediate legal support?

 - [Yes](#legal_end)
 - [No, I would like to troubleshoot my problem in the digital sphere](#takedown_content)
 - [No](#resolved_end)

### takedown_content

Do you want to work on getting the inappropriately shared documents removed from public view?

 - [Yes](#documenting)
 - [No](#resolve_end)

### documenting

> Keeping your own documentation of the attack you have been subjected to is very important. It makes it easier for legal or technical helpers to support you. Please check out the [Digital First Aid Kit guide on documenting online attacks](/../../documentation) to learn how to best gather information for your particular needs.
>
> You may also want to ask a person you trust to help you look for evidence of the attack you have been subjected to. This can help ensure you are not re-traumatized by looking at the published materials again.

Do you feel you are ready to document the attack in a safe way?

- [Yes, I am documenting the attacks](#where_published)

### where_published

> Depending on where the attacker has publicly shared your information, you may want to take different actions.
>
> If your information has been posted on social media platforms based in countries with legal systems which define corporations' legal responsibilities towards people who use their services (for example, the European Union or the United States), it is more likely that the corporation's moderators will be willing to help you. They will need you to [document](#documentation) evidence of the attacks. You may be able to look up the country where a company is based on Wikipedia.
>
> Sometimes adversaries may publish your information on smaller, less-well-known sites, and link to them from social media. Sometimes these sites are hosted in countries where there is no legal protection, or they are run by individuals or groups who practice, endorse, or simply do not object to harmful behavior. In this case it may be more difficult to contact the sites where the material is posted, or find out who runs them.
>
> But even when the attacker has posted your documents somewhere other than popular social media (which is often the case), working to take down those materials or links to them can greatly reduce how many people see your information. It can also reduce the number and severity of attacks you may face.

My information was posted on

- [a regulated platform or service under law](#what_info_published)
- [a platform or service with no likelihood of responding to takedown requests](#legal_advocacy)

### what_info_published

What information has been non-consensually shared?

- [personal information that identifies me or is private: address, phone number, national security number, bank account, etc.](#personal_info)
- [media I did not consent to share, including intimate material (pictures, video, audio, texts)](#media)
- [a pseudonym I use has been connected with my real-life identity](#linked_identities)

### linked_identities

> If an aggressor publicly links your real-life name or identity to a pseudonym, nickname, or handle that you use when you express yourself, make your opinion public, organize, or engage in activism, consider closing accounts related to that identity as a way of reducing the damage.

Do you need to continue using this identity/persona to achieve your goals, or can you close the account(s)?

- [I need to keep using it](#defamation_flow_end)
- [I can close the account(s)](#close)

### close

> Before closing the accounts associated with the affected identity or persona, consider the risks of closing the account. You might lose access to services or data, face risks to your reputation, lose contact with your online associates, etc.

- [I have closed relevant accounts](#resolved_end)
- [I have decided not to close the relevant accounts right now](#resolved_end)
- [I need help understanding what I should do](#harassment_end)

### personal_info

Where has your personal information been published?

- [On a social networking platform](#doxing-sn)
- [On a website](#doxing-web)

### doxing-sn

> If your private information has been published in a social media platform, you can report a violation of the community standards following the reporting procedures provided to users by social networking websites.
>
> ***Note:*** *Always [document](/../../documentation) before taking actions such as deleting messages or conversation logs or blocking profiles. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
>You will find instructions for reporting a violation of the community standards to the main platforms in the following list:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the information been deleted?

 - [Yes](#one-more-persons)
 - [No](#harassment_end)

### doxing-web

> You can try reporting the website to the hosting provider or domain registrar, asking for a takedown.
>
> ***Note:*** *Always [document](/../../documentation) before taking actions such as deleting messages or conversation logs or blocking profiles. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
> To send a takedown request, you will also need to gather information on the website where your information has been published:
>
> - Go to [Network Tools' NSLookup service](https://network-tools.com/nslookup/) and find out the IP address (or addresses) of the fake website by entering its URL in the search form.
> - Write down the IP address or addresses.
> - Go to [Domain Tools' Whois Lookup service](https://whois.domaintools.com/) and search both for the domain and the IP address/es of the fake website.
> - Record the name and abuse email address of the hosting provider and domain service. If included in the results of your search, also record the name of the website owner.
> - Write to the hosting provider and domain registrar of the fake website to request its takedown. In your message, include information on the IP address, URL, and owner of the impersonating website, as well as the reasons why it is abusive.
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond / help](#platform_help_end)
- [I need help to figure out how to send a takedown request](#platform_help_end)

### media

> If you have created the media yourself, you usually will own the copyrights to the media. In some jurisdictions, individuals also have rights to the media they appear in (known as rights of publicity).

Do you own the copyrights to the media?

- [Yes](#intellectual_property)
- [No](#nude)

### intellectual_property

> You can resort to copyright regulations, such as the [Digital Millennium Copyright Act (DMCA)](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), to take copyrighted information down. Most social media platforms provide forms to report copyright infringement and may be more responsive to this type of request than other takedown considerations, because of its legal implications for them.
>
> ***Note:*** *Always [document](/../../documentation) before asking to take down content. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
> You can use these links to send a takedown request for copyright violation to the major social networking platforms:
>
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/en/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond / help](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### nude

> Media that show nude bodies or details thereof are sometimes covered by specific platform policies.

Does the media include nude bodies or details of nude bodies?

- [Yes](#intimate_media)
- [No](#nonconsensual_media)

### intimate_media

> To learn how to report a violation of privacy rules or non-consensual sharing of intimate media/harmful content to the most popular platforms, you can check out the following resources:
>
> - [Stop NCII](https://stopncii.org/) (Facebook, Instagram, TikTok and Bumble - worldwide)
> - [Revenge Porn Helpline - Help for Victims Outside the UK](https://revengepornhelpline.org.uk/how-can-we-help/if-we-can-t-help-who-can/help-for-victims-outside-the-uk/)
>
> ***Note:*** *Always [document](/../../documentation) before asking to take down content. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond / help](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### nonconsensual_media

Where have your media been published?

- [On a social networking platform](#NCII-sn)
- [On a website](#NCII-web)

### NCII-sn

> If your media have been published in a social media platform, you can report a violation of the community standards following the reporting procedures provided to users by social networking websites.
>
> ***Note:*** *Always [document](/../../documentation) before taking actions such as deleting messages or conversation logs or blocking profiles. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
>You will find instructions for reporting a violation of the community standards to the main platforms in the following list:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond / help](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### NCII-web

> Follow the instructions in ["Without My Consent - Take Down"](https://withoutmyconsent.org/resources/take-down) to take down content from a website.
>
> ***Note:*** *Always [document](/../../documentation) before taking actions such as deleting messages or conversation logs or blocking profiles. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal).*
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond / help](#platform_help_end)
- [I need help to figure out how to send a takedown request](#platform_help_end)

### legal_advocacy

> In some cases, the platforms where the doxxing or non-consensual publication of intimate media is taking place do not have policies or a moderation process that are mature enough to handle requests regarding doxxing, harassment or defamation.
>
> Sometimes the offensive material is published or hosted on platforms or apps that are run by individuals or groups that practice, endorse, or simply do not object to harmful behavior.
>
> There also places in the cyberspace where regular laws and regulations are hardly enforceable, because they are specifically created to operate anonymously and leave no trace. One of these is what is known as *the dark web*. There are benefits to making anonymous spaces like these available, even though some people abuse them.
>
> In situations like these when legal action is not available, consider whether you are willing to resort to advocacy. This could include publicly shedding light on your case, generating a public debate about it, and possibly surfacing similar cases. There are rights organizations and groups that can help you with that. They can help you set expectations for outcomes, and give you a sense of the ramifications.
>
> Seeking legal aid may be a last resort if communication with the platform, app, or service provider fails or is impossible.
>
> The legal path usually takes time, costs money, and may require that you publicize that doxxing happened to you or your intimate media were published without your consent. A successful legal case depends on many factors, including [documentation](#documentation#legal) of the aggression in a way that is deemed acceptable by courts, and the legal framework that governs the case. Identifying the perpetrators or determining that they were responsible may also be difficult to prove. It may also be challenging to establish the jurisdiction in which the case should be tried.

What would you like to do?

- [I need support to plan an advocacy campaign](#advocacy_end)
- [I need legal support](legal_end)
- [I have support from my local community](#resolved_end)

### advocacy_end

> If you would like to counteract information on you that has been spread online without your will, we suggest you follow the recommendations in the [Digital First Aid Kit workflow on defamation campaigns](../../../defamation).
>
> If you would like support to launch a campaign to expose your attackers, you can get in touch with organizations that can help with advocacy efforts:

:[](organisations?services=advocacy)


### legal_end

> If you need legal support, please contact the organizations below who can support you.
>
> If you are thinking of taking legal action, it will be very important to keep evidence of the attacks you were subjected to. It is therefore strongly recommended to follow the [recommendations in the Digital First Aid Kit page on recording information on attacks](/../../documentation).

:[](organisations?services=legal)

### physical-risk_end

> If you are at physical risk and need immediate help, please contact the organizations below who can support you.

:[](organisations?services=physical_security)


### assessment_end

> If you feel that you need help in assessing the risks you face because your information or media have been shared without your permission, please contact the organizations below who can support you.

:[](organisations?services=harassment&services=assessment)

### resolved_end

> Hopefully this troubleshooting guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

> If you would like to counteract information on you that has been spread online without your will, we suggest you follow the recommendations in the [Digital First Aid Kit workflow on defamation campaigns](../../../defamation).
>
> If you need specialized support, the list below includes organizations who can help you address reputational damage.

:[](organisations?services=advocacy&services=legal)

### platform_help_end

> Below is a list of organizations who can support you in reporting to platforms and services.

:[](organisations?services=harassment&services=legal)

### harassment_end

> If you need support to address your situation, please contact the organizations below who can support you.

:[](organisations?services=harassment&services=triage)


### final_tips

- Map your online presence. Self-doxing consists in exploring open-source intelligence on oneself to prevent malicious actors from finding and using this information to impersonate you. Learn more on how to search your online traces in [Access Now Helpline's Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- If the doxxing involved information or documents that only you, or a trusted few people, should have known or had access to, then you might also want to reflect on how the doxxer accessed them. Review your personal information security practices to improve the security of your devices and accounts.
- You may want to be vigilant in the near future, in case the same material, or related material, re-surfaces on the internet. One way to protect yourself is to look up your name or the pseudonyms you used in search engines, to see where else they may appear. You can set up a Google Alert for yourself, or use Meltwater or Mention. These services will notify you when your name or pseudonym appears on the internet, as well.

#### resources

- [Crash Override Network - So You’ve Been Doxed: A Guide to Best Practices](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices)
- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Ken Gagne, Doxxing defense: Remove your personal info from data brokers](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html)
- [Totem Project - Keep it private](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_EN+001/about)
- [Totem Project - How to protect your identity online](https://learn.totem-project.org/courses/course-v1:Totem+TP_IO_EN+001/about)
- [Coalition against Online Violence - I've been doxxed](https://onlineviolenceresponsehub.org/for-journalists#doxxed)
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
