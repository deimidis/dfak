---
layout: page
title: correo postal
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/postal-mail.md
parent: /es/
published: true
---

El envío de correo es un método de comunicación lento si te enfrentas a una situación urgente. Dependiendo de las jurisdicciones por donde viaja el correo, las autoridades podrían abrirlo y, a menudo, rastrear el remitente, el destinatario y las ubicaciones de origen y destino.
