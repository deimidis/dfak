---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Lunes-Jueves 9:00-17:00h EET/EEST
response_time: 2 días
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine es una organización no gubernamental con sede en Kiev, establecida en 2017 con la misión de apoyar la implementación de los derechos humanos en Internet mediante el desarrollo de la capacidad de las ONG y los medios independientes para que se aborden sus preocupaciones en seguridad digital e impacten en las políticas gubernamentales y corporativas en el campo de los derechos digitales.
