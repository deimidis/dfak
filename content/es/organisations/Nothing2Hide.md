---
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: Lunes-Viernes, 9:00-18:00h CET
response_time: 1 día
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (online secured form - based on Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) es una asociación que tiene como objetivo proporcionar a personas en el periodismo, abogacía, activismo de derechos humanos y ciudadanía "común" los medios para proteger sus datos y comunicaciones, brindando soluciones técnicas y capacitación adaptada a cada contexto. Nuestra visión es poner la tecnología al servicio de la difusión y protección de la información para fortalecer las democracias en todo el mundo.