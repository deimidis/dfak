---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: días entre semana, 9h-18h, UTC-5
response_time: 12 horas
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Como complemento de su trabajo principal, Internews también trabaja con personas, organizaciones y comunidades de todo el mundo para concienciar sobre seguridad digital, proteger el acceso a una internet abierta y sin censura, y mejorar las prácticas de seguridad digital. Internews ha capacitado a periodistas y personas defensoras de los derechos humanos en más de 80 países, y cuenta con sólidas redes de personas que capacitan y evaluan en seguridad digital, locales y regionales familiarizados con el marco de auditoría de seguridad y plantilla de evaluación para grupos de defensa (SAFETAG) ([https://safetag.org (en inglés)](https://safetag.org)). Internews está construyendo alianzas sólidas y receptivas con firmas de análisis e inteligencia de amenazas tanto de la sociedad civil como del sector privado, siendo capaz de ayudar directamente a sus personas aliadas a mantener una presencia en línea, segura y sin censura. Internews ofrece evaluaciones de seguridad técnicas y no técnicas, que incluyen desde utilizar el marco de trabajo SAFETAG hasta evaluaciones de políticas organizacionales y estrategias de prevención fiables basadas en la investigación de amenazas. Internews apoya en el análisis de phishing y malware para grupos de derechos humanos y medios de comunicación que sufren ataques digitales dirigidos.
