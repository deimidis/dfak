---
layout: page
title: "¿Te están acosando en línea?"
author: Floriana Pagano, Natalia Krapiva
language: es
summary: "¿Te están acosando en línea?"
date: 2023-04
permalink: /es/topics/harassed-online/
parent: /es/
---

# ¿Te están acosando en línea?

La Internet, y en particular las redes sociales, se han convertido en un espacio crítico para que participantes y organizaciones de la sociedad civil, especialmente las mujeres, las personas LGBTIQIA+ y otros grupos marginalizados, se expresen y hagan oír su voz. Pero al mismo tiempo, también se han convertido en espacios donde estos grupos son fácilmente señalados por expresar sus opiniones. La violencia y el abuso en línea les niegan a las mujeres, a las personas LGBTIQIA+ y a muchas otras personas desprivilegiadas el derecho a expresarse por igual, libremente y sin temor.

La violencia y el abuso en línea pueden tener muchas formas diferentes, y las entidades malintencionadas a menudo pueden quedar impunes debido a la falta de leyes que protejan a las víctimas del acoso en muchos países, y principalmente porque no existen estrategias de protección universales: deben modificarse creativamente dependiendo de qué tipo de ataque se está lanzando. Por lo tanto, es importante identificar la tipología del ataque dirigido a ti para decidir qué pasos se pueden tomar.

Por lo tanto, es importante identificar la tipología del ataque dirigido para decidir qué pasos se pueden tomar.

Esta sección del Kit de Primeros Auxilios Digitales te guiará a través de algunos pasos básicos para entender que tipo de ataque estás sufriendo y planificar cómo protegerte contra él, o encontrar un servicio de asistencia en seguridad digital que pueda apoyarte.

Si estas enfrentando acoso en línea, te sugerimos leer las recomendaciones sobre [cómo cuidarse]](/../../self-care) y [como documentar ataques](/../../documentation), t kyegi sigue este cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### physical-wellbeing

¿Temes por tu integridad física o bienestar?

- [Sí](#physical-risk_end)
- [No](#no-physical-risk)

### location

¿Quién ataca parece conocer su ubicación física?

- [Sí](#location_tips)
- [No](#device)

### location_tips

> Revisa tus publicaciones recientes en las redes sociales: ¿incluyen tu ubicación exacta? Si es así, deshabilita el acceso GPS para las aplicaciones de redes sociales y otros servicios en tu teléfono, de modo que cuando publiques tus actualizaciones, tu ubicación no se muestre.
>
> - [Cómo desactivar los servicios de ubicación en iOS](https://support.apple.com/es-es/HT207092)
> - [Administrar la configuración de ubicación en Android](https://support.google.com/accounts/answer/3467281?hl=es)
>
> Revisa las fotos que publicaste de ti mismo en línea: ¿incluyen detalles de lugares que son reconocibles y pueden mostrar claramente dónde estás? Para protegerte de posibles acosadores, es mejor no mostrar detalles de tu ubicación cuando publiques fotos o videos tuyos.
>
> Como medida de precaución adicional, también es una buena idea mantener el GPS apagado todo el tiempo, excepto tan sólo brevemente cuando realmente necesitas encontrar tu posición en un mapa.

¿Podría quien ataca haberte seguido a través de la información que publicaste en línea? ¿O todavía sientes que quien ataca conoce tu ubicación física por otros medios?

 - [No, creo que he resuelto mis problemas](#resolved_end)
 - [Sí, sigo pensando que quien ataca sabe dónde estoy](#device)
 - [No, pero tengo otros problemas](#account)

### device

¿Crees que quien ataca ha accedido o está accediendo a tu dispositivo?

 - [Sí](#device-compromised)
 - [No](#account-compromised)

### device-compromised

> Cambia la contraseña para acceder a tú dispositivo por otra contraseña única, larga y compleja:
>
> - [Mac OS](https://support.apple.com/es-lamr/HT202860)
> - [Windows](https://support.microsoft.com/es-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/es-lamr/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=es)

¿Aún tienes la sensación de que quien ataca puede estar controlando tu dispositivo?

 - [No, creo que he resuelto mis problemas](#resolved_end)
 - [No, pero tengo otros problemas](#account)
 - [No](#info_stalkerware)

### info_stalkerware

> [Stalkerware](https://es.wikipedia.org/wiki/Stalkerware) es cualquier software utilizado para monitorear la actividad o ubicación de una persona con el fin de acecharla o controlarla.

Si crees que alguien puede estar espiándote a través de una aplicación que instaló en tu dispositivo móvil, [este flujo de trabajo del Kit de Primeros Auxilios Digitales te ayudará a decidir si tu dispositivo está infectado con malware y cómo tomar medidas para limpiarlo](../../../device-acting-suspiciously).

¿Necesitas más ayuda para entender el ataque que estás sufriendo?

 - [No, creo que he resuelto mis problemas](#resolved_end)
 - [Sí, todavía tengo algunos problemas que me gustaría solucionar](#account)

### account

> Ya sea que alguien tenga acceso a su dispositivo o no, existe la posibilidad de que hayan accedido a sus cuentas en línea pirateándolas o porque sabían o descifraron su contraseña.
>
> Si alguien tiene acceso a una o más de sus cuentas en línea, podría leer sus mensajes privados, identificar sus contactos, publicar sus publicaciones, fotos o videos privados, o comenzar a hacerse pasar por usted.
>
> Revise la actividad de sus cuentas en línea y el buzón de correo (incluidas las carpetas Enviados y Papelera) en busca de posibles actividades sospechosas.

¿Ha notado que desaparecen publicaciones o mensajes, u otras actividades que le dan una buena razón para pensar que su cuenta puede haber sido comprometida?

 - [Sí](#account-compromised)
 - [No](#private-contact)

### change passwords

> Intenta cambiar la contraseña de cada una de sus cuentas en línea con una contraseña segura y única.
>
> Puedes obtener más información sobre cómo crear y administrar contraseñas en esta [Guía de autodefensa de vigilancia](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras).
>
> También es una muy buena idea agregar una segunda capa de protección a tus cuentas en línea habilitando la autentificación de dos factores (2FA) siempre que sea posible.
>
> Descubre cómo habilitar la autentificación de dos factores en esta [guía de autodefensa de vigilancia](https://ssd.eff.org/es/module/c%C3%B3mo-habilitar-la-autenticaci%C3%B3n-de-dos-factores).

¿Aún tienes la sensación de que alguien puede tener acceso a tu cuenta?

 - [Sí](#hacked-account)
 - [No](#private-contact)

### hacked-account

> Si no puedes acceder a tu cuenta, existe la posibilidad de que tu cuenta haya sido pirateada y el pirata informático haya cambiado tu contraseña.

Si crees que tu cuenta puede haber sido pirateada, intenta seguir el flujo de trabajo del Kit de Primeros Auxilios Digitales que puede ayudarte a solucionar problemas de acceso a la cuenta.

 - [Llévame al flujo de trabajo para solucionar problemas de acceso a la cuenta](../../../account-access-issues)
 - [Creo que he resuelto mis problemas](#resolved_end)
 - [Mi cuenta está bien, pero tengo otros problemas](#private-contact)


### private-contact

¿Estás recibiendo llamadas telefónicas o mensajes no deseados en una aplicación de mensajería?

 - [Sí](#change_contact)
 - [No](#threats)

### change_contact

> Si recibes llamadas telefónicas no deseadas, mensajes SMS o mensajes en aplicaciones asociadas con tu número de teléfono, correo electrónico u otra información de contacto privada, puedes intentar cambiar tu número, tarjeta SIM, correo electrónico u otra información de contacto asociada con la cuenta.
>
> También debes considerar informar y bloquear los mensajes y la cuenta asociada a la plataforma correspondiente.

¿Ha detenido con éxito las llamadas o mensajes no deseados?

 - [Sí](#legal)
 - [No, tengo otros problemas](#threats)
 - [No, necesito ayuda](#harassment_end)

### threats

¿Estás siendo chantajeado o recibes amenazas a través de correos electrónicos o mensajes en una cuenta de redes sociales?

 - [Sí](#threats_tips)
 - [No](#impersonation)

### threats_tips

> Si recibes mensajes que contienen amenazas, incluidas amenazas de violencia física o sexual, o chantaje, debes documentar lo que sucedió tanto como sea posible, lo que incluye grabar enlaces y capturas de pantalla, reportar la persona a la plataforma o proveedor de servicios correspondiente y bloquear al atacante.

¿Has detenido con éxito las amenazas?

 - [Sí](#legal)
 - [Sí, pero tengo más problemas](#impersonation)
 - [No, necesito apoyo legal](#legal-warning)

### impersonation

> Otra forma de acoso de la que podría haber sido objeto es la suplantación de identidad.
>
> Por ejemplo, alguien puede haber creado una cuenta con tu nombre y está enviando mensajes privados o atacando públicamente a alguien en las plataformas de redes sociales, difundiendo desinformación o discursos de odio, o actuando de otras maneras para hacerte vulnerable, a tu organización o a otras personas cercanas a ti vulnerables a los riesgos reputacionales y de seguridad.
>
> Si crees que alguien se está haciendo pasar por ti o tu organización, puedes seguir este flujo de trabajo del Kit de Primeros Auxilios Digitales, que te guiará a través de las diversas formas de suplantación para identificar la mejor estrategia posible para enfrentar tu problema.

¿Qué te gustaría hacer?

 - [Estoy siendo suplantado y me gustaría encontrar una solución](../../../impersonated)
 - [Me enfrento a una forma diferente de acoso](#defamation)

### defamation

¿Alguien está tratando de dañar su reputación al difundir información falsa?

 - [Sí](#defamation-yes)
 - [Me enfrento a una forma diferente de acoso](#doxing)

### defamation-yes

> La difamación generalmente se define como hacer una declaración que daña la reputación de alguien. La difamación puede ser hablada (calumnia) o escrita (libelo). Las leyes de diferentes países pueden diferir en lo que constituye difamación para acciones legales. Por ejemplo, algunos países tienen leyes que protegen mucho la libertad de expresión y permiten que los medios de comunicación y los particulares digan cosas sobre los funcionarios públicos que pueden ser vergonzosas o dañinas, siempre y cuando crean que dicha información es cierta. Otros países pueden permitirle demandar a otros más fácilmente por difundir información sobre ti que no te gusta.
>
> Si alguien está tratando de dañar tu reputación o la de tu organización, puedes seguir el flujo de trabajo del Kit de Primeros Auxilios Digitales sobre difamación, que te guiará a través de varias estrategias para abordar las campañas de difamación.

¿Qué te gustaría hacer?

 - [Me gustaría encontrar una estrategia contra una campaña de difamación](../../../defamation)
 - [Me enfrento a una forma diferente de acoso](#doxing)

### doxing

¿Alguien ha publicado información privada o fotos sin tu consentimiento?

- [Sí](#doxing-yes)
- [No](#hate-speech)

### doxing-yes

> Si alguien ha publicado información privada sobre ti o está circulando videos, fotos u otros medios sobre ti, puedes seguir el flujo de trabajo del Kit de Primeros Auxilios Digitales que puede ayudarte a comprender qué está sucediendo y cómo responder a dicho ataque.

¿Qué te gustaría hacer?

 - [Me gustaría entender qué está pasando y qué puedo hacer al respecto](../../../doxing)
 - [Me gustaría recibir apoyo](#harassment_end)

### hate-speech

¿Alguien está difundiendo mensajes de odio contra ti basados ​​en atributos como raza, género o religión?

- [Sí](#one-more-persons)
- [No](#harassment_end)


### one-more-persons

¿Has sido atacado por una o más personas?

- [Me ha atacado una persona](#one-person)
- [Me han atacado más de una persona](#more-persons)

### one-person

> Si el discurso de odio proviene de una sola persona, la forma más fácil y rápida de contener el ataque y evitar que el usuario continúe enviándote mensajes de odio es denunciarlo y bloquearlo. Recuerda que si bloqueas al usuario, no podrás acceder a su contenido para documentarlo. Antes de bloquear, lee nuestros [consejos para documentar ataques digitales](/../../documentation).
>
> Ya sea que sepa quién es su acosador o no, siempre es una buena idea bloquearlo en las plataformas de redes sociales siempre que sea posible.
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=es)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/es/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/es/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?locale=es_LA&cms_platform=android&cms_id=1142481766359885&draft=false)

¿Has bloqueado a tu acosador de manera efectiva?

- [Sí](#legal)
- [No](#campaign)

### legal

> Si sabes quién te está acosando, puedes denunciarlo a las autoridades de tu país si lo consideras seguro y apropiado en tu contexto. Cada país tiene diferentes leyes para proteger a las personas del acoso en línea, y debes explorar la legislación de tu país o solicitar asesoramiento legal para ayudarte a decidir qué hacer.
>
> Si no sabes quién te está acosando, en algunos casos, podrías rastrear la identidad de quien ataca a través de un análisis forense de los rastros que pudo haber dejado.
>
> En cualquier caso, si te estás planteando emprender acciones legales, será muy importante conservar pruebas de las agresiones de las que fuiste objeto. Por lo tanto, se recomienda encarecidamente seguir las [recomendaciones de la página del Kit de Primeros Auxilios Digitales sobre el registro de información sobre ataques](/../../documentación).

¿Qué te gustaría hacer?

 - [Me gustaría recibir ayuda legal para demandar a mi atacante](#legal_end)
 - [Pienso que he resuelto mis problemas](#resolved_end)

### campaign

> Si te están atacando más de una persona, es posible que seas el blanco de una campaña de incitación al odio o de acoso, y deberías reflexionar sobre cuál es la mejor estrategia que se aplica a tu caso.
>
> Para conocer todas las estrategias posibles, lee la [página de Take Back The Tech sobre estrategias contra el discurso de odio](https://takebackthetech.net/es/be-safe/discurso-de-odio-estrategias).

¿Has identificado la mejor estrategia para ti?

 - [Sí](#resolved_end)
 - [No, necesito ayuda para solucionar más problemas](#harassment_end)
 - [No, necesito asistencia legal](#legal_end)

### harassment_end

> Si aún estás bajo acoso y necesitas una solución personalizada, por favor comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=harassment)


### physical-risk_end

> Si estás en riesgo físico y necesitas ayuda inmediata, por favor, comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=physical_security)


### legal_end

> Si necesitas asistencia legal, comunícate con las organizaciones descritas a continuación que pueden ayudarte.
>
> Si estás pensando en emprender acciones legales, será muy importante que conserves pruebas de los ataques de los que fuiste objeto. Por lo tanto, se recomienda encarecidamente seguir las [recomendaciones de la página del Kit de Primeros Auxilios Digitales sobre el registro de información sobre ataques](/../../documentation).

:[](organisations?services=legal)

### resolved_end

Esperamos que esta guía de solución de problemas te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Documentar acoso:** Es útil documentar los ataques o cualquier otro incidente que puedas estar presenciando: tomar capturas de pantalla, guardar los mensajes que recibes de los acosadores, etc. Si es posible, crea un diario donde puedas sistematizar esta documentación registrando fechas, horas, plataformas y URLs, ID de usuario, descripción de lo sucedido, etc. Los diarios pueden ayudarte a detectar posibles patrones e indicaciones sobre tus atacantes. Si te sientes abrumada/o, intenta pensar en alguien en quien confíes que pueda documentar dichos incidentes por ti durante un tiempo. Debes confiar profundamente en la persona que administrará esta documentación, ya que deberás entregarle las credenciales de tus cuentas personales. Antes de compartir tu contraseña con esta persona, cámbiala por otra diferente y compártela a través de un medio seguro, como una herramienta con [cifrado de extremo a extremo](https://www.frontlinedefenders.org/es/resource-publication/guide-secure-group-chat-and-conferencing-tools). Una vez que sientas que puedes recuperar el control de la cuenta, recuerda volver a cambiar tu contraseña a algo único, [seguro](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras), y que solo tu sepas.

    - - Sigue las [recomendaciones en la página del Kit de Primeros Auxilios Digitales sobre el registro de información de ataques](/../../documentation) para almacenar información sobre tu ataque de la mejor manera posible.

- **Configura la autenticación en 2 pasos** en todas tus cuentas. La autenticación en 2 pasos puede ser muy efectiva para evitar que alguien acceda a tus cuentas sin tu permiso. Si puedes elegir, no utilices la autenticación de 2 pasos basada en SMS y elige una opción diferente, basada en una aplicación de teléfono o en una clave de seguridad.

    - Si no sabes qué solución es la mejor para ti, puedes consultar la infografia de Access Now ["¿Cuál es el mejor tipo de autenticación multifactor para mí?"](https://www.accessnow.org/cms/assets/uploads/2017/12/2FA-infographic-ESP.png) y la [Guía del EFF, sobre los tipos comunes de autenticación de dos factores en la Web" (en inglés)](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).

    - Puedes encontrar instrucciones para configurar la autenticación de dos factores en las principales plataformas en [The 12 Days of 2FA: How to Enable Two-Factor Authentication For Your Online Accounts (en inglés)](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Mapea tu presencia en línea**. El auto doxing consiste en explorar la inteligencia de código abierto que existe sobre nosotras para evitar que actores maliciosos encuentren y utilicen esta información para hacerse pasar por ti. Obtén más información sobre cómo buscar tus rastros en línea en [Guía de la línea de ayuda de Access Now para prevenir el doxing (en inglés)](https://guides.accessnow.org/self-doxing.html)
- **No aceptes mensajes de remitentes desconocidos.** Algunas plataformas de mensajería, como WhatsApp, Signal o Facebook Messenger, te permiten obtener una vista previa de los mensajes antes de aceptar al remitente como confiable. Apple iMessage también te permite cambiar la configuración para filtrar mensajes de remitentes desconocidos. Nunca aceptes el mensaje o el contacto si parece sospechoso o si no conoces al remitente.


#### Resources

- [Acoso.Online](https://acoso.online)
- [Cloud seguro: ¿Qué hacer si soy víctima de Ciberacoso?](https://www.cloudseguro.co/victima-de-ciberacoso/)
- [National Network to End Domestic Violence: Sugerencias sobre la Documentación para Sobrevivientes del Abuso Tecnológico y Acecho](https://www.techsafety.org/sugerencias-sobre-la-documentacion-para-sobrevivientes-del-abuso-tecnolgico-y-acecho)
- [Stop bullying: Reportar casos de ciberacoso](https://espanol.stopbullying.gov/acoso-por-internet/c%C3%B3mo-denunciar-129k/%C3%ADndice.html)
- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Guía para prevenir el Doxing (en inglés)](https://guides.accessnow.org/self-doxing.html)
- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: FAQ - Acoso en línea dirigido a un miembro de la sociedad civil (en inglés)](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)
- [Equality Labs: Guía anti-doxing para activistas que enfrentan ataques desde la Alt-derecha (en inglés)](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Bloqueando tu identidad digital (en inglés)](http://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Red Nacional para Terminar con la Violencia Doméstica: Consejos de Documentación para Sobrevivientes de Abuso de Tecnología y Acecho (en inglés)](https://www.techsafety.org/documentationtips)
