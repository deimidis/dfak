﻿---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: دوشنبه تا جمعه، ۵/۲۴، UTC-4
response_time: شش ساعت
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect یک سرویس امنیتی رایگان وب سایت است که از جامعه مدنی و گروه‌های حقوق بشر در برابر حملات دیجیتال دفاع می‌کند.


