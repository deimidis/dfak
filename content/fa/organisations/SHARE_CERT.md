﻿---
name: SHARE CERT
website: https://www.sharecert.rs/
logo: SHARECERT_Logo.png
languages: Srpski, Македонски, English, Español
services: in_person_training, org_security, assessment, secure_comms, vulnerabilities_malware, browsing, account, harassment, forensic, legal, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: دوشنبه تا جمعه، ۹ صبح تا ۵ بعد از ظهر، GMT+1/GMT+2
response_time: یک روزه
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.sharecert.rs/prijava-incidenta/
email: info@sharecert.rs, emergency@sharecert.rs
pgp_key_fingerprint: info@sharecert.rs - 3B89 7A55 8C36 2337 CBC2 C6E9 A268 31E2 0441 0C10
mail: Kapetan Mišina 6A, Office 31, 11000 Belgrade, Serbia
phone: +381 64 089 70 67
initial_intake: yes
---

ماموریت SHARE CERT این است که حوزه انتخابیه خود را برای پاسخ به حملات به شیوه ای کارآمد و انعطاف پذیرتر کردن آن در جلوگیری از حملات سایبری آینده تقویت کند.


