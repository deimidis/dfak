﻿---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: دوشنبه تا جمعه, GMT+5.30
response_time: ۲۴ ساعت در ساعات تعطیل، ۲ ساعت در زمان کار ما
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

تیم آمادگی اضطراری رایانه تبتی (TibCERT) به دنبال ایجاد یک ساختار رسمی و مبتنی بر ائتلاف برای کاهش و کاهش تهدیدات آنلاین در جامعه تبتی و همچنین توسعه ظرفیت تحقیقات فنی تبتی ها در مورد تهدیدات در دیاسپورا و نظارت و سانسور در داخل است. تبت، در نهایت آزادی و امنیت آنلاین بیشتری را برای جامعه تبت به عنوان یک کل تضمین می کند.

ماموریت TibCERT شامل:

- ایجاد و حفظ بستری برای همکاری طولانی مدت بین سهامداران در جامعه تبت در مورد مسائل و نیازهای امنیت دیجیتال،
- تعمیق ارتباطات و توسعه یک فرآیند رسمی برای همکاری بین تبتی‌ها و بدافزارهای جهانی و محققان امنیت سایبری برای اطمینان از اشتراک‌گذاری سودمند متقابل،
- افزایش منابع در دسترس تبتی ها برای دفاع در برابر و کاهش حملات آنلاین با انتشار منظم اطلاعات و توصیه ها در مورد تهدیدات جامعه،
- کمک به تبتی‌ها در تبت برای دور زدن سانسور و نظارت با ارائه اطلاعات و تجزیه و تحلیل منظم و دقیق و همچنین راه حل های بالقوه.



