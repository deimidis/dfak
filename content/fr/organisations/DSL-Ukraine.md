---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: du lundi au jeudi de 9h à 17h EET/EEST
response_time: 2 jours
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine est une organisation non gouvernementale basée à Kiev, créée en 2017 avec pour mission de soutenir la mise en œuvre des droits de l'homme sur Internet en renforçant la capacité des ONG et des médias indépendants à faire prendre en compte leurs préoccupations en matière de sécurité numérique et à avoir un impact sur les politiques du gouvernement et des entreprises dans le domaine des droits numériques.
