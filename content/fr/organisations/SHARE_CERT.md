---
name: SHARE CERT
website: https://www.sharecert.rs/
logo: SHARECERT_Logo.png
languages: Srpski, Македонски, English, Español
services: in_person_training, org_security, assessment, secure_comms, vulnerabilities_malware, browsing, account, harassment, forensic, legal, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: du lundi au vendredi de 9h à 17h GMT+1/GMT+2
response_time: Dans la journée
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.sharecert.rs/prijava-incidenta/
email: info@sharecert.rs, emergency@sharecert.rs
pgp_key_fingerprint: info@sharecert.rs - 3B89 7A55 8C36 2337 CBC2 C6E9 A268 31E2 0441 0C10
mail: Kapetan Mišina 6A, Office 31, 11000 Belgrade, Serbia
phone: +381 64 089 70 67
initial_intake: yes
---

La mission de SHARE CERT est de permettre à ses membres de répondre aux attaques de manière efficace et de les rendre plus résilients pour prévenir de futures cyberattaques.
