---
layout: topic
title: "Mon appareil agit de façon suspecte"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: fr
summary: "Si votre ordinateur ou téléphone agit de façon suspecte, il y a peut être des logiciels non désirés ou malveillants dans votre appareil."
date: 2023-04
permalink: /fr/topics/device-acting-suspiciously
parent: /fr/
---

# My device is acting suspiciously

Les attaques de logiciels malveillants ont évolué et sont devenues très sophistiquées au fil des années. Ces attaques posent des menaces multiples et différentes et peuvent avoir de graves implications pour votre infrastructure et vos données personnelles et organisationnelles.

Les attaques de logiciels malveillants se présentent sous différentes formes, telles que les virus, l'hameçonnage, les rançongiciels, les chevaux de Troie et les kits de prise de contrôle de votre appareil (rootkits). Certaines des menaces sont : le crash d'ordinateurs, le vol de données (c'est à dire : informations d'identification de compte, informations financières, identifiants de compte bancaire), un·e attaquant·e qui vous fait chanter pour lui payer une rançon en prenant le contrôle de votre appareil et l'utiliser pour espionner tout ce que vous faites en ligne ou lancer des attaques par déni de service (DDoS).

Certaines méthodes couramment utilisées par les attaquant·e·s pour vous compromettre, vous et vos périphériques, semblent être des activités régulières, telles que :

- En vous envoyant un courriel ou un message sur les médias sociaux qui vous incitera à ouvrir une pièce jointe ou à cliquer sur un lien.
- En vous poussant à télécharger et à installer des logiciels à partir d'une source non fiable.
- En vous poussant à entrer votre nom d'utilisateur·rice et votre mot de passe dans un site Web qui a l'air légitime, mais qui ne l'est pas.
- En installant à votre insu un logiciel espion commercial dans votre appareil lorsque vous le laissez sans surveillance et déverrouillé.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour déterminer si votre appareil est probablement infecté ou non.

Si vous pensez que votre ordinateur ou votre appareil mobile a commencé à agir de façon suspecte, vous devriez d'abord penser aux symptômes.

Les symptômes qui peuvent généralement être considérés comme une activité suspecte de l'appareil, mais qui souvent ne sont pas une raison suffisante pour s'inquiéter, sont les suivants :

- Bruit de clics pendant les appels téléphoniques
- Décharge inattendue de la batterie
- Surchauffe lorsque l'appareil n'est pas utilisé
- Dispositif fonctionnant lentement

Ces symptômes sont souvent perçus à tort comme des indicateurs fiables de l'activité suspecte des appareils en question. Cependant, aucun d'entre eux pris isolément n'est une raison suffisante de s'inquiéter.

Les symptômes fiables d'un appareil compromis sont généralement :

- L'appareil redémarre fréquemment tout seul
- Plantage d'applications, en particulier après une interaction avec celles-ci
- Les mises à jour du système d'exploitation et/ou les correctifs de sécurité échouent de façon répétée
- Le témoin d'activité de la webcam s'allume lorsque la webcam n'est pas utilisée
- Un ["Écran bleu de la mort"](https://fr.wikipedia.org/wiki/%C3%89cran_bleu_de_la_mort) à répétition ou panique du noyau
- Fenêtres clignotantes
- Avertissements de l'antivirus


## Workflow

### start

Compte tenu des informations fournies dans l'introduction, si vous pensez toujours que votre appareil peut être compromis, le guide suivant peut vous aider à identifier le problème.

 - [Je pense que mon téléphone mobile agit de façon suspecte](#phone-intro)
 - [Je pense que mon ordinateur mobile agit de façon suspecte](#computer-intro)
 - [Je ne pense plus que mon appareil est compromis](#device-clean)


### device-clean

> Super ! Cependant, n'oubliez pas que ces instructions vous aideront à n'effectuer qu'une analyse rapide. Si cela suffit à identifier les anomalies visibles, des logiciels espions plus sophistiqués pourraient être capables de se cacher plus efficacement.

Si vous soupçonnez toujours que votre appareil pourrait être compromis, vous voudrez peut-être :

- [chercher une aide complémentaire](#malware_end)
- [procéder directement à une réinstallation de l'appareil](#reset).


### phone-intro

> Il est important de considérer comment votre appareil a pu être compromis.
>
> Depuis combien de temps avez-vous commencé à soupçonner que votre appareil agissait de façon suspecte ?
> Vous souvenez-vous d'avoir cliqué sur des liens de sources inconnues ?
> Avez-vous reçu des messages de personnes que vous ne reconnaissez pas ?
> Avez-vous installé un logiciel non signé ?
> L'appareil a t-il été hors de votre possession ?

Réfléchissez à ces questions pour essayer d'identifier les circonstances, le cas échéant, qui ont conduit à la compromission de votre appareil.

 - [J'ai un appareil Android](#android-intro)
 - [J'ai un appareil iOS](#ios-intro)


### android-intro

> Vérifiez d'abord si des applications inconnues sont installées sur votre appareil Android.
>
> Vous trouverez la liste des applications dans la section "Applications" du menu Paramètres. Identifiez toutes les applications qui n'ont pas été préinstallées avec votre appareil et que vous ne vous souvenez pas avoir téléchargées.
>
> Si vous soupçonnez l'une des applications de la liste, lancez une recherche sur le Web et recherchez des ressources pour voir s'il existe des rapports qui identifient l'application comme malveillante.

Avez-vous trouvé des applications suspectes ?

 - [Non](#android-unsafe-settings)
 - [Oui, j'ai identifié des applications potentiellement malicieuses](#android-badend)


### android-unsafe-settings

> Android donne aux utilisateur·rice·s la possibilité d'activer l'accès à un plus bas niveau de leur appareil. Cela peut être utile pour les développeur·euse·s de logiciels, mais cela peut aussi exposer les appareils à des attaques supplémentaires. Vous devriez examiner ces paramètres de sécurité et vous assurer qu'ils sont réglés sur les options les plus sûres. Les fabricants peuvent expédier des appareils dont les paramètres par défaut ne sont pas sécurisés. Ces paramètres doivent être revus même si vous n'avez pas fait les changements vous-même.
>
> #### Applications non signées
>
> Android bloque normalement l'installation des applications qui ne sont pas chargées depuis le Google Play Store. Google a des processus pour examiner et identifier les applications malveillantes sur le Play Store. Les attaquant·e·s tentent souvent d'éviter ces contrôles en vous proposant directement des applications malveillantes via un message contenant un lien ou un fichier. Il est important de vous assurer que votre appareil ne permet pas l'installation d'applications provenant de sources non fiables.
>
> Allez dans la section "Sécurité" de vos paramètres Android et assurez-vous que l'option Installer l'application depuis une source inconnue est désactivée.
>
> #### Mode développeur·euse et accès ADB (Android Debug Bridge)
>
> Android permet aux développeur·euse·s d'exécuter directement des commandes sur le système d'exploitation sous-jacent en "mode développeur". Lorsqu'il est activé, cela expose les appareils à des attaques physiques. Une personne ayant un accès physique à l'appareil pourrait utiliser le mode développeur·euse pour télécharger des copies de données privées de l'appareil ou pour installer des applications malveillantes.
>
> Si vous voyez un menu "Mode développeur" dans les paramètres de votre appareil, vous devez vous assurer que l'accès ADB est désactivé.
>
> #### Google Play Protect
>
> Le service Google Play Protect est disponible sur tous les appareils Android récents. Il effectue des analyses régulières de toutes les applications installées sur votre appareil. Play Protect peut également supprimer automatiquement toutes les applications malveillantes connues de votre appareil. L'activation de ce service envoie des informations à propos de votre appareil (applications installées) à Google.
>
> Google Play Protect peut être activé dans les paramètres de sécurité de votre appareil. Plus d'informations sont disponibles sur le site [Play Protect](https://www.android.com/intl/fr_fr/play-protect/).

Avez-vous identifié des réglages peu sûrs ?

- [Non](#android-bootloader)
- [Oui, j'ai identifié des réglages potentiellement peu sûrs](#android-badend)


### android-bootloader

> Le chargeur d'amorçage (ou bootloader) Android est un logiciel clé qui s'exécute dès que vous allumez votre appareil. Le chargeur d'amorçage permet au système d'exploitation de démarrer et d'utiliser le matériel. Un chargeur d'amorçage compromis donne à un·e attaquant·e un accès complet au périphérique. La majorité des fabricants livrent leurs appareils avec un chargeur d'amorçage verrouillé. Une façon courante de savoir si le chargeur d'amorçage signé du fabricant a été modifié est de redémarrer votre appareil et de rechercher le logo d'amorçage. Si un triangle jaune avec un point d'exclamation apparaît, c'est que le chargeur d'amorçage original a été remplacé. Votre appareil peut également être compromis s'il affiche un écran d'avertissement de chargeur d'amorçage déverrouillé et que vous ne l'avez pas déverrouillé vous-même pour installer une ROM Android personnalisée telle que LineageOS or GrapheneOS. Vous devez effectuer une réinitialisation d'usine de votre périphérique s'il affiche un écran d'avertissement de chargeur d'amorçage déverrouillé auquel vous ne vous attendez pas.

Le chargeur d'amorçage est-il compromis ou votre périphérique utilise-t-il le chargeur d'amorçage d'origine ?

- [Le chargeur d'amorçage sur mon périphérique est compromis](#android-badend)
- [Mon périphérique utilise le chargeur d'amorçage d'origine](#android-goodend)


### android-goodend

> Il ne semble pas que votre appareil ait été compromis.

Êtes-vous toujours inquiet que votre appareil soit compromis ?

- [Oui, j'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Non, j'ai résolu mes problèmes](#resolved_end)


### android-badend

> Votre appareil peut être compromis. Une réinitialisation aux paramètres d'usine supprimera probablement toute menace présente sur votre appareil. Cependant, ce n'est pas toujours la meilleure solution. De plus, vous voudrez peut-être enquêter davantage sur la question pour déterminer votre niveau d'exposition et la nature précise de l'attaque que vous avez subie.
>
> Vous pouvez utiliser un outil d'autodiagnostic appelé [VPN d'urgence](https://www.civilsphereproject.org/emergency-vpn), ou [PiRogue](https://pts-project.org/), ou demander l'assistance d'une organisation qui peut vous aider.

Souhaitez-vous obtenir de l'aide supplémentaire ?

- [Je souhaite réinitialiser mon appareil aux paramètres d'usine](#reset)
- [J'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Je dispose d'un réseau de soutien local auquel je peux m'adresser pour obtenir une aide professionnelle](#resolved_end)


### ios-intro

> Suivez ces étapes pour [savoir qui a accès à votre iPhone ou iPad](https://support.apple.com/fr-fr/guide/personal-safety/ipsb8deced49/web). Vérifiez les paramètres de l'iOS pour voir s'il y a quelque chose d'inhabituel.
>
> Dans l'application Paramètres, vérifiez que votre appareil est lié à votre Apple ID. Le premier élément dans le menu à gauche doit être votre nom ou le nom que vous utilisez pour votre Apple ID. Cliquez dessus et vérifiez que l'adresse e-mail correcte est affichée. Au bas de cette page, vous trouverez une liste avec les noms et modèles de tous les périphériques iOS liés à cet Apple ID.
>
> Vérifiez que votre appareil n'est pas lié à un système de gestion des appareils mobiles (MDM) que vous ne reconnaissez pas. Allez dans Réglages > Général > VPN et gestion des appareils et vérifiez si vous avez une section pour les profils. Vous pouvez [supprimer tout profil de configuration inconnu](https://support.apple.com/fr-fr/guide/personal-safety/ips41ef0e8c3/1.0/web/1.0#ips68379dd2e). Si aucun profil n'est répertorié, c'est que vous n'êtes pas inscrit au système MDM.

 - [Toutes les informations sont correctes et je contrôle toujours mon Apple ID](#ios-goodend)
 - [Le nom ou d'autres détails sont incorrects ou je vois des périphériques ou des profiles dans la liste qui ne sont pas les miens](#ios-badend)


### ios-goodend

> Il ne semble pas que votre appareil ait été compromis.

Êtes-vous toujours inquiet que votre appareil soit compromis ?

- [Oui, j'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Non, j'ai résolu mes problèmes](#resolved_end)


### ios-badend

> Votre appareil peut être compromis. Une réinitialisation aux paramètres d'usine supprimera probablement toute menace présente sur votre appareil. Cependant, ce n'est pas toujours la meilleure solution. De plus, vous voudrez peut-être enquêter davantage sur la question pour déterminer votre niveau d'exposition et la nature précise de l'attaque que vous avez subie.
>
> Vous pouvez utiliser un outil d'autodiagnostic appelé [VPN d'urgence](https://www.civilsphereproject.org/emergency-vpn), ou [PiRogue](https://pts-project.org/), ou demander l'assitance d'une organisation qui peut vous aider.

Que souhaitez-vous faire ?

- [Je souhaite réinitialiser mon appareil aux paramètres d'usine](#reset)
- [J'aimerais obtenir de l'aide professionnelle](#malware_end)
- [Je dispose d'un réseau de soutien local auquel je peux m'adresser pour obtenir une aide professionnelle](#resolved_end)

### computer-intro

> **Note : Si vous êtes victime d'une attaque de type "ransomware", rendez-vous directement sur le site [No More Ransom !](https://www.nomoreransom.org/fr/index.html).**
>
> Ce déroulé va vous aidera à enquêter sur les activités suspectes de votre ordinateur. Si vous aidez une personne à distance, vous pouvez essayer de suivre les étapes décrites dans les liens ci-dessous en utilisant un logiciel de prise de contrôle du bureau à distance comme [TeamViewer](https://www.teamviewer.com), ou vous pouvez consulter des outils d'investigation à distance comme [Google Rapid Response (GRR)](https://github.com/google/grr). Gardez à l'esprit que le temps de latence et la fiabilité du réseau seront essentiels pour y parvenir correctement.

Veuillez sélectionner votre système d'exploitation :

 - [J'ai un ordinateur Windows](#windows-intro)
 - [J'ai un ordinateur Mac](#mac-intro)


### windows-intro

> Vous pouvez suivre ce guide d'introduction pour enquêter sur les activités suspectes sur les périphériques Windows :
>
> [Comment faire un examen des activités suspectes sur Windows par Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

Ces instructions vous ont-elles aidé à identifier une activité malveillante ?

 - [Oui, je pense que l'ordinateur est infecté](#device-infected)
 - [Non, aucune activité malveillante n'a été identifiée](#device-clean)


### mac-intro

> Pour identifier une infection potentielle sur macOS, vous devez suivre les étapes suivantes :
>
> 1. Vérifier si des programmes suspects démarrent automatiquement
> 2. Vérifier les processus suspects en cours d'exécution
> 3. Vérifiez les extensions suspectes du noyau
>
>
> Le site Web [Objective-See](https://objective-see.com) fournit plusieurs utilitaires gratuits qui facilitent ce processus :
>
> [KnockKnock](https://objective-see.com/products/knockknock.html) peut être utilisé pour identifier tous les programmes qui sont configurés pour démarrer automatiquement.
> [TaskExplorer](https://objective-see.com/products/taskexplorer.html) peut être utilisé pour vérifier les processus en cours d'exécution et identifier ceux qui semblent suspects (par exemple parce qu'ils ne sont pas signés, ou parce qu'ils sont identifiés par VirusTotal).
> [KextViewr](https://objective-see.com/products/kextviewr.html) peut être utilisé pour identifier toute extension suspecte du noyau qui est chargée sur un macOS.
>
> Dans le cas où ceux-ci ne révèlent rien de suspect dans l'immédiat et que vous souhaitez effectuer d'autres recherches, vous pouvez utiliser [pcQF](https://github.com/botherder/pcqf). pcQF est un utilitaire qui simplifie le processus de collecte d'informations sur le système et la réalisation d'un instantané complet de la mémoire.
>
> Un outil supplémentaire qui pourrait être utile pour recueillir plus de détails (mais qui nécessite une certaine familiarité avec les commandes du terminal) est [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) de la société américaine de cybersécurité CrowdStrike.

Ces instructions vous ont-elles aidé à identifier une activité malveillante ?

 - [Oui, je pense que l'ordinateur est infecté](#device-infected)
 - [Non, aucune activité malveillante n'a été identifiée](#device-clean)

### device-infected

Oh non ! Votre appareil semble être infecté. Pour vous débarrasser de l'infection, vous pouvez :

- [Demander de l'aide supplémentaire](#malware_end)
- [Procéder directement à une réinitialisation de l'appareil](#reset).

### reset

> Vous voudrez peut-être réinitialiser votre appareil comme mesure de précaution supplémentaire. Les guides ci-dessous vous fourniront les instructions appropriées pour votre type d'appareil :
>
> - [Android](https://support.google.com/android/answer/6088915?hl=fr)
> - [iOS](https://support.apple.com/fr-fr/HT201252)
> - [Windows](https://support.microsoft.com/fr-fr/windows/r%C3%A9installer-windows-d8369486-3e33-7d9c-dccc-859e2b022fc7)
> - [Mac](https://support.apple.com/fr-fr/HT201314)

Avez-vous besoin d'aide supplémentaire ?

- [Oui](#malware_end)
- [Non](#resolved_end)


### malware_end

Si vous avez besoin d'aide supplémentaire pour traiter un dispositif infecté, vous pouvez vous adresser aux organisations listées ci-dessous.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Nous espérons que ce guide de la trousse de premiers soins numériques vous a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Derniers_conseils

Voici quelques conseils pour vous éviter d'être la proie d'un·e attaquant·e qui tente de compromettre vos appareils et vos données :

- Vérifiez toujours deux fois la légitimité de tout email que vous recevez, d'un fichier que vous avez téléchargé ou d'un lien vous demandant les détails de connexion de votre compte.
- Pour en savoir plus sur la façon de protéger votre appareil contre les infections par des logiciels malveillants, consultez les guides en lien dans les ressources.

#### Resources
- [Sécurité - auto-défense: Comment éviter les attaques par hameçonnage](https://ssd.eff.org/fr/module/guide-pratique-%C3%A9viter-les-attaques-par-hame%C3%A7onnage)
- [Totem: Le Hameçonnage](https://learn.totem-project.org/courses/course-v1:Totem+TP_PM_FR001+cours/about)
En anglais
- Security in a Box: [Protect your device from malware and phishing attacks](https://securityinabox.org/fr/phones-and-computers/malware)
  - Further tips for protecting [Android](https://securityinabox.org/fr/phones-and-computers/android), [iOS](https://securityinabox.org/fr/phones-and-computers/ios), [Windows](https://securityinabox.org/fr/phones-and-computers/windows), [MacOS](https://securityinabox.org/fr/phones-and-computers/mac), and [Linux](https://securityinabox.org/fr/phones-and-computers/linux) devices
- [Security without borders: Guide to Phishing](https://github.com/securitywithoutborders/guide-to-phishing/blob/master/SUMMARY.md)
