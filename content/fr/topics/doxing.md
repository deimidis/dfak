---
layout: page
title: "J'ai été victime d'un "doxing" ou quelqu'un partage mes informations privées ou mes médias sans mon consentement."
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa, Alex Argüelles
language: fr
summary: "Private information about me or media featuring my likeness is being circulated. It is distressing to me or could potentially be harmful"
date: 2023-05
permalink: /fr/topics/doxing
parent: /fr/
---

# Someone is sharing my private information or media without my consent

Les harceleurs peuvent parfois publier des informations qui vous identifient personnellement ou qui exposent des aspects de votre vie personnelle que vous avez choisi de garder privés. C'est ce qu'on appelle parfois le "doxing", ou "doxage" (terme abrégé pour "publication de documents").

L'objectif du doxing est d'embarrasser la cible ou de la mettre en difficulté. Cela peut avoir de graves conséquences sur votre bien-être psychosocial, votre sécurité personnelle, vos relations et votre travail.

Le doxing est utilisé pour intimider des personnalités publiques, des journalistes, des défenseurs des droits de l'homme et des activistes, mais il vise aussi fréquemment des personnes appartenant à des minorités sexuelles ou à des populations marginalisées. Il est étroitement lié à d'autres formes de violence en ligne et de violence sexiste.

La personne qui vous envoie un doxx peut avoir obtenu vos documents à partir de diverses sources. Elle peut notamment s'être introduite dans vos comptes et appareils personnels. Mais il est également possible qu'elle expose et sorte de leur contexte des informations ou des médias que vous pensiez partager en privé avec vos amis, partenaires, connaissances ou collègues. Dans certains cas, les harceleurs peuvent rassembler des informations publiques vous concernant provenant des médias sociaux ou des archives publiques. Le fait de rassembler ces informations d'une manière nouvelle et mal interprétée peut également constituer un acte de doxing.

Si vous avez été victime de doxing, vous pouvez utiliser le questionnaire suivant pour identifier où le harceleur a trouvé vos informations et trouver des moyens de limiter les dégâts, notamment en supprimant les documents des sites web.

## Workflow

### be_supported

> La documentation est fondamentale pour évaluer les impacts, identifier l'origine de ces attaques et développer des réponses qui rétablissent votre sécurité. En fonction de ce qui s'est passé, le processus de documentation peut renforcer l'impact émotionnel de cette agression sur vous. Pour alléger le fardeau de ce travail et réduire le stress émotionnel potentiel, vous aurez peut-être besoin du soutien d'un ami proche ou d'une autre personne de confiance pendant que vous documentez ces agressions.

Pensez-vous pouvoir compter sur une personne de confiance qui pourra vous aider en cas de besoin ?

 - [Oui, une personne de confiance est prête à m'aider](#physical_risk)

### physical_risk

> Réfléchissez : Qui, selon vous, peut être responsable de ces attaques ? Quelles sont leurs capacités ? Dans quelle mesure sont-ils déterminés à vous nuire ? Quelles sont les autres parties susceptibles d'être motivées pour vous nuire ? Ces questions vous aideront à évaluer les menaces en cours et à déterminer si vous êtes exposé·e à des risques physiques.

Craignez-vous pour votre intégrité physique ou votre bien-être, ou pensez-vous être exposé·e à un risque juridique ?

 - [Oui, je pense être exposé·e à des risques physiques](#yes_physical_risk)
 - [Je pense être exposé·e à des risques juridiques](#egal_risk)
 - [Non, j'aimerais résoudre mon problème dans la sphère numérique](#takedown_content)
 - [Je ne suis pas sûr, j'ai besoin d'aide pour évaluer mes risques](#assessment_end)

### yes_physical_risk

> Si vous pensez que vous pourriez être la cible d'attaques personnelles ou que votre intégrité physique ou votre bien-être est menacé, vous pouvez consulter les ressources suivantes pour réfléchir à vos besoins immédiats en matière de sécurité physique.
>
> - [Front Line Defenders - Manuel de Sécurité](https://www.frontlinedefenders.org/fr/manuel-de-s%C3%A9curit%C3%A9)
> - [Comment se protéger et protéger nos luttes - Premiers pas dans la mise en place de pratiques de sécurité](https://infokiosques.net/spip.php?page=lire&id_article=2017)
> - [National Domestic Violence Hotline - Create a Safety Plan](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/)
> - [Coalition against Online Violence - Physical Security Support](https://onlineviolenceresponsehub.org/physical-security-support)
> - [Cheshire Resilience Forum - How to Prepare for an Emergency](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/)
> - [FEMA Form P-1094 Create Your Family Emergency Communication Plan](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html)
> - [Reolink - How to Cleverly Secure Your Home Windows — Top 9 Easiest Security Solutions](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/)
> - [Reolink - How to Make Your Home Safe from Break-ins](https://reolink.com/blog/make-home-safe-from-break-ins/)
> - [Seguridad integral para periodistas - Seguridad Física](https://seguridadintegral.articulo19.org/categorias-prevencion/seguridad-fisica/) (in Spanish)

Avez-vous besoin de plus d'aide pour assurer votre intégrité physique et votre bien-être ?

 - [Oui](#physical-risk_end)
 - [Non, mais je pense que je suis également exposé à un risque juridique](#legal_risk)
 - [Non, j'aimerais résoudre mon problème dans la sphère numérique](#takedown_content)
 - [Je ne suis pas sûr, j'ai besoin d'aide pour évaluer mes risques](#assessment_end)

### legal_risk

> Si vous envisagez d'engager une action en justice, il sera très important de conserver des preuves des attaques dont vous avez fait l'objet. Il est donc fortement recommandé de suivre les [recommandations de la page de la trousse de premiers soins numériques relative à l'enregistrement d'informations sur les attaques](/../../documentation).

Avez-vous besoin d'une assistance juridique immédiate ?

 - [Oui](#legal_end)
 - [Non, j'aimerais résoudre mon problème dans la sphère numérique](#takedown_content)
 - [Non](#resolved_end)

### takedown_content

Voulez-vous travailler à ce que les documents partagés de manière inappropriée soient retirés de la vue du public ?

 - [Oui](#documenting)
 - [Non](#resolve_end)

### documenting

> Il est très important de conserver votre propre documentation sur l'attaque dont vous avez été victime. Il est ainsi plus facile pour les juristes ou les techniciens de vous aider. Consultez le [Guide de la trousse de premiers soins numériques sur la documentation des attaques en ligne](/../../documentation) pour savoir comment rassembler au mieux les informations dont vous avez besoin.
>
> Vous pouvez également demander à une personne de confiance de vous aider à rechercher des preuves de l'attaque dont vous avez été victime. Vous pourrez ainsi vous assurer que vous ne serez pas traumatisé·e à nouveau en examinant les documents publiés.

Vous sentez-vous prêt à documenter l'agression de manière sécurisée ?

- [Oui, je documente les attaques](#where_published)

### where_published

> Selon l'endroit où l'auteur de l'attaque a diffusé publiquement vos informations, vous pouvez prendre différentes mesures.
>
> Si vos informations ont été publiées sur des plateformes de médias sociaux basées dans des pays dont les systèmes juridiques définissent les responsabilités légales des entreprises à l'égard des personnes qui utilisent leurs services (par exemple, l'Union européenne ou les États-Unis), il est plus probable que les modérateurs de l'entreprise soient disposés à vous aider. Ils vous demanderont de [documenter](#documentation) les preuves des attaques. Vous pouvez rechercher le pays dans lequel une entreprise est basée sur Wikipédia.
>
> Parfois, les adversaires peuvent publier vos informations sur des sites plus petits et moins connus, et créer des liens vers ces sites à partir des médias sociaux. Parfois, ces sites sont hébergés dans des pays où il n'existe aucune protection juridique, ou ils sont gérés par des personnes ou des groupes qui pratiquent, approuvent ou ne s'opposent tout simplement pas à des comportements préjudiciables. Dans ce cas, il peut être plus difficile de contacter les sites où le matériel est publié ou de découvrir qui les gère.
>
> Mais même lorsque l'auteur de l'attaque a publié vos documents ailleurs que dans les médias sociaux populaires (ce qui est souvent le cas), s'efforcer de supprimer ces documents ou les liens qui y mènent peut réduire considérablement le nombre de personnes qui voient vos informations. Cela peut également réduire le nombre et la gravité des attaques auxquelles vous pourriez être confronté·e.

Mes informations ont été publiées sur

- [une plateforme ou un service réglementé par la loi](#what_info_published)
- [une plateforme ou un service qui n'est pas susceptible de répondre aux demandes de retrait](#legal_advocacy)

### what_info_published

Quelles sont les informations qui ont été partagées sans consentement ?

- [informations personnelles qui m'identifient ou qui sont privées : adresse, numéro de téléphone, numéro d'identité nationale, compte bancaire, etc.](#personal_info)
- [des médias que je n'ai pas consenti à partager, y compris des documents intimes (photos, vidéos, audio, textes)](#media)
- [un pseudonyme que j'utilise a été relié à mon identité réelle](#linked_identities)

### linked_identities

> Si un agresseur établit publiquement un lien entre votre nom ou votre identité réelle et un pseudonyme, un surnom ou un pseudo que vous utilisez lorsque vous vous exprimez, que vous rendez votre opinion publique, que vous vous organisez ou que vous vous engagez dans l'activisme, envisagez de fermer les comptes liés à cette identité afin de réduire les dommages.

Avez-vous besoin de continuer à utiliser cette identité/personne pour atteindre vos objectifs, ou pouvez-vous fermer le(s) compte(s) ?

- [Je dois continuer à l'utiliser](#defamation_flow_end)
- [Je peux fermer le(s) compte(s)](#close)

### close

> Avant de fermer les comptes associés à l'identité ou au pseudonyme concerné, évaluez les risques que cela comporte. Vous pourriez perdre l'accès à des services ou à des données, voir votre réputation menacée, perdre le contact avec vos partenaires en ligne, etc.

- [J'ai fermé les comptes concernés](#resolved_end)
- [J'ai décidé de ne pas fermer les comptes concernés pour le moment](#resolved_end)
- [J'ai besoin d'aide pour comprendre ce que je dois faire](#harassment_end)

### personal_info

Où vos informations personnelles ont-elles été publiées ?

- [Sur une plateforme de réseau social](#doxing-sn)
- [Sur un site web](#doxing-web)

### doxing-sn

> Si vos informations privées ont été publiées sur une plateforme de médias sociaux, vous pouvez signaler une violation des normes de la communauté en suivant les procédures de signalement fournies aux utilisateurs par les sites de réseaux sociaux.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant d'entreprendre des actions telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Vous trouverez des instructions pour signaler une violation des normes de la communauté aux principales plateformes dans la liste suivante :
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce processus de travail dans quelques jours.

Les informations ont-elles été supprimées ?

 - [Oui](#one-more-persons)
 - [Non](#harassment_end)

### doxing-web

> Vous pouvez essayer de signaler le site web à l'hébergeur ou au bureau d'enregistrement de noms de domaine, en demandant qu'il soit retiré.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant d'entreprendre des actions telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Pour envoyer une demande de retrait, vous devrez également recueillir des informations sur le site web où vos informations ont été publiées :
>
> - Allez sur [Network Tools' NSLookup service](https://network-tools.com/nslookup/) et trouvez l'adresse IP (ou les adresses) du faux site web en entrant son URL dans le formulaire de recherche.
> - Notez l'adresse ou les adresses IP.
> Allez sur [Domain Tools' Whois Lookup service](https://whois.domaintools.com/) et recherchez le domaine et l'adresse ou les adresses IP du faux site web.
> Enregistrez le nom et l'adresse électronique de l'hébergeur et du service de domaine. Si cela figure dans les résultats de votre recherche, notez également le nom du propriétaire du site web.
> Écrivez au fournisseur d'hébergement et au bureau d'enregistrement de noms de domaine du faux site web pour demander son retrait. Dans votre message, indiquez l'adresse IP, l'URL et le propriétaire du site usurpé, ainsi que les raisons pour lesquelles il est abusif.
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce processus de travail dans quelques jours.

Le contenu a-t-il été supprimé ?

- [Oui](#resolved_end)
- [Non, la plateforme n'a pas répondu / aidé](#platform_help_end)
- [J'ai besoin d'aide pour savoir comment envoyer une demande de retrait](#platform_help_end)

### media

> Si vous avez créé le média vous-même, vous en possédez généralement les droits d'auteur. Dans certaines juridictions, les individus ont également des droits sur les médias dans lesquels ils apparaissent (connus sous le nom de droit à l'image).

Êtes-vous titulaire des droits d'auteur sur les médias ?

- [Oui](#intellectual_property)
- [Non](#nude)

### intellectual_property

> Vous pouvez recourir à la réglementation sur les droits d'auteur, telle que le [Digital Millennium Copyright Act (DMCA)](https://fr.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), pour retirer un contenu. La plupart des plateformes de médias sociaux proposent des formulaires pour signaler les violations de droits d'auteur et peuvent être plus réceptives à ce type de demande qu'à d'autres considérations de retrait, en raison des implications juridiques pour elles.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant de demander le retrait d'un contenu. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Vous pouvez utiliser ces liens pour envoyer une demande de retrait pour violation des droits d'auteur aux principales plateformes de réseaux sociaux :
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/fr/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622)
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce proessus de travail dans quelques jours.

Le contenu a-t-il été supprimé ?

 - [Oui](#resolved_end)
 - [Non, j'ai besoin d'une assistance juridique](#legal_end)
 - [Je n'ai pas trouvé les plateformes concernées dans la liste des ressources.](#platform_help_end)

### nude

> Les médias qui montrent des corps nus ou des détails de ces corps sont parfois couverts par des politiques spécifiques de la plateforme.

Les médias montrent-ils des corps nus ou des détails de corps nus ?

- [Oui](#intimate_media)
- [Non](#nonconsensual_media)

### intimate_media

> Pour savoir comment signaler une violation des règles de confidentialité ou le partage non consenti de médias intimes/contenus préjudiciables sur les plateformes les plus populaires, vous pouvez consulter les ressources suivantes :
>
> - [Stop NCII](https://stopncii.org/) (Facebook, Instagram, TikTok and Bumble - worldwide)
> - [Revenge Porn Helpline - Help for Victims Outside the UK](https://revengepornhelpline.org.uk/how-can-we-help/if-we-can-t-help-who-can/help-for-victims-outside-the-uk/)
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant d'entreprendre des actions telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce processus de travail dans quelques jours.

Le contenu a-t-il été supprimé ?

 - [Oui](#resolved_end)
 - [Non, la plateforme n'a pas répondu / aidé](#platform_help_end)
 - [Je n'ai pas trouvé les plateformes concernées dans la liste des ressources.](#platform_help_end)

### nonconsensual_media

Où vos médias ont-ils été publiés ?

- [Sur une plateforme de réseau social](#NCII-sn)
- [Sur un site web](#NCII-web)

### NCII-sn

> Si vos informations privées ont été publiées sur une plateforme de médias sociaux, vous pouvez signaler une violation des normes de la communauté en suivant les procédures de signalement fournies aux utilisateurs par les sites de réseaux sociaux.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant d'entreprendre des actions telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Vous trouverez des instructions pour signaler une violation des normes de la communauté aux principales plateformes dans la liste suivante :
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce processus de travail dans quelques jours.

Le contenu a-t-il été supprimé ?

 - [Oui](#resolved_end)
 - [Non, la plateforme n'a pas répondu / aidé](#platform_help_end)
 - [Je n'ai pas trouvé les plateformes concernées dans la liste des ressources.](#platform_help_end)

### NCII-web


> Suivez les instructions de "Without My Consent - Take Down"](https://withoutmyconsent.org/resources/take-down) pour supprimer le contenu d'un site web.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant d'entreprendre des actions telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce processus de travail dans quelques jours.

Le contenu a-t-il été supprimé ?

 - [Oui](#resolved_end)
 - [Non, la plateforme n'a pas répondu / aidé](#platform_help_end)
 - [Je n'ai pas trouvé les plateformes concernées dans la liste des ressources.](#platform_help_end)

### legal_advocacy

> Dans certains cas, les plateformes sur lesquelles ont lieu le doxing ou la publication non consensuelle de médias intimes ne disposent pas de politiques ou d'un processus de modération suffisamment mûrs pour traiter les demandes concernant le doxing, le harcèlement ou la diffamation.
>
> Parfois, le matériel offensant est publié ou hébergé sur des plateformes ou des applications gérées par des personnes ou des groupes qui pratiquent, approuvent ou ne s'opposent tout simplement pas à un comportement préjudiciable.
>
> Il existe également des endroits dans le cyberespace où les lois et règlements ordinaires sont difficilement applicables, car ils sont spécifiquement créés pour fonctionner dans l'anonymat et ne pas laisser de traces. L'un de ces endroits est ce que l'on appelle le "dark web". La mise à disposition d'espaces anonymes de ce type présente des avantages, même si certaines personnes en abusent.
>
> Dans des situations comme celles-ci, lorsqu'il n'est pas possible d'intenter une action en justice, réfléchissez à la possibilité de porter une parole publique. Il peut s'agir de faire publiquement la lumière sur votre cas, de susciter un débat public à son sujet et, éventuellement, de faire émerger des cas similaires. Il existe des organisations et des groupes de défense des droits qui peuvent vous aider dans cette démarche. Ils peuvent vous aider à définir vos attentes en matière de résultats et vous donner une idée des ramifications.
>
> Demander une aide juridique peut être une solution de dernier recours si la communication avec la plateforme, l'application ou le fournisseur de services échoue ou est impossible.
>
> La voie juridique prend généralement du temps, coûte de l'argent et peut nécessiter que vous fassiez savoir que vous avez été victime de doxing ou que vos médias intimes ont été publiés sans votre consentement. La réussite d'une action en justice dépend de nombreux facteurs, notamment de la [documentation](#documentation#legal) de l'agression d'une manière jugée acceptable par les tribunaux, et du cadre juridique qui régit l'affaire. Il peut également être difficile d'identifier les auteurs de l'agression ou de déterminer leur responsabilité. Il peut également être difficile d'établir la juridiction dans laquelle l'affaire doit être jugée.

Qu'aimeriez-vous faire ?

- [J'ai besoin d'aide pour planifier une campagne sensibilisation de l'opinion publique](#advocacy_end)
- [J'ai besoin d'un soutien juridique](#legal_end)
- [J'ai le soutien de ma communauté locale](#resolved_end)

### advocacy_end

> Si vous souhaitez contrecarrer les informations vous concernant qui ont été diffusées en ligne sans votre volonté, nous vous suggérons de suivre les recommandations du [questionnaire de la trousse de premiers soins numériques sur les campagnes de diffamation](../../../defamation).
>
> Si vous souhaitez obtenir un soutien pour lancer une campagne visant à dénoncer vos agresseurs, vous pouvez prendre contact avec des organisations qui peuvent vous aider dans vos efforts de sensibilisation :

:[](organisations?services=advocacy)


### legal_end

> Si vous avez besoin d'un soutien juridique, veuillez contacter les organisations ci-dessous qui peuvent vous aider.
>
> Si vous envisagez d'engager une action en justice, il sera très important de conserver des preuves des attaques dont vous avez été victime. Il est donc fortement recommandé de suivre les [recommandations de la trousse de premiers soins numériques sur relatives à l'enregistrement d'informations sur les attaques] (/../../documentation).

:[](organisations?services=legal)

### physical-risk_end

> Si vous êtes en danger physique et avez besoin d'une aide immédiate, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=physical_security)


### assessment_end

> Si vous pensez avoir besoin d'aide pour évaluer les risques que vous encourez parce que vos informations ou vos médias ont été partagés sans votre autorisation, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=harassment&services=assessment)

### resolved_end

> Nous espérons que ce guide de dépannage vous a été utile. N'hésitez pas à nous faire part de vos commentaires [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

> Si vous souhaitez contrecarrer les informations vous concernant qui ont été diffusées en ligne sans votre volonté, nous vous suggérons de suivre les recommandations [de la trousse de premiers soins numériques sur les campagnes de diffamation](../../../defamation).
>
> Si vous avez besoin d'un soutien spécialisé, la liste ci-dessous comprend des organisations qui peuvent vous aider à remédier aux atteintes à la réputation.

:[](organisations?services=advocacy&services=legal)

### platform_help_end

> Vous trouverez ci-dessous une liste d'organisations qui peuvent vous aider à faire des rapports aux plateformes et services en ligne.

:[](organisations?services=harassment&services=legal)

### harassment_end

> Si vous avez besoin d'aide pour faire face à votre situation, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=harassment&services=triage)


### final_tips

- Cartographiez votre présence en ligne. L'autodoxage consiste à explorer les sources ouvertes de renseignements sur soi-même afin d'empêcher les acteurs malveillants de trouver et d'utiliser ces informations pour usurper votre identité. Pour en savoir plus sur la manière de rechercher vos traces en ligne, consultez le [Guide d'Access Now Helpline pour prévenir le doxing (en anglais)](https://guides.accessnow.org/self-doxing.html).
- Si le doxing implique des informations ou des documents que vous seul, ou quelques personnes de confiance, auriez dû connaître ou auxquels vous auriez dû avoir accès, vous pouvez également réfléchir à la manière dont le doxer y a accédé. Passez en revue vos pratiques en matière de sécurité des informations personnelles afin d'améliorer la sécurité de vos appareils et de vos comptes.
- Vous pouvez être vigilant dans un avenir proche, au cas où le même matériel, ou du matériel connexe, réapparaîtrait sur l'internet. Une façon de vous protéger est de rechercher votre nom ou les pseudonymes que vous avez utilisés dans les moteurs de recherche, pour voir où ils peuvent apparaître. Vous pouvez créer une alerte Google pour vous-même ou utiliser Meltwater ou Mention. Ces services vous préviendront également lorsque votre nom ou votre pseudonyme apparaîtra sur l'internet.

#### resources

- [Totem Project - Protège ta vie privée !](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_FR+001/about)
- [Totem Project - Comment protéger votre identité en ligne](https://learn.totem-project.org/courses/course-v1:Totem+TP_IO_FR+01/about)
- [La sécurité numérique : la suppression des données personnelles de l’Internet](https://cpj.org/fr/2019/09/securite-numerique-retirer-les-donnees-personnelle/)
- [Crash Override Network - So You’ve Been Doxed: A Guide to Best Practices](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices)
- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Ken Gagne, Doxxing defense: Remove your personal info from data brokers](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html)
- [Coalition against Online Violence - I've been doxxed](https://onlineviolenceresponsehub.org/for-journalists#doxxed)
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
