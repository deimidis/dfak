---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: "երկուշաբթիից հինգշաբթի 09:00-17:00 ԿԵԺ"
response_time: "4 օր"
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership-ն աջակցում է թվային սպառնալիքի տակ գտնվող մարդու իրավունքների պաշտպաններին և փորձում է ուժեղացնել տեղական շտապ արձագանքման ցանցերը։ DDP-ն շտապ օգնություն է տրամադրում անհատներին ու կազմակերպություններին, ինչպիսիք են մարդու իրավունքի պաշտպանները, լրագրողները, քաղաքացիական հասարակության ակտիվիստները և բլոգերները։

DDP-ն ունի արտակարգ իրավիճակներին վերաբերվող հինգ տարբեր ֆինանսավորման ծրագրեր, ինչպես նաև երկարաժամկետ դրամաշնորհներ՝ ուղղված կազմակերպությունների ներքին բազայի զարգացմանը։ DDP-ն նաև համակարգում է Digital Integrity Fellowship ծրագիրը, որը կազմակերպություններին տրամադրում է թվային անվտանգության և գաղտնիության նպատակային աջակցություն, դասընթացներ, ու արագ արձագանքման ծրագիր։
