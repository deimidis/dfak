---
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: "շաբաթը 7 օր, 09:00-17:00 ՀԿԺ+5 (PST)"
response_time: "56 ժամ"
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

2012-ից Պակիստանում գործում է Digital Rights Foundation-ը (DRF-ը)։ DRF-ը Հասարակական կազմակերպություն է, որը պայքարում է շահերի պաշտպանության համար՝ հիմնվելով հետազոտությունների վրա։ Կազմակերպությունը հիմնականում տեղեկատվական հաղորդակցության տեխնոլոգիաներում (ՏՀՏ-ներում), մարդու իրավունքների, ներառականության, ժողովրդավարական գործընթացների թվային պաշտպանությամբ է զբաղվում։ DRF-ը նաև զբաղվում է խոսքի ազատության, գաղտնիության, տվյալների պաշտպանության և կանանց նկատմամբ թվային բռնության հարցերով։
