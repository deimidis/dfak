---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: "երկուշաբթիից հինգշաբթի 09:00-17:00 EET/EEST"
response_time: "2 օր"
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine-ը 2017-ից գործող հասարակական կազմակերպություն է։ Այն հիմնված է Կիևում և նպատակ ունի նպաստել համացանցում մարդու իրավունքների պաշտպանությանը։ Կազմակերպությունն աշխատում է զարգացնել ՀԿ-ների և անկախ լրատվամիջոցների ներքին բազաները՝ իրենց թվային անվտանգության խնդիրները ինքնուրույն լուծելու համար։ Սա անելու համար, Digital Security Lab-ը կենտրոնանում է կազմակերպությունների թվային իրավունքների հետ առնչվող կառավարման մոդելների ու կորպորատիվ քաղաքականության վրա։
