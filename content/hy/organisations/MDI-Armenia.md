---
name: Բազմակողմանի տեղեկատվության ինստիտուտ - Հայաստան (Media Diversity Institute - Armenia)
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: "24 ժամ, GMT+4"
response_time: "3 ժամ"
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Հայաստանի Բազմակողմանի տեղեկատվության ինստիտուտը (Հայաստանի MDI-ը) շահույթ չհետապնդող հասարակական կազմակերպություն է, որը նպատակ ունի տեխնոլոգիաները, ավանդական ու սոցիալական մեդիաները, ծառայեցնել մարդու իրավունքների պաշտպանությանը, օգնել ժողովրդավար քաղաքացիական հասարակությունների ստեղծմանը, ինչպես նաև ինքնաարտահայտվելու հնարավորություն տալ նրանց, ովքեր այդ հնարավորությունը չունեն։ Ինստիտուտի հիմնական նպատակն է խորացնելը սոցիալական բազմազանությանը վերաբերվող հասարակության պատկերացումները։

Հայաստանի MDI-ը կապված է Լոնդոնի Media Diversity Institute-ի հետ, սակայն հանդիսանում է անկախ մարմին։
