---
layout: sidebar.pug
title: “Անձի և համայնքային խնամք”
author: Peter Steudtner, Flo Pagano
language: hy
summary: «Թվային ոտնձգությունը, սպառնալիքները, և այլ տեսակի թվային հարձակումները կարող են առաջ բերել ճնշող զգացումներ ու խոցելի էմոցիոնալ վիճակներ. Հնարավոր է, որ զգաք ձեզ մեղավոր, ամաչեք, անհանգստանաք, ջղայինանաք, չհասկանաք, թե ինչ է կատարվում, զգաք ձեզ անօգնական ու մինչև անգամ վախենաք ձեր հոգեբանական ու ֆիզիկական առողջության համար։»
date: 2023-04
permalink: /hy/self-care/
parent: Home
sidebar: >
  <h3>Կարդացեք ավելին այն մասին, թե ինչպես պաշտպանել ինքներդ ձեզ և ձեր թիմին ճնշող զգացումներից։</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care for Activists: Sustaining Your Most Valuable Resource</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Caring for yourself so you can keep defending human rights</a></li>
    <li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">Resources for Well-being & Stress Management</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care for People Experiencing Harassment</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Cyberwomen self-care training module</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Wellness and Community</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Twenty ways to help someone who's being bullied online</a></li>
    <li><a href="https://www.hrresilience.org/">Human Rights Resilience Project</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Totem-Project Online Learning Course: Taking care of your mental health (requires registration)</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">Totem-Project Online Learning Course: Psychological First Aid (requires registration)</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Individual Responses to Threat</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Team and Peer Responses to Threat</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Communicating ain Teams and Organisations</a></li>
    </ul>
---

# Ճնշվա՞ծ եք ձեզ զգում։

Թվային ոտնձգությունը, սպառնալիքները, և այլ տեսակի թվային հարձակումները կարող են առաջ բերել ճնշող զգացումներ ու խոցելի էմոցիոնալ վիճակներ. հնարավոր է, որ զգաք ձեզ մեղավոր, ամաչեք, անհանգստանաք, ջղայինանաք, չհասկանաք, թե ինչ է կատարվում, զգաք ձեզ անօգնական ու մինչև անգամ վախենաք ձեր հոգեբանական ու ֆիզիկական առողջության համար։ Այս զգացումները տարբեր դեպքերում կարող են տարբեր լինել։

Չկա զգալու «ճիշտ» ձև։ Ամեն մարդու անձնական տվյալներն ու դրանց խոցելիության աստիճանը տարբեր են և ամեն մեկի համար առաջացած անախորժ իրավիճակը տարբեր նշանակություն ունի։ Ցանկացած էմոցիա արդարացված է, չպետք է մտահոգվեք՝ թե արդյոք ձեր հուզական ռեակցիան ճիշտ է, թե՝ ոչ։

Առաջին բանը, որ պետք է հիշեք, դա այն է, որ ինքներդ ձեզ կամ թիմի անդամներին մեղադրելով հարցը չեք լուծի։ Կարող եք դիմել որևէ վստահված անձի, ով ձեզ տեխնիկապես ու էմոցիոնալ առումով կաջակցի։

Օնլայն հարձակում կանխելու համար պետք է հավաքել ինֆորմացիա դրա ընթացքի մասին։ Եթե ձեր կողքին կա մեկն, ում վստահում եք, ապա դա կարող եք անել նրա հետ միասին. հետևել մեր հրահանգներին, կիսվելով մուտքի թույլտվությամբ տվյալ անձի հետ։ Կարող եք նաև դիմել տեխնիկական ու հոգեբանական աջակցության այդ ընթացքում։

Եթե ձեզ կամ ձեր թիմին անհրաժեշտ է հոգեբանական աջակցություն թվային արտակարգ իրավիճակի ժամանակ կամ դրանից հետո,  դիմեք [Team-ի Համայնքային հոգեբանական բարեկեցության ծրագրին՝ CommUNITY-ին](https://www.communityhealth.team/)։ Այն տրամադրում է տարբեր ձևաչափերով հոգեբանական-սոցիալական ծառայություններ, շտապ իրավիճակների և ավելի ընդհանուր բարդությունների ժամանակ. տարբեր լեզուներով ու տարբեր համատեքստերում։
