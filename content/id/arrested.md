---
layout: sidebar.pug
title: "Seseorang yang saya kenal ditangkap"
author: Peter Steudtner, Shakeeb Al-Jabri
language: id
summary: "Seorang teman, kolega, atau anggota keluarga Anda ditangkap oleh aparat keamanan. Anda ingin membatasi dampak penangkapan tersebut terhadap mereka dan orang lain yang mungkin terlibat."
date: 2020-11
permalink: /id/arrested/
parent: Home
sidebar: >
  <h3>Baca selengkapnya tentang apa yang harus dilakukan jika seseorang yang Anda kenal ditangkap:</h3>

  <ul>
    <li><a href="https://coping-with-prison.org">Inspirasi dan panduan untuk tahanan beserta keluarga, pengacara, dan para pendukungnya</a></li>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Mendapatkan dukungan untuk kampanye atas nama tahanan</a></li>
    <li><a href="https://perpustakaan.bantuanhukum.or.id/index.php?p=show_detail&id=3531">Hak Tersangka di dalam KUHAP dari Lembaga Bantuan Hukum Jakarta</a></li>
  </ul>

---

# Seseorang yang saya kenal ditangkap

Penangkapan pembela HAM, jurnalis, dan aktivis menempatkan mereka serta siapapun yang bekerja dan tinggal dengan mereka dalam risiko tinggi.

Panduan ini ditujukan khususnya untuk negara-negara yang belum mumpuni dalam hal hak asasi manusia serta proses hukumnya, atau di mana pihak berwenang dapat menghindari proses hukum, atau badan non-pemerintahan dapat beroperasi dengan bebas dan penahanan atau penangkapan menyebabkan risiko yang lebih besar bagi korban serta kolega dan kerabat mereka.

Dalam panduan ini kami bertujuan untuk mengurangi bahaya yang dihadapi oleh orang yang ditangkap dan membatasi akses penahan ke data sensitif yang dapat memberatkan korban dan rekan mereka, atau yang dapat digunakan untuk menyalahgunakan operasi lain.

Mengurus tahanan serta dampak digital dari penahanan tersebut bisa jadi melelahkan dan menantang. Cobalah untuk mencari pihak lain untuk membantu Anda dan mengkoordinasikan tindakan Anda dengan komunitas yang terlibat.

Penting juga bagi Anda untuk menjaga diri Anda sendiri dan orang lain yang terdampak oleh penangkapan ini, dengan cara:

- [menjaga kesehatan dan kebutuhan psikososial Anda,](../self-care),
- mengurus aspek hukum dari aktivitas dukungan Anda - *Anda dapat menemukan organisasi yang menawarkan bantuan hukum di* [*halaman bantuan P2D2*](../support)
<!-- - [taking care of the legal side of your support work](***link to RaReNet section on organizations helping on legal matters***) -->
- [mencari bantuan untuk kampanye atas nama pihak yang ditahan](https://www.newtactics.org/search/solr/arrest)

Jika Anda merasa kewalahan secara teknis atau emosional, atau (untuk alasan apapun) tidak berada dalam posisi di mana Anda dapat mengikuti langkah-langkah yang diuraikan di bawah ini, silakan carilah bantuan dan bimbingan ke organisasi yang tercantum di [sini](../support). Mereka menawarkan asesmen tahap awal sebagai salah satu layanan mereka.


## Make a plan

Sebelum Anda menindaklanjuti bagian lain yang kami uraikan di bawah ini, silakan perhatikan langkah-langkah berikut:

- Usahakan untuk membaca panduan ini sampai selesai supaya mendapatkan gambaran umum dari semua area penting yang terdampak, sebelum Anda mulai mengambil tindakan atas aspek-aspek khusus. Alasannya adalah tiap-tiap bagian menyoroti skenario ancaman yang berbeda yang bisa jadi saling tumpang tindih, sehingga Anda perlu menyusun sendiri urutan tindakan Anda.
- Luangkan waktu bersama rekan-rekan atau tim untuk melakukan beragam asesmen risiko yang diperlukan di bagian-bagian yang berbeda.

<a name="harm-reduction"></a>
## Bersiap sebelum bertindak

Apakah Anda memiliki alasan untuk percaya bahwa penangkapan atau penahanan ini dapat mengakibatkan dampak lanjutan pada anggota keluarga, rekan, atau kolega dari tahanan, termasuk Anda sendiri?

Panduan ini akan memandu Anda melalui serangkaian langkah untuk membantu menyediakan solusi yang dapat menolong mengurangi eksposur terhadap tahanan dan siapapun yang terlibat dengan mereka.

### Tips for coordinated response

Dalam segala situasi di mana seseorang telah ditahan, hal pertama yang harus diingat adalah seringkali saat insiden terjadi, sejumlah rekan atau kolega akan bereaksi di saat yang bersamaan, yang menghasilkan upaya yang sama atau kontradiktif. Maka dari itu, penting untuk diingat bahwa tindakan yang terkoordinir dan berdasarkan mufakat, pada tataran lokal dan internasional, diperlukan untuk mendukung tahanan dan menjaga semua orang yang termasuk dalam jaringan pendukung, keluarga, dan rekan mereka.

- Bentuk tim krisis yang akan mengkoordinasi semua aktivitas dukungan, perawatan, kampanye, dll.
- Libatkan anggota keluarga, pasangan, dll. sebanyak mungkin (hormati batasan mereka, jika misalnya mereka kewalahan).
- Tetapkan tujuan yang jelas untuk kampanye dukungan Anda (dan tinjau sesering mungkin): misalnya, Anda ingin membebaskan tahanan, memastikan kesejahteraan mereka, atau melindungi keluarga dan para pendukung serta memastikan kesejahteraan mereka sebagai tujuan Anda yang paling mendesak.
- Sepakati saluran dan frekuensi komunikasi yang aman, beserta batasan-batasannya (misalnya dilarang berkomunikasi antara jam 10 malam hingga 8 pagi kecuali untuk situasi darurat atau berita terbaru).
- Bagikan tugas di antara anggota tim dan hubungi pihak ketiga untuk bantuan (analisis, advokasi, kerja media, dokumentasi, dll.)
- Minta bantuan lebih lanjut di luar “tim krisis” untuk memenuhi kebutuhan dasar (misalnya asupan makanan rutin, dll.)

### Digital security precautions

Jika Anda memiliki alasan untuk mengkhawatirkan dampak lanjutan bagi diri Anda sendiri atau anggota pendukung lain, sebelum menangani situasi darurat digital yang berkaitan dengan penahanan rekan Anda, penting pula bagi Anda untuk mengambil langkah-langkah pencegahan pada tingkat keamanan digital untuk melindungi diri Anda sendiri dan orang lain dari bahaya langsung.

- Sepakati saluran komunikasi (yang aman) yang akan digunakan oleh jaringan pendukung Anda untuk mengkoordinasikan penanggulangan dan berkomunikasi tentang tahanan dan dampak lebih lanjut.
- Kurangi data pada peranti Anda seminimal mungkin, dan lindungi data di peranti Anda dengan  [enkripsi](https://ssd.eff.org/en/module/keeping-your-data-safe#1).
- Buat [cadangan semua data Anda yang aman dan terenkripsi](https://communitydocs.accessnow.org/182-Secure_Backup.html), dan simpan di tempat yang tidak akan ditemukan saat pencarian atau penahanan lebih lanjut.
- Bagi kata sandi untuk peranti-peranti, akun-akun daring, dll dengan orang tepercaya yang tidak berada dalam bahaya langsung. [Pengelola kata sandi](https://securityinabox.org/id/passwords/passwords-and-2fa/#use-a-password-manager) can help you do this securely.
- Sepakati langkah yang akan diambil (seperti penangguhan akun, penghapusan peranti dari jarak jauh, dll.) sebagai reaksi pertama dari kemungkinan penahanan Anda.


## Risk assessment
### Reduce possible harm caused by our own actions

Secara umum, Anda harus mendasarkan tindakan Anda pada pertanyaan berikut:

- Apa dampak dari setiap maupun seluruh tindakan terhadap tahanan, dan juga terhadap komunitas, sesama aktivis, rekan, keluarga mereka, dll, termasuk Anda sendiri?

Masing-masing bagian berikut ini akan menguraikan aspek-aspek khusus dari penilaian risiko ini.

Pertimbangan dasarnya adalah:

- Sebelum menghapus akun, data, utas media sosial, dll., pastikan Anda telah mendokumentasikan konten dan informasi yang Anda sedang hapus, terutama jika Anda perlu memulihkan konten dan informasi tersebut nantinya, atau membutuhkannya untuk bukti di lain waktu.
- Jika Anda mengubah kata sandi, menghapus akun atau berkas, ketahuilah bahwa:
    - Pihak berwenang dapat menafsirkan ini sebagai penghancuran atau penghapusan bukti
    - Hal ini dapat membuat situasi tahanan semakin sulit, jika mereka memberi akses ke akun atau berkas tersebut, dan pihak berwenang tidak bisa menemukannya, karena tahanan akan tampak tidak bisa dipercaya serta dapat menyebabkan tindakan yang merugikan tahanan akibat adanya penghapusan tersebut.
- Jika Anda memberi tahu orang-orang bahwa informasi pribadi mereka yang disimpan di sebuah peranti atau akun yang disita oleh pihak berwenang, dan komunikasi tersebut disadap, maka ini dapat digunakan sebagai bukti tambahan tentang kaitan dengan tahanan.
- Perubahan dalam prosedur komunikasi (termasuk penghapusan akun, dll.) dapat memicu perhatian pihak berwenang.

### Inform contacts

Secara umum, tidaklah mungkin untuk menentukan apakah otoritas penahanan memiliki kapasitas untuk memetakan jaringan kontak tahanan, dan apakah mereka telah melakukannya atau tidak. Maka dari itu kita harus mengasumsikan kemungkinan terburuknya, yaitu bahwa mereka telah melakukannya atau akan melakukannya.

Sebelum menginformasikan kontak tahanan, silakan perkirakan risikonya:

- Apakah Anda memiliki salinan daftar kontak milik tahanan? Dapatkah Anda mengecek siapa saja yang ada dalam daftar kontak mereka, baik itu di peranti, akun surel, dan platform media sosial mereka? Kumpulkan daftar kontak yang memungkinkan untuk mendapatkan gambaran umum dari siapa saja yang mungkin terdampak. [Simpan](https://securityinabox.org/id/files/secure-file-storage/) dan [pindahkan](https://securityinabox.org/id/communication/private-communication/) daftar tersebut seaman mungkin serta bagikan daftar itu atas dasar hanya "perlu tahu".
- Apakah ada risiko bahwa menginformasikan orang-orang dalam daftar kontak dapat mengaitkan mereka lebih dekat kepada tahanan dan hal ini dapat di(salah)gunakan oleh otoritas penahanan untuk melawan mereka?
- Haruskah semua orang diberitahu, atau hanya sekelompok orang tertentu yang ada di daftar kontak?
- Siapa yang akan menginformasikan kontak yang mana? Siapa yang sudah berhubungan dengan siapa? Apa dampak dari keputusan tersebut?
- Bentuk saluran komunikasi yang paling aman, termasuk pertemuan pribadi di ruang yang tidak memiliki pengawasan CCTV, untuk menginformasikan kontak yang terlibat.

### Document to keep evidence

Untuk informasi lebih lanjut tentang bagaimana cara menyimpan bukti digital, kunjungi [Panduan DFAK tentang mendokumentasikan insiden daring](../documentation).

Sebelum Anda menghapus konten apapun dari situs web, situs media sosial, utas, dll., Anda perlu memastikan bahwa Anda telah mendokumentasikannya sebelumnya. Alasan mengapa Anda perlu mendokumentasikannya adalah untuk menyimpan tanda atau bukti dari akun yang disalahgunakan - seperti konten tambahan atau mengandung peniruan identitas - atau konten yang Anda butuhkan sebagai bukti hukum.

Tergantung dari situs web atau platform media sosial yang ingin Anda dokumentasikan feed atau data daringnya, pendekatan yang dipakai dapat berbeda:

- Anda dapat mengambil gambar tangkapan layar (screenshot) dari bagian yang relevan (pastikan cap waktu, alamat URL, dll. disertakan dalam screenshot-nya).
- Anda dapat mengecek bahwa situs web atau blog yang relevan tersebut didaftarkan di [Wayback Machine](https://archive.org/web), atau unduh situs web atau blog ke peranti lokal Anda.

Anda dapat menemukan informasi yang lebih komprehensif tentang cara menyimpan informasi daring di [panduan dokumentasi DFAK](../documentation).

*Ingatlah bahwa penting untuk menyimpan informasi yang telah Anda unduh di peranti yang aman, yang disimpan di tempat yang juga aman.*

### Device Seizure

Jika ada peranti milik tahanan yang disita selama atau setelah penangkapan, silakan baca panduan [Saya kehilangan peranti saya](../topics/lost-device), khususnya bagian [penghapusan jarak jauh](../topics/lost-device/questions/find-erase-device), termasuk rekomendasi jika ada kasus penahanan.  


### Incriminating online data and accounts

Jika tahanan memiliki informasi dalam peranti mereka yang mungkin dapat membahayakan mereka atau orang lain, ada baiknya untuk mencoba membatasi akses yang dimiliki pihak penahan pada informasi ini.

Sebelum melakukannya, bandingkan risiko yang ditimbulkan oleh informasi ini dengan risiko yang ditimbulkan oleh kemarahan aparat keamanan akibat tidak adanya akses ke informasi ini (atau pengambilan tindakan hukum karena penghancuran bukti). Jika risiko yang ditimbulkan oleh informasi ini lebih tinggi, Anda dapat meneruskan dengan menghapus data yang relevan dan/atau menutup/menangguhkan dan menghapus tautan akun dengan mengikuti instruksi berikut ini.

#### Suspend or Close Online Accounts

Jika Anda memiliki akses ke akun yang ingin Anda tutup, Anda dapat mengikuti proses yang ditentukan oleh setiap platform berbeda untuk menutup akun tersebut. Pastikan bahwa Anda memiliki cadangan atau salinan dari konten dan data yang dihapus! Ketahuilah bahwa setelah menutup akun, kontennya tidak secara langsung tidak dapat diakses: misalnya di Facebook, perlu dua minggu untuk menghapus konten dari semua server.

Ketahuilah bahwa ada berbagai opsi untuk menangguhkan atau menutup akun, yang memiliki dampak berbeda pada konten dan akses:

- Mengunci: Mengunci akun akan mencegah siapa pun masuk ke akun tersebut. Saat akun dikunci, semua kredensial dan token aplikasi dihapus. Artinya masuk ke akun tidak dimungkinkan, tetapi akun tetap aktif, dan konten publik apa pun akan tetap terlihat.

- Penangguhan Sementara: Saat meminta penangguhan sementara, platform akan membuat akun tersebut tidak tersedia untuk sementara. Artinya tidak seorang pun akan dapat masuk ke akun tersebut, dan akun tersebut juga akan muncul sebagai ditangguhkan bagi siapa pun yang mencoba berinteraksi dengannya, akan menampilkan pesan "akun ditangguhkan" saat dilihat atau dijangkau.

- Penutupan: Mengacu pada penutupan akun, dengan cara yang tidak dapat dipulihkan di masa mendatang. Ini biasanya merupakan mekanisme pembatasan akses yang paling tidak disukai, tetapi cara ini adalah satu-satunya yang tersedia untuk beberapa layanan dan platform. Mengingat ketidakmampuan untuk memulihkan akun yang relevan, persetujuan dari kuasa hukum atau keluarga diperlukan sebelum meminta penutupan akun.

Jika Anda tidak mempunyai akses ke akun tahanan atau Anda butuh tindakan yang lebih mendesak untuk akun media sosial, silakan cari bantuan dari organisasi yang terdaftar [di sini](../support). Mereka menawarkan keamanan akun sebagai salah satu layanan mereka.

#### Delink Accounts from Devices

Terkadang Anda mungkin juga ingin memutus tautan akun dari peranti tertentu, karena tautan ini dapat memberikan akses ke data sensitif kepada siapa saja yang mengontrol peranti yang terhubung tersebut. Untuk melakukannya, Anda dapat mengikuti [instruksi di alur kerja “Saya kehilangan peranti saya”](../topics/lost-device/questions/accounts).

Jangan lupa untuk memutus tautan [akun bank online](#online_bank_accounts) dari peranti tersebut.


#### Change passwords

Jika Anda memutuskan untuk tidak menutup atau menangguhkan akun, ada baiknya untuk tetap mengubah kata sandinya, dengan mengikuti [instruksi di alur kerja “Saya kehilangan peranti saya”](../topics/lost-device/questions/passwords).

Pertimbangkan juga untuk mengaktifkan autentikasi 2 langkah (2FA) guna meningkatkan keamanan akun tahanan, dengan mengikuti [instruksi di alur kerja “Saya kehilangan peranti saya”](../topics/lost-device/questions/2fa).

Jika kata sandi yang sama digunakan untuk sejumlah akun yang berbeda, Anda harus mengubah semua kata sandi yang terpengaruh pada akun tersebut karena akun-akun tersebut bisa jadi telah disalahgunakan juga.

**Other tips on changing passwords**

- Gunakan [pengelola kata sandi](https://securityinabox.org/id/passwords/passwords-and-2fa/#use-a-password-manager) (misalnya [KeepassXC](https://keepassxc.org)) untuk mendokumentasikan pengubahan kata sandi untuk penggunaan lebih lanjut, atau untuk diserahkan kepada tahanan setelah dibebaskan.
- Pastikan untuk memberitahu tahanan, selambat-lambatnya setelah pembebasan mereka, mengenai pengubahan kata sandi dan kembalikan ke mereka kepemilikan atas akun-akun tersebut.


#### Remove group membership and unshare folders

Jika tahanan merupakan anggota dari grup (misalnya, tetapi tidak terbatas pada) grup Facebook, grup obrolan WhatsApp, Signal, atau Wire, atau dapat mengakses folder daring bersama, dan keberadaan mereka di grup-grup tersebut memberikan akses untuk penahan mereka ke informasi istimewa dan berpotensi bahaya, Anda mungkin ingin menghapus mereka dari grup atau ruang daring bersama.

**Cara menghapus keanggotaan grup di berbagai layanan pesan singkat:**

- [WhatsApp](https://faq.whatsapp.com/id/android/26000116/?category=5245251)
- Telegram
    - Untuk iOS: Orang yang membuat grup dapat menghapus anggota dengan memilih “Info Grup” dan geser ke kiri pengguna yang ingin dihapus.
    - Untuk Android: Ketuk ikon pensil. Pilih “Anggota” - ketuk tiga titik di sebelah nama anggota yang ingin Anda hapus, dan pilih “Hapus dari grup”.
- [Wire](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- [Signal](https://support.signal.org/hc/id/articles/360050427692-Manage-a-group#remove)

**Cara berhenti membagikan folder di berbagai layanan daring:**

- [Facebook](https://www.facebook.com/help/211909018842184/?cms_id=211909018842184)
- [Google Drive](https://support.google.com/drive/answer/2494893?hl=id&co=GENIE.Platform=Desktop)  
- [Dropbox](https://help.dropbox.com/id-id/share/unshare-folder)
- [iCloud](https://support.apple.com/id-id/HT201081)


### Feed Deletion

Dalam beberapa kasus, Anda mungkin ingin menghapus konten dari linimasa media sosial tahanan, atau feed lain yang terhubung dengan akun mereka. Hal-hal tersebut dapat disalahgunakan sebagai bukti untuk melawan mereka atau menciptakan kebingungan dan konflik di dalam komunitas di mana tahanan tergabung maupun untuk mendiskreditkan mereka.

Beberapa layanan mempermudah penghapusan feed dan postingan dari akun dan linimasa. Panduan untuk Twitter, Facebook, dan Instagram tertaut di bawah ini. Pastikan bahwa Anda telah mendokumentasikan konten yang ingin Anda hapus, siapa tahu Anda masih membutuhkannya sebagai bukti jika terjadi gangguan dll.

- Untuk Twitter, Anda bisa menggunakan [Tweet Deleter](https://tweetdeleter.com/).
- Untuk Facebook, Anda bisa mengikuti [panduan Louis Barclay “Cara menghapus Umpan Berita Facebook Anda”](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), berdasarkan aplikasi untuk Browser Chrome, [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Untuk Instagram, Anda dapat mengikuti [instruksi di laman Instagram tentang mengedit dan menghapus postingan](https://help.instagram.com/997924900322403) (ingat bahwa orang yang memiliki akses ke pengaturan akun Anda akan dapat melihat sebagian besar riwayat interaksi Anda, bahkan setelah menghapus konten).


### Delete Harmful Online Associations

Jika ada informasi daring apapun yang menyebut nama tahanan, yang mungkin dapat berdampak negatif bagi mereka atau kontak mereka, sebaiknya hapus informasi tersebut. Lakukan langkah ini hanya jika tidak akan merugikan tahanan lebih lanjut.

- Buat daftar ruang dan informasi daring yang perlu dihapus atau diubah.
- Jika Anda telah mengidentifikasi konten yang akan dihapus atau diubah, sebaiknya Anda membuat cadangannya sebelum memproses penghapusannya atau mengajukan permintaan penghapusan.
- Perkirakan jika penghapusan nama tahanan dapat berdampak negatif pada situasi mereka (misalnya, menghapus nama mereka dari daftar staf suatu organisasi dapat melindungi organisasi tersebut, tetapi hal itu juga dapat menghapus justifikasi bagi tahanan, contohnya bahwa mereka bekerja di organisasi tersebut).
- Jika Anda memiliki akses ke situs atau akun terkait, ubah atau hapus konten dan informasi sensitif.
 - Jika Anda tidak mempunyai aksesnya, mintalah pada orang yang memiliki akses untuk menghapus informasi sensitif tersebut.
- Temukan instruksi untuk menghapus konten pada layanan Google [di sini](https://support.google.com/webmasters/answer/6332384?hl=id#get_info_off_web)
- Cek apakah situs web yang berisi informasi tersebut telah didaftarkan di [Wayback Machine](https://web.archive.org) atau [Google Cache](https://cachedview.com/). Jika demikian, maka konten tersebut harus dihapus juga.
    - [Cara meminta penghapusan dari Wayback Machine (dalam bahasa Inggris)](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
    - [Hapus halaman yang disimpan di situs Anda dari Google](https://developers.google.com/search/docs/crawling-indexing/remove-information?hl=id)

<a name="online_bank_accounts"></a>
### Online Bank Accounts

Seringkali, rekening bank dikelola dan diakses secara daring, dan verifikasi via peranti seluler menjadi penting untuk bertransaksi atau bahkan untuk sekedar mengakses rekening daring tersebut. Jika tahanan tidak mengakses rekening bank mereka untuk jangka waktu lama, hal ini dapat menyebabkan implikasi pada situasi keuangan tahanan dan kemampuan mereka untuk mengakses rekening tersebut. Dalam kasus ini, pastikan untuk:

- Putuskan tautan peranti yang disita dari rekening bank milik tahanan.
- Dapatkan otorisasi dan surat kuasa dari tahanan untuk dapat mengoperasikan rekening bank mereka atas nama mereka di awal proses penahanan (dengan persetujuan dari kerabat mereka).


## Tip Penutup

- Pastikan bahwa Anda mengembalikan seluruh kepemilikan data kembali ke tahanan setelah dibebaskan.
- Baca tip berikut di [alur kerja “Saya telah kehilangan peranti saya”](../topics/lost-device/questions/device-returned) tentang cara menangani peranti yang disita setelah dikembalikan oleh pihak berwenang.
