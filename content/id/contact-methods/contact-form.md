---
layout: page
title: Formulir kontak
author: mfc
language: id
summary: Metode kontak
date: 2020-10
permalink: /id/contact-methods/contact-form.md
parent: /id/
published: true
---

Formulir kontak kemungkinan besar akan menjaga kerahasiaan pesan Anda ke organisasi penerima, sehingga hanya Anda dan organisasi penerima yang dapat membacanya. Hal ini hanya akan terjadi jika situs web yang meng-*hosting* formulir kontak menerapkan tindakan keamanan yang tepat seperti enkripsi [TLS/SSL](https://ssd.eff.org/en/glossary/secure-sockets-layer-ssl) antara lainnya, yang dijalankan di dalam organisasi CiviCERT.

Namun, fakta bahwa Anda telah mengunjungi situs web organisasi tempat formulir kontak di-*hosting* kemungkinan dapat diketahui oleh pemerintah, lembaga penegak hukum, atau pihak lain yang memiliki akses ke infrastruktur pengawasan lokal, regional, atau global. Bahwa Anda mengunjungi situs web organisasi menunjukkan bahwa Anda mungkin telah menghubungi organisasi tersebut.

Jika Anda ingin merahasiakan fakta bahwa Anda mengunjungi situs web organisasi (dan berpotensi menghubungi mereka), maka lebih baik mengakses situs web mereka melalui [Tor Browser](https://www.torproject.org/) atau VPN atau proxy tepercaya. Sebelum melakukannya, pertimbangkan konteks hukum di mana Anda tinggal dan jika Anda perlu menyamarkan penggunaan *Browser* Tor Anda dengan [mengonfigurasinya](https://tb-manual.torproject.org/running-tor-browser/) menggunakan [*pluggable transport*](https://tb-manual.torproject.org/circumvention/). Jika Anda mempertimbangkan untuk menggunakan VPN atau proxy, [selidiki di mana server atau proxy VPN tersebut](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) berada serta [tingkat kepercayaan Anda terhadap entitas VPN](https://ssd.eff.org/en/module/choosing-vpn-thats-right-you).
