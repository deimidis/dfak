---
layout: page
title: Surat Pos
author: mfc
language: id
summary: Metode kontak
date: 2018-09
permalink: /id/contact-methods/postal-mail.md
parent: /id/
published: true
---

Mengirim surat pos adalah metode komunikasi yang lambat jika Anda menghadapi situasi mendesak. Bergantung pada yurisdiksi tempat pengiriman surat, pihak berwenang dapat membuka surat, dan mereka sering melacak pengirim, lokasi pengiriman, penerima, dan lokasi tujuan.