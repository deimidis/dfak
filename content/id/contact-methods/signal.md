---
layout: page
title: Signal
author: mfc
language: id
summary: Metode kontak
date: 2018-09
permalink: /id/contact-methods/signal.md
parent: /id/
published: true
---

Menggunakan Signal akan memastikan konten pesan Anda dienkripsi hanya untuk organisasi penerima, dan hanya Anda dan penerima Anda yang akan mengetahui komunikasi tersebut terjadi. Ketahuilah bahwa Signal menggunakan nomor telepon Anda sebagai nama pengguna sehingga Anda akan membagikan nomor telepon Anda dengan organisasi yang Anda hubungi.

Sumber: [Bagaimana caranya: Gunakan Signal untuk Android (dalam bahasa Inggris)](https://ssd.eff.org/en/module/how-use-signal-android), [Bagaimana caranya: Gunakan Signal untuk iOS (dalam bahasa Inggris)](https://ssd.eff.org/en/module/how-use-signal-ios), [Bagaimana caranya menggunakan Signal Tanpa Memberikan Nomor Telepon Anda (dalam bahasa Inggris)](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)