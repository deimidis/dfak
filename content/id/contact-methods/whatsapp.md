---
layout: page
title: WhatsApp
author: mfc
language: id
summary: Metode kontak
permalink: /id/contact-methods/whatsapp.md
parent: /id/
published: true
---

Menggunakan WhatsApp akan memastikan percakapan Anda dengan penerima terlindungi sehingga hanya Anda dan penerima yang dapat membaca komunikasi tersebut, namun fakta bahwa Anda berkomunikasi dengan penerima mungkin dapat diakses oleh pemerintah atau lembaga penegak hukum.

Sumber: [Bagaimana Cara: Gunakan WhatsApp di Android (dalam bahasa Inggris)](https://ssd.eff.org/en/module/how-use-whatsapp-android) [Bagaimana Cara: Gunakan WhatsApp di iOS (dalam bahasa Inggris)](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
