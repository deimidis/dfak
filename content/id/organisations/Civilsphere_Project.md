---
name: Civilsphere Project
website: https://www.civilsphereproject.org/
logo: Civilsphere.png
languages: English, Español
services: assessment, vulnerabilities_malware, forensic
beneficiaries: jurnalis, pembela HAM, ormas, aktivis, pengacara
hours: Senin-Jumat, 09:00-17:00, CET
response_time: 3 hari
contact_methods: form_web, surel, pgp, pos, telegram
web_form: www.civilsphereproject.org
email: civilsphere@aic.fel.cvut.cz
pgp_key: https://www.civilsphereproject.org/s/Civilsphere_Project_pub.asc
pgp_key_fingerprint: C1FD 513E D50F 00FE 6CBF C72F 52F7 76AD 9B72 6C6D
mail: "Stratosphere Lab (KN-E313), Karlovo náměstí 13, Praha 2, 121 35, Prague, Czech Republic"
telegram: "@civilsphereproject"
initial_intake: no
---

Misi Civilsphere Project adalah menyediakan layanan dan alat bagi jurnalis, aktivis, dan pembela hak asasi manusia untuk mengidentifikasi secara dini ancaman digital yang membahayakan hidup dan pekerjaan mereka.
