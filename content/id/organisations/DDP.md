---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: aktivis, jurnalis, pembela HAM, lgbti, perempuan, pemuda, ormas
hours: Senin-Kamis, 9am-5pm CET
response_time: 4 hari
contact_methods: surel, telepon, pos
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership menawarkan bantuan kepada pembela hak asasi manusia yang mengalami ancaman digital, serta bekerja untuk menguatkan jaringan respons cepat lokal. DDP mengoordinasikan bantuan darurat untuk individu dan organisasi seperti pembela HAM, jurnalis, aktivis masyarakat sipil, dan *blogger*.

DDP memiliki lima jenis pendanaan berbeda yang menangani situasi darurat mendesak, serta hibah jangka panjang yang berfokus pada pembangunan kapasitas dalam suatu organisasi. Selanjutnya, mereka mengoordinasikan Digital Integrity Fellowship di mana organisasi menerima pelatihan keamanan dan privasi digital yang didesain sesuai kebutuhan, dan program Jaringan Respons Cepat.
