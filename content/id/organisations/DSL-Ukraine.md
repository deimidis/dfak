---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: aktivis, jurnalis, pembela HAM, lgbti, perempuan, pemuda, ormas
hours: Senin-Kamis, 9am-5pm EET/EEST
response_time: 2 hari
contact_methods: surel, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine adalah organisasi non-pemerintah yang berbasis di Kyiv, didirikan pada tahun 2017 dengan misi mendukung penerapan hak asasi manusia di Internet dengan membangun kapasitas LSM dan media independen agar masalah keamanan digital mereka ditangani dan berdampak pada kebijakan pemerintah dan perusahaan di bidang hak digital.
