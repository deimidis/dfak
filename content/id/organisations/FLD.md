---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: pembela HAM, organisasi HAM
hours: darurat 24/7, global; kerja regular Senin-Jumat pada jam kerja, IST (UTC+1), staf berada di berbagai zona waktu di berbagai wilayah
response_time: di hari yang sama atau hari berikutnya untuk hal darurat
contact_methods: form_web, telepon, skype, surel
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders merupakan organisasi internasional yang berbasis di Irlandia yang bekerja untuk perlindungan terpadu bagi pembela hak asasi manusia yang terancam. Front Line Defenders menyediakan bantuan yang cepat dan praktis bagi pembela HAM yang menghadapi risiko melalui dana hibah keamanan, pelatihan keamanan fisik dan digital, advokasi dan kampanye.

Front Line Defenders mengoperasikan *hotline* dukungan darurat 24/7 di +353-121-00489 untuk pembela HAM yang menghadapi risiko langsung dalam bahasa Arab, Inggris, Perancis, Rusia, atau Spanyol. Ketika pembela HAM menghadapi ancaman langsung terhadap nyawa mereka, Front Line Defenders dapat membantu dengan relokasi sementara. Front Line Defenders menyediakan pelatihan keamanan fisik dan digital. Front Line Defenders juga memublikasikan kasus-kasus pembela HAM yang berisiko dan melakukan kampanye serta advokasi di tingkat internasional termasuk Uni Eropa, PBB, mekanisme antardaerah dan dengan pemerintah secara langsung untuk perlindungan mereka.
