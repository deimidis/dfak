---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: aktivis, lgbti, perempuan, pemuda, ormas
hours: 24/7, UTC+2
response_time: 4 jam
contact_methods: form_web, surel, pgp, pos, telepon
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost menawarkan layanan TI dengan pendekatan etis dan berkelanjutan. Penawaran layanan mereka termasuk *hosting* web, layanan cloud, dan penawaran khusus yang kuat dalam keamanan informasi. Greenhost secara aktif terlibat dalam pengembangan sumber terbuka, dan mengambil bagian dalam berbagai proyek di bidang teknologi, jurnalisme, budaya, pendidikan, keberlanjutan, dan kebebasan Internet.
