---
layout: page
title: "Seseorang Meniru Identitas Daring Saya"
author: Flo Pagano, Alexandra Hache
language: id
summary: "Seseorang ditiru identitasnya melalui akun media sosial, alamat email, kunci PGP, situs web atau aplikasi palsu"
date: 2023-04
permalink: /id/topics/impersonated
parent: Home
---

# Seseorang Meniru Identitas Daring Saya

Ancaman yang dihadapi oleh banyak aktivis, pembela HAM, LSM, media independen, dan blogger adalah peniruan identitas oleh lawan yang akan membuat profil, situs web, atau email palsu atas identitas mereka. Terkadang hal tersebut dimaksudkan untuk menciptakan kampanye kotor, informasi yang menyesatkan atau rekayasa sosial, atau pencurian identitas seseorang untuk menciptakan kegaduhan, masalah kepercayaan, dan pelanggaran data yang berdampak pada reputasi individu dan kolektif yang ditiru identitasnya. Pada kasus lain, lawan bisa jadi menirukan identitas daring seseorang dengan motif finansial, seperti penggalangan dana, pencurian kredensial pembayaran, menerima pembayaran, dll.

Ini adalah masalah yang membuat frustrasi, pada tingkat yang berbeda, juga dapat memengaruhi kemampuan Anda untuk berkomunikasi dan memberi informasi. Hal tersebut juga dapat memiliki penyebab yang berbeda, tergantung di mana dan bagaimana Anda ditiru identitasnya.

Penting untuk diketahui bahwa ada berbagai cara untuk meniru identitas seseorang (profil palsu di media sosial, situs web hasil kloning, surel palsu, publikasi gambar dan video pribadi tanpa persetujuan). Strateginya dapat berupa antara mengirimkan pemberitahuan penghapusan sampai membuktikan kepemilikan asli, mengklaim hak cipta situs web atau informasi asli, atau memperingatkan jaringan dan kontak pribadi Anda melalui komunikasi publik atau rahasia. Mendiagnosis masalah dan menemukan solusi yang memungkinkan untuk peniruan identitas bisa jadi sulit. Terkadang hampir tidak mungkin untuk mendorong perusahaan *hosting* kecil untuk menghapus sebuah situs web, dan tindakan hukum mungkin diperlukan. Ada baiknya memasang peringatan dan memantau internet untuk mengetahui apakah Anda atau organisasi Anda sedang ditiru identitasnya.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mendiagnosis kemungkinan-kemungkinan cara peniruan identitas dan strategi penanggulangan yang memungkinkan untuk menghapus akun, situs web dan surel yang meniru identitas Anda atau organisasi Anda.

Jika Anda mengalami peniruan identitas, ikuti kuesioner ini untuk mengidentifikasi sifat masalah Anda dan mencari solusi yang memungkinkan.


## Workflow

### urgent-question

Apakah Anda mengkhawatirkan integritas atau kesejahteraan fisik Anda?

 - [Ya](#physical-sec_end)
 - [Tidak](#diagnostic-start1)

### diagnostic-start1

Apakah peniruan identitas tersebut memengaruhi Anda sebagai individu (seseorang menggunakan nama resmi dan nama keluarga Anda, atau nama panggilan yang menjadi dasar reputasi Anda) atau sebagai sebuah organisasi/kolektif?

- [Sebagai individu](#individual)
- [Sebagai organisasi](#organization)

### individual

> Jika Anda terpengaruh secara individu, Anda mungkin ingin memperingatkan kontak Anda. Lakukan langkah ini menggunakan akun surel, profil, atau situs web yang sepenuhnya berada dalam kendali Anda.

- Setelah Anda telah memperingatkan kontak Anda bahwa ada yang meniru identitas Anda, lanjutkan ke [langkah selanjutnya](#diagnostic-start2).

### organization

> Jika Anda terpengaruh secara kelompok, pertimbangkan untuk melakukan komunikasi publik. Lakukan langkah ini menggunakan akun surel, profil, atau situs web yang sepenuhnya berada dalam kendali Anda.

- Setelah Anda telah memperingatkan kontak Anda bahwa ada yang meniru identitas Anda, lanjutkan ke [langkah selanjutnya](#diagnostic-start2).

### diagnostic-start2

Bagaimana bentuk peniruan identitas yang Anda alami?

 - [Ada situs web palsu meniru identitas saya atau grup saya](#fake-website)
 - [Melalui akun jejaring sosial ](#social-network)
 - [Melalui pembagian video atau gambar tanpa persetujuan](#doxing)
 - [Melalui alamat surel saya atau alamat yang serupa](#spoofed-email1)
 - [Melalui kunci PGP yang terhubung dengan alamat surel saya](#PGP)
 - [Melalui aplikasi palsu yang meniru aplikasi saya](#app1)

### social-network

> Anda dapat memutuskan untuk melaporkan akun atau konten yang meniru Anda ke platform yang relevan tempat peniruan identitas terjadi.
>
> ***Catatan:*** *Selalu [dokumentasikan](/../../documentation) sebelum mengambil tindakan seperti menghapus pesan atau log percakapan, atau memblokir profil. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/../../documentation#legal).*

Di platform jejaring sosial mana identitas Anda ditiru?

- [Facebook](#facebook)
- [Instagram](#instagram)
- [TikTok](#tiktok)
- [Twitch](#twitch)
- [Twitter](#twitter)
- [YouTube](#youtube)

### facebook

> Ikuti instruksi di [“Bagaimana cara melaporkan akun atau Halaman Facebook yang berpura-pura sebagai saya atau orang lain?”](https://www.facebook.com/help/174210519303259?cms_id=174210519303259) untuk meminta akun yang meniru identitas Anda tersebut dihapus.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### twitter

> Ikuti langkah-langkah di [“Laporkan akun untuk peniruan identitas”](https://help.twitter.com/id/forms/authenticity/impersonation) untuk meminta akun yang meniru identitas Anda tersebut dihapus.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### instagram

> Ikuti instruksi di [ “Akun Samaran”](https://help.instagram.com/446663175382270?cms_id=446663175382270) untuk meminta akun yang meniru identitas Anda tersebut dihapus.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### tiktok

> Ikuti instruksi di ["Melaporkan akun peniru identitas"](https://support.tiktok.com/id/safety-hc/report-a-problem/report-an-impersonation-account) untuk meminta akun yang meniru identitas Anda tersebut dihapus.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### youtube

> Ikuti instruksi di ["Melaporkan video, channel, dan konten lain yang tidak pantas di YouTube"](https://support.google.com/youtube/answer/2802027) untuk melaporkan akun peniru. Pilih "Peniruan Identitas" di antara kemungkinan alasan pelaporan.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### twitch

> Ikuti instruksi di ["Cara Mengajukan Laporan Pengguna"](https://help.twitch.tv/s/article/how-to-file-a-user-report) untuk melaporkan akun peniruan dan meminta untuk dihapus. Pilih "Peniruan Identitas" di antara kemungkinan kategori yang disertakan dalam formulir.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### fake-website

> Periksa apakah situs web ini dikenal berbahaya dengan cara mencari URL-nya di layanan daring berikut:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

Apakah domain web tersebut telah diketahui berbahaya?

 - [Ya](#malicious-website)
 - [Tidak](#non-malicious-website)

### malicious-website

> Laporkan URL tersebut ke Google Safe Browsing dengan mengisi formulir [“Laporkan perangkat lunak berbahaya”](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk memastikan bahwa laporan Anda berhasil. Sementara itu, Anda dapat melanjutkan ke langkah selanjutnya yaitu mengirim permintaan penghapusan ke penyedia *hosting* dan pendaftar domain, atau simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#non-malicious-website)


### non-malicious-website

> Anda dapat mencoba melaporkan situs web tersebut ke penyedia *hosting* atau pendaftar domain untuk meminta penghapusannya.
>
> ***Catatan:*** *Selalu [dokumentasikan](/../../documentation) sebelum mengambil tindakan seperti menghapus pesan atau log percakapan, atau memblokir profil. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/../../documentation#legal).*
>
> Jika situs web yang ingin Anda laporkan menggunakan konten Anda, satu hal yang mungkin perlu Anda buktikan adalah bahwa Anda adalah pemilik sah dari konten yang asli. Anda dapat menunjukkan hal ini dengan memperlihatkan kontrak asli Anda dengan pendaftar domain dan/atau penyedia *hosting*, tapi Anda juga dapat melakukan pencarian di [Wayback Machine](https://archive.org/web/), guna mencari URL situs web Anda dan situs web yang palsu. Jika kedua situs web tersebut telah terdaftar di sana, Anda akan menemukan riwayat yang memungkinkan untuk memperlihatkan bahwa situs web Anda telah ada sebelum situs web palsu tersebut dipublikasikan.
>
> Untuk mengirimkan permintaan penghapusan, Anda juga akan perlu untuk mengumpulkan informasi tentang situs web palsu tersebut:
>
> - Buka layanan [NSLookup dari Network Tools](https://network-tools.com/nslookup/) dan temukan alamat IP (bisa lebih dari satu) dari situs web palsu dengan cara memasukkan URL-nya di formulir pencarian.
> - Tulislah alamat IP-nya.
> - Buka layanan [Whois Lookup dari Domain Tools](https://whois.domaintools.com/) dan carilah domain serta alamat IP dari situs web palsu tersebut.
> - Catat nama dan alamat surel untuk laporan milik penyedia *hosting* dan layanan domain. Catat juga nama pemilik situs webnya, jika hal tersebut termasuk dalam hasil pencarian Anda.
> - Kirim pesan ke penyedia *hosting* dan pendaftar domain dari situs web palsu tersebut untuk meminta penghapusannya. Dalam pesan tersebut, sertakan informasi mengenai alamat IP, URL dan pemilik situs web yang meniru identitas Anda, serta alasan mengapa situs tersebut merugikan.
> - Anda dapat menggunakan [templat dari Saluran Bantuan Access Now untuk melaporkan situs web yang dikloning ke penyedia *hosting*](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) untuk mengirim pesan ke penyedia *hosting*.
> - Anda dapat menggunakan [templat dari Saluran Bantuan Access Now untuk melaporkan peniruan identitas atau pengkloningan ke penyedia domain](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) untuk mengirim pesan ke pendaftar domain.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#web-protection_end)


### spoofed-email1

> Karena alasan teknis, mengautentikasi surel cukup sulit dilakukan. Ini juga yang menjadi alasan mengapa sangat mudah membuat alamat pengirim palsu dan surel palsu.

Apakah identitas Anda yang ditiru adalah alamat surel Anda atau yang mirip, misalnya dengan nama pengguna yang sama, tapi dengan domain yang berbeda?

- [Identitas yang ditiru adalah alamat surel saya](#spoofed-email2)
- [Identitas yang ditiru adalah alamat surel yang mirip](#similar-email)


### spoofed-email2

> Orang yang meniru identitas Anda mungkin telah meretas akun surel Anda. Untuk mengesampingkan kemungkinan ini, coba ubah kata sandi Anda.

Apakah Anda dapat mengganti kata sandi Anda?

- [Ya](#spoofed-email3)
- [Tidak](#hacked-account)

### hacked-account

> Jika Anda tidak dapat mengubah kata sandi, akun surel Anda mungkin telah diretas. Anda bisa mengikuti alur kerja ["Saya tidak dapat mengakses akun saya"](../../../account-access-issues) untuk menyelesaikan masalah ini.

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### spoofed-email3

> Email palsu terdiri dari pesan surel dengan alamat pengirim yang dipalsukan. Pesan tersebut tampak seperti berasal dari seseorang atau tempat lain, namun bukan dari sumber sebenarnya.
>
> Email palsu umum ditemui dalam kampanye *phishing* dan *spam* karena orang lebih cenderung membuka surel ketika mereka pikir surel tersebut berasal dari sumber yang sah.
>
> Jika seseorang memalsukan surel Anda, Anda harus memberi tahu kontak Anda untuk memperingatkan mereka tentang bahaya *phishing* (lakukan dari akun surel, profil atau situs web yang sepenuhnya berada dalam kendali Anda).
>
> Jika Anda mengira bahwa peniruan identitas ini ditujukan untuk *phishing* atau maksud jahat lainnya, Anda mungkin juga perlu membaca bagian [“Saya menerima pesan mencurigakan”.](../../../suspicious_messages).

Apakah kiriman surel berhenti setelah Anda mengganti kata sandi akun surel Anda?

- [Ya](#compromised-account)
- [Tidak](#secure-comms_end)


### compromised-account

> Mungkin akun Anda telah diretas oleh seseorang yang menggunakannya untuk mengirimkan surel dan meniru identitas Anda. Karena akun Anda telah disalahgunakan, Anda mungkin juga ingin membaca penyelesaian masalah [“Saya kehilangan akses ke akun saya”](../../../account-access-issues/).

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#account_end)


### similar-email

> Jika si peniru identitas menggunakan alamat surel yang serupa dengan milik Anda tetapi dengan domain atau nama pengguna yang berbeda, sebaiknya peringatkan kontak Anda mengenai upaya peniruan identitas Anda ini (lakukan dari akun surel, profil, atau situs web yang sepenuhnya berada dalam kendali Anda).
>
> Anda mungkin juga ingin membaca bagian [“Saya menerima pesan mencurigakan”](../../../suspicious-messages), karena peniruan identitas ini mungkin ditujukan untuk *phishing*.

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#secure-comms_end)


### PGP

Apakah menurut Anda kunci PGP pribadi Anda mungkin telah disalahgunakan, misalnya karena Anda kehilangan kendali atas peranti yang menjadi tempat penyimpanannya?

- [Ya](#PGP-compromised)
- [Tidak](#PGP-spoofed)

### PGP-compromised

> - Buat sepasang kunci baru dan minta ditandatangani oleh orang yang Anda percayai.
> - Beri tahu kontak Anda melalui saluran tepercaya yang Anda kendalikan, misalnya Signal atau [alat yang terenkripsi ujung-ke-ujung lainnya](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools), bahwa mereka harus menggunakan kunci baru Anda dan berhenti menggunakan yang lama. Beri tahu mereka bahwa mereka dapat mengenali kunci Anda yang sebenarnya berdasarkan sidik jari kunci asli Anda. Anda juga dapat mengirimkan kunci publik baru Anda secara langsung melalui saluran tepercaya yang sama yang Anda gunakan untuk memberi tahu mereka.

Apakah Anda memerlukan bantuan lebih lanjut untuk menyelesaikan masalah Anda?

- [Ya](#secure-comms_end)
- [Tidak](#resolved_end)

### PGP-spoofed

> - Beri tahu kontak Anda melalui saluran tepercaya yang Anda kendalikan, misalnya Signal atau [alat yang terenkripsi ujung-ke-ujung lainnya](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools), bahwa seseorang mencoba meniru identitas Anda. Beri tahu mereka bahwa mereka dapat mengenali kunci Anda yang sebenarnya berdasarkan sidik jari kunci asli Anda. Anda juga dapat mengirimkan kunci publik baru Anda secara langsung melalui saluran tepercaya yang sama yang Anda gunakan untuk memberi tahu mereka.

Apakah Anda memerlukan bantuan lebih lanjut untuk menyelesaikan masalah Anda?

- [Ya](#secure-comms_end)
- [Tidak](#resolved_end)

### doxing

> Jika seseorang membagikan informasi pribadi, video atau gambar pribadi Anda, kami sarankan Anda mengikuti alur kerja Pertolongan Pertama Darurat Digital di [*Doxing* dan Pembagian Media Pribadi Tanpa Persetujuan](../../../doxing).

Apa yang ingin Anda lakukan?

- [Bawa saya ke bagian Pertolongan Pertama Darurat Digital tentang *Doxing* dan Pembagian Media Pribadi Tanpa Persetujuan](../../../doxing)
- [Saya membutuhkan bantuan untuk mengatasi masalah saya](#harassment_end)

### app1

> Jika seseorang menyebarkan salinan aplikasi Anda atau perangkat lunak lain yang berbahaya, sebaiknya lakukan komunikasi publik untuk memperingatkan para pengguna agar hanya mengunduh versi yang sah.
>
> Anda juga harus melaporkan aplikasi berbahaya tersebut dan meminta penghapusannya.

Di mana salinan aplikasi Anda berbahaya didistribusikan?

- [Github](#github)
- [Gitlab.com](#gitlab)
- [Google Play Store](#playstore)
- [Apple App Store](#apple-store)
- [Situs web lain](#fake-website)

### github

> Jika perangkat lunak yang berbahaya tersebut disimpan di server Github, baca [Panduan Github untuk Mengirim Pemberitahuan Penghapusan Digital Millennium Copyright Act (DMCA)](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) untuk menghapus konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)

### gitlab

> Jika perangkat lunak yang berbahaya tersebut disimpan di server Gitlab.com, baca [persyaratan permintaan penurunan Digital Millennium Copyright Act (DMCA) milik Gitlab](https://about.gitlab.com/handbook/dmca/) untuk menghapus konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)

### playstore

> Jika aplikasi berbahaya tersebut disimpan di server Google Play Store, ikuti langkah-langkah yang ada di [“Menghapus Konten dari Google”](https://support.google.com/legal/troubleshooter/1114905?hl=id&sjid=10283664226388671403-AP) untuk menghapus konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)

### apple-store

> Jika aplikasi berbahaya tersebut disimpan di server App Store, isilah formulir [“Sengketa Konten Apple App Store”](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=en) untuk menghapus konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di *bookmark* Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah cara ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)

### physical-sec_end

> Jika Anda mengkhawatirkan kesejahteraan fisik Anda, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=physical_sec)

### harassment_end

> Jika informasi atau media pribadi Anda telah dipublikasikan tanpa persetujuan Anda dan Anda memerlukan bantuan untuk menyelesaikan masalah tersebut, Anda dapat menghubungi organisasi yang dapat membantu Anda di bawah ini.
>
> Sebelum Anda menjangkau suatu organisasi, kami sangat menyarankan Anda untuk mengikuti [penyelesaian masalah pada *doxing* dan publikasi media pribadi tanpa persetujuan](../../../doxing) pada Pertolongan Pertama Darurat Digital, karena ini akan membantu Anda mengetahui apa yang sebenarnya terjadi pada Anda.

:[](organisations?services=harassment)

### account_end

> Jika Anda masih mengalami peniruan identitas atau akun Anda masih disalahgunakan, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=account&services=legal)

### app_end

> Jika aplikasi palsu tersebut belum dihapus, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=account&services=legal)

### web-protection_end

> Jika permintaan penghapusan Anda belum berhasil, Anda dapat mencoba menghubungi organisasi di bawah untuk mendapatkan bantuan lebih lanjut.

:[](organisations?services=web_protection)

### secure-comms_end

> Jika Anda butuh bantuan atau rekomendasi mengenai *phishing*, keamanan dan enkripsi surel, atau komunikasi yang aman secara umum, Anda dapat menghubungi organisasi berikut:

:[](organisations?services=secure_comms)


### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital ini bermanfaat. Silakan berikan masukan melalui [surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

Untuk mencegah upaya peniruan identitas Anda lebih jauh, baca tip di bawah ini.

### final_tips

- Buat kata sandi yang kuat, rumit dan unik untuk semua akun Anda.
- Pertimbangkan untuk menggunakan pengelola kata sandi (*password manager*) untuk membuat dan menyimpan kata sandi, sehingga Anda dapat menggunakan berbagai kata sandi yang unik pada situs dan layanan yang berbeda tanpa harus menghafalkan semuanya.
- Aktifkan autentikasi 2-langkah (2FA) untuk akun-akun terpenting Anda. 2FA menawarkan keamanan akun yang lebih baik dengan mengharuskan untuk menggunakan lebih dari satu metode untuk masuk ke akun Anda. Hal ini berarti bahwa meski seseorang mendapatkan kata sandi utama Anda, mereka tidak bisa mengakses akun Anda kecuali mereka juga memiliki ponsel Anda atau sarana autentikasi sekunder lainnya.
- Verifikasi profil Anda di platform jejaring sosial. Beberapa platform menawarkan fitur untuk memverifikasi identitas Anda dan menghubungkannya dengan akun Anda.
- Atur peringatan Google. Anda bisa mendapatkan surel saat hasil baru untuk suatu topik muncul di Penelusuran Google. Misalnya, Anda bisa mendapatkan informasi tentang penyebutan nama Anda atau nama organisasi/kolektif Anda.
- Ambil tangkapan layar halaman web Anda seperti yang terlihat sekarang untuk digunakan sebagai bukti di masa depan. Jika situs web Anda mengizinkan bot perayap web(*crawlers*), Anda bisa menggunakan the Wayback Machine yang ditawarkan oleh [Internet Archive](https://archive.org). Kunjungi [Arsip Internet Wayback Machine](https://archive.org/web/), masukkan nama situs web Anda di bidang bagian bawah "Simpan Laman Sekarang", dan klik tombol "Simpan Laman Sekarang".

#### Resources

- [Surveillance Self-Defense: Membuat Kata Sandi yang Kuat dan Unik (dalam bahasa Inggris)](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Surveillance Self-Defense: Animasi Ringkasan dalam Menggunakan Pengelola Kata Sandi (dalam bahasa Inggris)](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Surveillance Self-Defense: Cara Mengaktifkan Autentikasi Dua-Langkah (dalam bahasa Inggris)](https://ssd.eff.org/module/how-enable-two-factor-authentication)
- [Archive.org: Arsipkan situs web Anda (dalam bahasa Inggris)](https://archive.org/web/)
- [Surveillance Self-Defense: Cara menggunakan KeePassXC - Pengelola Kata Sandi Sumber-Terbuka yang Aman (dalam bahasa Inggris)](https://ssd.eff.org/en/module/how-use-keepassxc)
- [Dokumentasi Komunitas Saluran Bantuan Access Now: Memilih Pengelola Kata Sandi (dalam bahasa Inggris)](https://communitydocs.accessnow.org/295-Password_managers.html)
