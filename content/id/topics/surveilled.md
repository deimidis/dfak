---
layout: page
title: "Saya mungkin sedang diawasi"
author: Carlos Guerra, Peter Steudtner, Ahmad Gharbeia
language: id
summary: "Jika Anda merasa menjadi target pengawasan digital, alur kerja ini akan memandu Anda melalui beberapa pertanyaan untuk mengidentifikasi kemungkinan indikator pengawasan."
date: 2023-05
permalink: /id/topics/surveilled
parent: Home
---

# Saya mungkin sedang diawasi

Baru-baru ini, kita telah melihat banyak kasus pengawasan terhadap aktor ruang sipil, mulai dari penggunaan *stalkerware* oleh pasangan yang kasar dan pengawasan perusahaan terhadap karyawan, hingga pengawasan negara secara massal dan kampanye *spyware* yang ditargetkan terhadap aktivis dan jurnalis. Mengingat luasnya spektrum teknik dan preseden yang diamati, tidak mungkin untuk mencakup setiap aspek pengawasan, terutama untuk bantuan darurat.

Meskipun demikian, alur kerja ini akan berfokus pada kasus umum yang terlihat di ranah digital dan terkait dengan peranti pengguna akhir serta akun daring yang mewakili keadaan darurat dengan respons yang dapat ditindaklanjuti. Ketika tidak ada solusi yang layak, maka kami tidak akan menganggapnya sebagai keadaan darurat, dan tidak akan dibahas secara luas (sebaliknya, referensi akan dibagikan).

Beberapa kasus yang tidak tercakup dalam alur kerja ini adalah:

- Pengawasan fisik. Jika Anda khawatir telah menjadi sasaran pengawasan fisik dan menginginkan bantuan, Anda dapat menghubungi organisasi yang berfokus pada keamanan fisik di Pertolongan Pertama Darurat Digital pada bagian [halaman bantuan](../../support).
- Pengawasan besar-besaran dengan perangkat keras seperti kamera CCTV di ruang publik, perusahaan, dan pribadi
- *Bug* perangkat keras yang tidak tergantung pada koneksi internet

Penafian relevan yang kedua adalah bahwa di zaman modern setiap orang diawasi secara bawaan pada tingkat tertentu:

- Infrastruktur ponsel dirancang untuk mengumpulkan sejumlah besar data tentang diri kita sendiri, yang dapat digunakan oleh beberapa aktor untuk mempelajari detail seperti lokasi kita, jaringan kontak, dll.
- Layanan daring seperti jejaring sosial dan penyedia surel juga mengumpulkan sejumlah besar data tentang kita, yang dapat dihubungkan misalnya ke kampanye iklan untuk menampilkan iklan yang terkait dengan topik posting kita atau bahkan pesan personal langsung.
- Semakin banyak negara memiliki banyak sistem yang saling berhubungan yang menggunakan identitas kita dan mengumpulkan informasi tentang interaksi kita dengan berbagai lembaga publik, hal ini memungkinkan pembuatan "profil" berdasarkan permintaan dokumen untuk informasi pajak atau kesehatan.

Berbagai contoh pengawasan massal tersebut, ditambah banyak preseden dari operasi yang ditargetkan, menciptakan perasaan bahwa semua orang (terutama di ruang aktivis) sedang diawasi, yang dapat menyebabkan paranoia serta memengaruhi kemampuan emosional kita untuk menghadapi ancaman aktual yang mungkin kita hadapi. Meskipun demikian, ada kasus tertentu di mana - bergantung pada profil risiko kita, pekerjaan yang kita lakukan, serta kapasitas dari musuh potensial kita - kita mungkin menemukan kecurigaan bahwa kita sedang diawasi. Jika konteks tersebut adalah kasus Anda, teruslah membaca hingga akhir.

Sebagai penafian ketiga, banyak skema pengawasan (terutama yang paling ditargetkan), biasanya meninggalkan sedikit atau bahkan tidak ada indikator yang dapat diakses oleh korban, yang membuat deteksi menjadi sulit dan terkadang tidak mungkin. Harap perhatikan hal ini saat menavigasi alur kerja, dan jika Anda masih yakin kasus Anda tidak tercakup atau lebih rumit, silakan hubungi organisasi yang tercantum di akhir alur kerja.

Sebagai pertimbangan terakhir, perlu diingat bahwa upaya pengawasan dapat dilakukan secara masif atau ditargetkan. Operasi pengawasan besar-besaran biasanya mencakup seluruh populasi atau sebagian besar dari mereka, seperti semua pengguna platform, semua orang dari wilayah tertentu, dll. Ini biasanya lebih bergantung pada teknologi dan biasanya kurang berbahaya daripada pengawasan yang ditargetkan; yang menarget satu atau sejumlah kecil individu, memerlukan sumber daya khusus (uang, waktu, dan/atau kapasitas), serta biasanya lebih berbahaya daripada pengawasan besar-besaran, mengingat motivasi di balik penempatan sumber daya tersebut untuk memantau aktivitas dan komunikasi orang-orang tertentu.

Jika Anda menemukan indikator bahwa Anda sedang diawasi, klik mulai untuk memulai alur kerja.

## Workflow

### start

> Jika Anda berada di sini, mungkin Anda memiliki alasan untuk berpikir bahwa komunikasi, lokasi, atau aktivitas daring Anda lainnya mungkin diawasi. Di bawah ini Anda akan menemukan serangkaian indikator umum mengenai aktivitas mencurigakan terkait pengawasan. Klik kasus yang sesuai dengan konteks Anda untuk melanjutkan.

Mengingat informasi yang diberikan dalam pendahuluan, masalah apa yang Anda alami?

 - [Saya menemukan alat pelacak di dekat saya](#tracking-device-intro)
 - [Peranti saya tampak mencurigakan](#device-behaviour-intro)
 - [Saya dialihkan ke situs web dengan http yang tidak terlindungi untuk memasang pembaruan atau aplikasi](#suspicious-redirections-intro)
 - [Saya menerima pesan dan peringatan keamanan dari layanan tepercaya](#security-alerts-intro)
 - [Saya menemukan peranti mencurigakan yang tertaut ke komputer atau jaringan saya](#suspicious-device-intro)
 - [Informasi yang saya bagikan secara pribadi melalui surel, perpesanan, atau sejenisnya diketahui oleh musuh](#leaked-internet-info-intro)
 - [Informasi rahasia yang dibagikan melalui panggilan telepon atau SMS tampaknya telah bocor ke musuh (terutama selama peristiwa sensitif)](#leaked-phone-info-intro)
 - [Orang-orang di sekitar Saya telah berhasil menjadi sasaran tindakan pengawasan](#peer-compromised-intro)
 - [Sesuatu yang lain / saya tidak tahu](#other-intro)

### tracking-device-intro

> Jika Anda menemukan peranti tak terduga di dekat Anda, dan Anda yakin itu adalah peranti pengawas, langkah pertama adalah memastikan bahwa memang peranti tersebut ada untuk melacak Anda, dan tidak memenuhi fungsi relevan lainnya. Dengan diperkenalkannya peranti Internet untuk Segala (*Internet of Things*, IoT), menjadi lebih umum untuk menemukan peranti yang melakukan banyak tindakan yang dapat bisa kita lupakan setelah menggunakannya selama sementara waktu. Salah satu langkah pertama untuk memastikan bahwa peranti tersebut memang benar peranti pengawas adalah memeriksa informasi peranti tersebut terkait merek, model, dll. Beberapa alat pelacak umum yang tersedia dengan harga murah di pasaran antara lain Apple Airtag, Samsung Galaxy SmartTag, atau pelacak pelacak Tile. Peranti ini meamng dijual untuk tujuan yang sah seperti membantu menemukan barang pribadi. Namun, aktor jahat mungkin menyalahgunakan benda-benda tersebut untuk melacak orang lain tanpa persetujuan.
>
> Untuk memeriksa apakah kita sedang diikuti oleh pelacak yang tidak diharapkan, selain menemukannya secara fisik, ada baiknya untuk mempertimbangkan beberapa solusi spesifik:
>
> - Untuk Apple Airtag, di iOS [Anda harus menerima notifikasi otomatis setelah peranti yang tidak dikenal terdeteksi selama beberapa waktu](https://support.apple.com/id-id/HT212227).
> - Untuk Android, ada [aplikasi untuk memeriksa Airtag terdekat](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - Untuk peranti Tile, [aplikasi Tile](https://www.tile.com/download) memiliki fitur untuk memeriksa pelacak yang tidak dikenal.
> - Untuk Samsung Galaxy SmartTag, Anda dapat menggunakan [aplikasi SmartThings](https://www.samsung.com/us/smartthings/) untuk fitur serupa.
>
> Jenis peranti lain yang mungkin Anda temukan adalah pelacak GPS. Peranti ini sering mencatat lokasi target dan dalam beberapa kasus dapat mengirimkan informasi melalui saluran telepon, atau dapat menyimpan lokasi dalam beberapa bentuk penyimpanan internal yang dapat dikumpulkan secara fisik nantinya untuk diunduh dan dianalisis. Jika Anda menemukan peranti seperti ini, memahami cara pengoperasian peranti tersebut adalah kunci untuk mengetahui dengan lebih baik siapa yang menanam peranti tersebut dan bagaimana mereka mendapatkan informasinya.
>
> Untuk semua jenis peranti pengawas yang Anda temukan, Anda harus mendokumentasikan segala sesuatu tentangnya: merek, model, apakah terhubung ke jaringan nirkabel, di mana tepatnya Anda menemukannya, nama peranti apa pun yang ada dalam aplikasi yang relevan, dll.

Setelah mengidentifikasi peranti, ada beberapa strategi yang mungkin ingin Anda ikuti. Apa yang ingin Anda lakukan dengan peranti tersebut?

 - [Saya ingin menonaktifkannya](#tracking-disable-device)
 - [Saya ingin menggunakannya untuk menyesatkan si operator](#tracking-use-device)

### tracking-disable-device

> Bergantung pada jenis dan merek peranti yang Anda temukan, Anda mungkin dapat mematikan atau menonaktifkannya secara penuh. Dalam beberapa kasus, hal ini dapat dilakukan melalui aplikasi pelacak seperti yang dijelaskan pada langkah sebelumnya, tetapi juga bisa melalui sakelar daya fisik, atau bahkan dengan merusak perantinya. Perlu diingat bahwa jika Anda ingin melakukan penyelidikan lebih dalam seputar kejadian ini, menonaktifkan peranti dapat mengubah bukti relevan yang tersimpan di peranti tersebut.

Apakah Anda ingin mengambil tindakan lebih lanjut untuk mengidentifikasi pemilik peranti tersebut? (*harap diperhatikan bahwa Anda harus menjaga alat pelacak tetap daring untuk melakukannya*)

 - [Ya](#identify-device-owner)
 - [Tidak](#pre-closure)

### tracking-use-device

> Salah satu strategi umum dalam menangani alat pelacak adalah membuatnya melacak sesuatu yang berbeda dari yang dimaksudkan. Misalnya, Anda dapat meninggalkannya dalam posisi aman yang statis saat Anda pergi ke tempat lain, atau Anda bahkan dapat membuatnya melacak target lain dalam gerakan yang tidak berhubungan dengan Anda. Harap diingat bahwa dalam beberapa skenario ini Anda mungkin kehilangan akses ke peranti tersebut, dan membuat pelacak mengikuti orang lain atau melacak lokasi tertentu dapat menimbulkan implikasi keamanan dan hukum.

Apakah Anda ingin mengambil tindakan lebih lanjut untuk mengidentifikasi pemilik peranti tersebut? (*harap diperhatikan bahwa Anda harus menjaga alat pelacak tetap daring untuk melakukannya*)

 - [Ya](#identify-device-owner)
 - [Tidak](#pre-closure)

### identify-device-owner

> Mengidentifikasi pemilik alat pelacak mungkin sulit dalam beberapa skenario, dan ini akan sangat bergantung pada jenis peranti yang digunakan, jadi kami sarankan untuk meneliti secara khusus alat pelacak yang Anda temukan. Beberapa contohnya adalah:
>
> - Untuk pelacak nirkabel level konsumen:
>     - Bagaimana mereka ditampilkan di aplikasi pengecekan?
>     - Kapan Anda menerima notifikasinya?
>     - Apakah ada cara untuk mengekstrak lebih banyak data dari peranti yang mungkin berisi informasi relevan?
> - Untuk pelacak yang lebih khusus, seperti pelacak GPS:
>     - Apakah mereka memiliki penyimpanan internal? Beberapa model memiliki kartu SD yang dapat diekstraksi untuk melihat kapan peranti mulai mengambil data, atau informasi relevan lainnya.
>     - Apakah mereka memiliki saluran telepon? Bisakah Anda mengekstrak kartu SIM dari peranti dan melihat apakah Anda dapat mencobanya di ponsel lain untuk mendapatkan nomornya? Dengan informasi ini, dapatkah Anda mengetahui siapa pemilik saluran telepon itu?
> - Secara umum:
>     - Cari tanda-tanda fisik apa pun, seperti nama yang tertulis di peranti, nomor inventaris, dll.
>     - Jika peranti tersebut terhubung ke jaringan, apakah ada nama peranti khusus yang dapat menyertakan data identifikasi?
>     - Siapa yang dapat mengakses tempat di mana Anda menemukan peranti tersebut? Setelah tahu tempatnya, kapan peranti itu diletakkan di situ?

Apakah Anda ingin mengambil tindakan hukum terhadap pemilik alat pelacak?

 - [Ya](#legal-action-device-owner)
 - [Tidak](#pre-closure)

### legal-action-device-owner

> Jika Anda ingin mengambil tindakan hukum terhadap pemilik peranti pengawas, Anda perlu memeriksa seperti apa prosesnya di yurisdiksi Anda. Dalam beberapa kasus diperlukan pengacara, dalam kasus lain cukup dengan mendatangi kantor polisi untuk memulai prosesnya. Satu-satunya saran yang umum untuk semua kasus adalah bahwa kasus apa pun akan memiliki kekuatan yang setara dengan bukti yang telah Anda kumpulkan sebelumnya. Awal yang baik adalah dengan mengikuti saran yang diberikan di langkah sebelumnya dari alur kerja ini tentang apa yang harus didokumentasikan. Anda dapat menemukan lebih banyak informasi dan contoh dalam panduan Pertolongan Pertama Darurat Digital untuk [mendokumentasikan keadaan darurat digital](/../../documentation).

Apakah Anda memerlukan bantuan untuk mengambil tindakan hukum terhadap pemilik alat pelacak?

 - [Ya, saya butuh bantuan hukum](#legal_end)
 - [Tidak, saya pikir saya telah menyelesaikan masalah saya](#resolved_end)
 - [Tidak, tapi saya ingin mempertimbangkan skenario pengawasan lainnya](#pre-closure)

### device-behaviour-intro

> Salah satu teknik paling umum hingga saat ini dalam pengawasan yang ditargetkan adalah menginfeksi ponsel atau komputer dengan *spyware* - *malware* yang dirancang untuk memantau dan mengirimkan aktivitas ke orang lain. *Spyware* dapat ditanam oleh pelaku yang berbeda melalui metode yang berbeda, misalnya pasangan kasar yang mengunduh aplikasi mata-mata secara manual (biasanya disebut sebagai *stalkerware* atau *spouseware*) di peranti pasangannya, atau kejahatan terorganisir yang mengirimkan tautan *phishing* untuk mengunduh aplikasi.
>
> Beberapa contoh indikator pengawasan mungkin termasuk aplikasi mencurigakan yang Anda temukan di peranti Anda dengan izin akses untuk mikrofon, kamera, akses jaringan, dll. Melihat indikator webcam yang aktif ketika sedang tidak menggunakan aplikasi apa pun yang seharusnya mengoperasikannya, atau mengalami "kebocoran" konten berkas di peranti.
>
> Untuk kasus seperti itu, Anda dapat merujuk ke alur kerja Pertolongan Pertama Darurat Digital bagian "Peranti saya tampak mencurigakan".

Apa yang ingin Anda lakukan?

 - [Bawa saya ke alur kerja peranti yang tampak mencurigakan](../../../device-acting-suspiciously)
 - [Bawa saya ke langkah berikutnya di sini](#pre-closure)

### suspicious-redirections-intro

> Meskipun ini jarang terjadi, terkadang kita melihat peringatan keamanan saat mencoba memperbarui aplikasi atau bahkan sistem operasi peranti kita. Ada banyak alasan untuk hal ini, dan itu bisa jadi memang sah atau bahkan berbahaya. Beberapa pertanyaan panduan yang harus kita tanyakan pada diri sendiri adalah:
>
> - Apakah peranti saya memiliki tanggal dan waktu yang akurat? Cara peranti kita dalam memercayai *server* untuk beroperasi dengan lebih aman bergantung pada pemeriksaan sertifikat keamanan yang valid dalam jangka waktu tertentu. Jika, misalnya, komputer Anda dikonfigurasikan dengan tahun sebelumnya, pemeriksaan keamanan ini akan menampilkan kegagalan untuk sejumlah situs web dan layanan. Jika tanggal dan waktu Anda akurat, ada kemungkinan dari pihak penyedia yang tidak memperbarui sertifikat mereka, atau ada sesuatu yang mencurigakan terjadi. Bagaimanapun, aturan praktis yang baik adalah tidak pernah memperbarui atau mengoperasikan aplikasi di peranti Anda ketika Anda melihat peringatan atau pengalihan keamanan tersebut.
> - Apakah masalah tersebut muncul setelah kejadian yang tidak biasa, seperti mengeklik iklan, memasang aplikasi, atau membuka dokumen? Banyak serangan mengandalkan proses peniruan identitas seperti pemasangan dan pembaruan perangkat lunak yang tampak sah, jadi ada kemungkinan peranti Anda mengetahuinya lalu kemudian memberikan peringatan atau menghentikan proses sepenuhnya.
> - Apakah proses yang bermasalah dengan Anda terjadi dengan cara yang tidak biasa, misal biasanya proses pembaruan aplikasi tersebut mengikuti proses yang berbeda dari yang sedang Anda lihat?
>
> Bagaimanapun, jangan lupa untuk mendokumentasikan semuanya sedetail mungkin, terutama jika Anda ingin menerima bantuan khusus, melakukan penyelidikan lebih dalam, atau melakukan tindakan hukum apa pun.
>
> Di hampir semua kasus, mengatasi akar penyebab masalah sudah cukup sebelum mengembalikan peranti dan navigasi kembali ke normal. Namun, ada kemungkinan masalah lebih lanjut jika Anda benar-benar memasang atau memperbarui aplikasi (atau sistem operasi) dalam keadaan yang mencurigakan.

Apakah Anda mengeklik dan mengunduh, atau memasang perangkat lunak atau pembaruan apa pun dalam kondisi yang mencurigakan?

 - [Ya](#pre-closure)
 - [Tidak](#suspicious-software-installed)

### suspicious-software-installed

> Dalam kebanyakan kasus, serangan yang membajak proses pemasangan atau pembaruan aplikasi atau sistem operasi ditujukan untuk memasang *malware* ke sistem. Jika Anda menduga ini adalah skenario Anda, sebaiknya buka alur kerja Pertolongan Pertama Darurat Digital bagian "Peranti saya tampak mencurigakan".

Apa yang ingin Anda lakukan?

 - [Bawa saya ke alur kerja peranti yang tampak mencurigakan](../../../device-acting-suspiciously)
 - [Bawa saya ke langkah berikutnya di sini](#pre-closure)

### security-alerts-intro

> Jika Anda menerima pesan atau notifikasi peringatan dari layanan surel, platform jejaring sosial, atau layanan daring lainnya yang benar-benar Anda gunakan, dan hal tersebut berhubungan dengan masalah keamanan seperti proses log masuk baru yang mencurigakan dari peranti atau lokasi baru, Anda dapat terlebih dahulu memeriksa apakah pesan tersebut sah atau jika pesan tersebut adalah pesan *phishing*. Untuk melakukannya, sebaiknya periksa alur kerja Pertolongan Pertama Darurat Digital bagian [Saya menerima pesan mencurigakan](../../../suspicious-messages) sebelum melanjutkan.
>
> Jika pesan tersebut sah, pertanyaan selanjutnya yang harus Anda tanyakan pada diri sendiri adalah apakah ada alasan mengapa pesan ini bisa jadi sah. Misalnya, jika Anda sedang menggunakan layanan VPN atau Tor, lalu melakukan navigasi ke versi web suatu layanan, ia akan mengira Anda sedang berada di negara lokasi *server* VPN atau *exit node* Tor Anda tersebut, sehingga dapat memicu peringatan log masuk baru yang mencurigakan, padahal tindakan tersebut Anda lakukan hanya sebagai solusi untuk mengelak dari penyensoran.
>
> Hal lain yang berguna untuk ditanyakan kepada diri sendiri adalah peringatan keamanan seperti apa yang kita terima: apakah tentang seseorang yang mencoba masuk ke akun Anda atau tentang upaya mereka yang berhasil? Bergantung pada jawabannya, respons yang diperlukan mungkin sangat berbeda.
>sama
> Jika kita menerima notifikasi tentang upaya masuk yang gagal, dan kita memiliki kata sandi yang baik (panjang, tidak sama dengan akun lainnya untuk layanan yang tidak aman, dll.) serta menggunakan autentikasi multi-langkah (juga dikenal sebagai MFA atau 2FA), kita tidak perlu khawatir terhadap kemungkinan akun yang disalahgunakan. Terlepas dari seberapa serius ancaman penyalahgunaan akun, selalu disarankan untuk memeriksa log apa pun pada aktivitas terkini di mana kita biasanya dapat meninjau riwayat log masuk kita, termasuk lokasi dan jenis peranti yang digunakan.
>
> Jika Anda melihat sesuatu yang mencurigakan di log aktivitas Anda, strategi paling umum, tergantung pada akun tersebut, mencakup:
>
> - Mengubah kata sandi
> - Mengaktifkan MFA atau 2FA
> - Mencoba memutuskan tautan semua peranti dari akun tersebut
> - Mendokumentasikan indikator upaya peretasan (jika kita ingin mencari bantuan, menyelidiki lebih lanjut, atau memulai tindakan hukum)
>
> Pertanyaan penting lainnya untuk membantu asesmen notifikasi ini adalah jika kita berbagi peranti atau akses ke akun dengan orang lain yang bisa jadi telah memicu notifikasi tersebut. Jika demikian, berhati-hatilah dengan tingkat akses yang Anda berikan kepada orang lain terhadap informasi Anda serta ulangi langkah-langkah di atas jika Anda ingin mencabut akses orang ketiga ini.

Jika Anda yakin ada potensi ancaman lain yang tercantum di awal alur kerja yang ingin Anda periksa, atau memiliki kasus yang lebih rumit dan ingin mencari bantuan, klik Lanjut.

 - [Lanjut](#pre-closure)


### suspicious-device-intro

> Jika kita telah menemukan peranti yang mencurigakan, hal pertama yang perlu kita tanyakan pada diri sendiri adalah apakah peranti tersebut berbahaya atau memang dimaksudkan untuk berada di tempatnya. Ini karena dalam beberapa tahun terakhir jumlah peranti dengan kemampuan komunikasi yang ada di rumah dan tempat kerja kita telah berkembang pesat: printer, lampu pintar, mesin cuci, termostat, dll. Mengingat banyaknya peranti tersebut, kita mungkin akan khawatir tentang peranti yang kita tidak ingat pernah mengaturnya atau yang dikonfigurasikan oleh orang lain tetapi tidak berbahaya.
>
> Jika Anda tidak tahu apakah peranti yang Anda temukan tersebut berbahaya, salah satu hal pertama yang harus dilakukan adalah mendapatkan informasi tentangnya: merek, model, apakah terhubung ke jaringan kabel atau nirkabel, apakah peranti bekerja sesuai dengan peruntukannya, apakah dalam kondisi dihidupkan, dll. Jika Anda masih tidak tahu apa peruntukan peranti tersebut, Anda dapat menggunakan informasi ini untuk mencari di web dan mempelajari lebih lanjut tentang identitas peranti beserta fungsinya.

Setelah meninjau peranti tersebut, apakah menurut Anda itu sah?

 - [Itu adalah peranti yang sah](#pre-closure)
 - [Saya sudah periksa tetapi masih tidak tahu peruntukan peranti tersebut atau apakah peranti itu berbahaya](#suspicious-device2)


### suspicious-device2

> Jika Anda masih mengkhawatirkan peranti tersebut setelah tinjauan awal, Anda harus mempertimbangkan kemungkinan terjauh bahwa peranti tersebut memang peranti pengawas. Biasanya peranti ini memantau aktivitas di jaringan tempat mereka terhubung, atau bahkan dapat merekam serta mengirimkan audio dan/atau video seperti halnya *spyware*. Penting untuk dipahami bahwa skenario ini sangat jarang terjadi dan biasanya terkait dengan profil yang berisiko sangat tinggi. Namun, jika hal ini mengkhawatirkan Anda, beberapa hal yang dapat Anda lakukan antara lain:
>
> - Memutuskan sambungan peranti dari jaringan/daya listrik lalu periksa apakah semua layanan yang diperuntukan berfungsi tanpa masalah. Setelah itu Anda bisa membuat peranti dianalisis oleh seseorang yang berpengalaman.
> - Meneliti lebih lanjut lalu mencari bantuan (hal ini umumnya melibatkan tes yang lebih canggih seperti memetakan jaringan atau mencari sinyal yang tidak biasa).
> - Sebagai tindakan pencegahan untuk mengurangi pengawasan jaringan apa pun jika membiarkan peranti tersebut tetap terhubung, gunakan VPN, Tor, atau alat serupa yang mengenkripsi penelusuran web Anda, serta alat komunikasi dengan enkripsi ujung-ke-ujung.
> - Sebagai tindakan pencegahan untuk memitigasi potensi perekaman audio/video jika membiarkan peranti tersebut tetap terhubung, isolasi peranti tersebut secara fisik sehingga tidak akan merekam audio atau video yang berharga.

Jika Anda yakin ada potensi ancaman lain yang tercantum di awal yang ingin Anda periksa, atau ingin mencari bantuan, klik Lanjut.

 - [Lanjut](#pre-closure)
 - [Saya pikir saya telah menyelesaikan masalah saya](#resolved_end)

### leaked-internet-info-intro

> Cabang alur kerja ini akan mencakup data yang bocor dari layanan dan aktivitas internet. Untuk informasi yang bocor dari komunikasi telepon, silakan merujuk ke bagian ["Informasi rahasia yang dibagikan melalui panggilan telepon atau SMS tampaknya telah bocor ke musuh"](#leaked-phone-info-intro).
>
> Secara umum, kebocoran dari layanan berbasis internet dapat terjadi karena tiga alasan:
>
> 1. Informasi tersebut sedari awal bersifat publik, yang tidak akan dicakup dalam sumber ini.
> 2. Informasi tersebut dibocorkan langsung dari akun (lebih umum untuk musuh "lebih kecil" yang dekat dengan korban).
> 3. Informasi tersebut dibocorkan dari platform tempat informasi tersebut pertama kali diunggah (lebih umum untuk musuh "besar" seperti pemerintah dan perusahaan besar).
>
> Kami akan membahas dua kasus terakhir, karena lebih terkait dengan keadaan darurat umum yang terlihat di komunitas ruang sipil. Anda dapat mengikuti alur kerja mulai dari pertanyaan di bawah ini:

Apakah Anda berbagi peranti atau akun dengan musuh Anda sebelumnya?

 - [Ya](#shared-devices-or-accounts)
 - [Tidak](#leaked-internet-info2)

### shared-devices-or-accounts

> Dalam beberapa kasus, berbagi akses akun dengan orang lain dapat memberi akses pada pihak yang tidak diinginkan terhadap informasi sensitif, yang kemudian dapat dibocorkan dengan mudah. Selain itu, umumnya saat kita berbagi akun, kita perlu membuat prosesnya praktis, sehingga sering kali akan memengaruhi aspek keamanan dari mekanisme akses akun, seperti memperlemah kata sandi agar mudah dibagikan dan menonaktifkan autentikasi multi-langkah (MFA atau 2FA) agar banyak orang dapat masuk ke dalam akun yang sama.
>
> Salah satu saran pertama adalah membuat asesmen tentang siapa saja yang harus memiliki akses ke akun atau sumber relevan (seperti dokumen di penyimpanan cloud) dan membatasinya hanya untuk sesedikit mungkin orang. Prinsip lain yang bermanfaat adalah memiliki satu akun untuk setiap orang, sehingga memungkinkan setiap orang untuk mengonfigurasi pengaturan akses mereka dengan cara yang paling aman.
>
> Juga disarankan untuk memeriksa bagian ["Saya menerima pesan dan peringatan keamanan dari layanan tepercaya"](#security-alerts-intro), di mana Anda dapat menemukan tip untuk mempersulit akses terhadap akun jika ada akses yang mencurigakan.

Klik Lanjut untuk menjelajahi situasi Anda lebih detail.

 - [Lanjut](#leaked-internet-info2)

### leaked-internet-info2

Bisakah musuh Anda mengontrol atau mengakses layanan daring yang Anda gunakan serta menyimpan informasi yang bocor? (*Ini biasanya berlaku untuk pemerintah, lembaga penegak hukum, atau penyedia layanan itu sendiri*)

 - [Ya](#control-of-online-services)
 - [Tidak](#leaked-internet-info3)

### control-of-online-services

> Terkadang platform yang kita gunakan ternyata mudah diakses atau bahkan dikendalikan oleh musuh. Beberapa pertanyaan umum yang dapat Anda tanyakan pada diri sendiri adalah:
>
> - Siapa yang memiliki layanan ini?
> - Apakah dia musuh?
> - Apakah musuh memiliki akses ke *server* atau data melalui penyelidikan data secara legal?
>
> Dalam hal ini, disarankan untuk memindahkan informasi ke platform lain yang tidak dikendalikan oleh musuh untuk menghindari kebocoran di masa mendatang.
>
> Indikator berguna lainnya dapat berupa:
>
> - Aktivitas log masuk (termasuk lokasi geografis), jika tersedia
> - Perilaku aneh terkait elemen yang telah dibaca dan belum dibaca
> - Konfigurasi yang memengaruhi tindakan terhadap pesan atau informasi, seperti pengalihan, aturan untuk memantulkan kembali surel, dll., jika tersedia
>
> Anda juga dapat memeriksa apakah pesan Anda dibaca oleh seseorang secara tidak sengaja dengan menggunakan token Canary, seperti yang dapat kami hasilkan di [canarytokens.org](https://canarytokens.org/generate)

Klik Lanjut untuk mengeksplor situasi Anda dengan lebih detail.

 - [Lanjut](#leaked-internet-info3)

### leaked-internet-info3

Apakah musuh Anda memiliki akses ke peralatan pengawas serta kapasitas untuk menyebarkannya di dekat Anda?

 - [Ya](#leaked-internet-info4)
 - [Tidak](#pre-closure)

### leaked-internet-info4

> Musuh yang lebih kuat mungkin memiliki kapasitas untuk menyebarkan peralatan pengawas khusus di sekitar korbannya, seperti memasang alat pelacak atau menginfeksi peranti Anda sendiri dengan *spyware*.

Menurut Anda, apakah ada kemungkinan Anda dilacak oleh alat pelacak, atau komputer atau ponsel Anda mungkin telah terinfeksi *malware* untuk memata-matai Anda?

 - [Saya ingin mempelajari lebih lanjut tentang alat pelacak](#tracking-device-intro)
 - [Saya ingin mempelajari lebih lanjut tentang kemungkinan peranti saya disalahgunakan oleh *malware*](#device-behaviour-intro)

### leaked-phone-info-intro

> Seperti yang dijelaskan dalam pengantar alur kerja ini, pengawasan telepon besar-besaran merupakan ancaman yang ada di mana-mana di sebagian besar (jika tidak semua) wilayah dan negara. Sebagai garis dasar, kita perlu menyadari bahwa sangat mudah bagi operator (dan siapa saja yang memiliki kendali atau akses terhadapnya) untuk memeriksa informasi seperti:
>
> - Lokasi historis (setiap kali ponsel aktif dan terhubung ke jaringan), data dari menara ponsel yang terhubung ke ponsel
> - Log panggilan telepon (siapa yang menelepon siapa, kapan, dan untuk berapa lama)
> - SMS (baik log maupun konten sebenarnya dari pesan tersebut)
> - Lalu lintas internet (hal ini kurang umum dan kurang berguna untuk musuh, tetapi dari sini dimungkinkan untuk menyimpulkan aplikasi mana saja yang digunakan, kapan digunakan, situs web mana yang dikonsultasikan, dll.)
>
> Skenario lain yang masih teramati dalam beberapa konteks, meski tidak umum, adalah penggunaan peranti intersepsi fisik yang disebut Stingray atau IMSI-Catcher. Peranti ini menyamar sebagai menara jaringan seluler yang sah untuk mengalihkan lalu lintas telepon orang-orang di area jangkauan fisik yang dekat agar melaluinya. Sementara kerentanan yang timbul ketika mengaktifkan peranti ini sedang diselesaikan atau dikurangi dengan menjalankan protokol yang lebih baru (4G/5G), masih ada kemungkinan untuk mengganggu sinyal sedemikian rupa sehingga ponsel percaya bahwa satu-satunya protokol yang tersedia adalah 2G (saluran yang paling banyak disalahgunakan oleh peranti tersebut). Untuk kasus ini, ada beberapa indikator yang bisa diandalkan, terutama ketika sinyal Anda "diturunkan" ke 2G di area di mana seharusnya bisa terhubung ke 4G atau 5G dengan mudah, atau ketika orang lain di sekitar jaringan dapat terhubung ke protokol yang lebih baru tanpa masalah. Untuk informasi lebih lanjut Anda dapat memeriksa [situs web FADe Project (dalam bahasa Inggris)](https://fadeproject.org/?page_id=36). Selain itu, Anda juga dapat menanyakan pada diri sendiri apakah musuh Anda terbukti memiliki akses pada peranti Stingray atau peranti serupa lainnya.
>
> Rekomendasi umum untuk skenario tersebut adalah disarankan bagi individu yang berisiko untuk memindahkan saluran komunikasi mereka ke saluran terenkripsi dengan mengandalkan internet, daripada menggunakan saluran telepon dan SMS konvensional. Sama halnya untuk lalu lintas internet yang sensitif, gunakan VPN atau Tor (termasuk penggunaan aplikasi juga). Aspek besar yang belum terselesaikan adalah pelacakan lokasi pengguna, yang tidak dapat dihindari ketika tetap terhubung ke jaringan telepon konvensional.
>
> Mengenai pengumpulan data besar-besaran melalui operator layanan telepon, di beberapa yurisdiksi pengguna dapat meminta data mereka yang mana saja yang telah dibagikan di masa lalu. Anda dapat meneliti apakah hal ini berlaku dalam konteks Anda dan apakah informasi tersebut dapat diandalkan untuk mendeteksi potensi pengawasan. Perlu diingat bahwa hal tersebut biasanya melibatkan penyelidikan hukum.

Jika Anda ingin memeriksa potensi ancaman lain atau memiliki kasus yang lebih rumit dan ingin mencari bantuan, klik Lanjut.

 - [Lanjut](#pre-closure)


### peer-compromised-intro

> Jika ada orang atau organisasi yang berhubungan dengan Anda telah menjadi korban pengawasan, kami menyarankan Anda untuk mencoba memahami teknik pengawasan yang digunakan pada mereka, serta memilih dari daftar sebelumnya yang paling mirip dengan situasi tersebut, sehingga Anda dapat menerima konteks yang lebih relevan serta memutuskan jika Anda juga bisa menjadi korban dari teknik serupa.

Apa yang ingin Anda lakukan?

 - [Bawa saya ke langkah sebelumnya](#start)
 - [Saya tidak yakin](#pre-closure)

### other-intro

> Daftar yang Anda lihat sebelumnya mencakup kasus paling umum di ruang sipil yang terlihat oleh tim pembantu, tetapi situasi Anda bisa jadi tidak tercakup di sana. Jika demikian, klik "Saya butuh bantuan"; jika tidak, Anda dapat kembali ke daftar kasus paling umum dan memilih skenario terdekat yang berlaku untuk Anda guna mendapatkan panduan yang lebih spesifik.

Apa yang ingin Anda lakukan?

 - [Bawa saya ke daftar potensi ancaman](#start)
 - [Saya butuh bantuan](#help_end)

### pre-closure

> Kami harap alur kerja ini telah membantu Anda sejauh ini. Anda dapat memilih untuk kembali ke daftar awal skenario untuk memeriksa ancaman lain yang mungkin Anda khawatirkan, atau pergi ke daftar organisasi yang mungkin Anda hubungi untuk membantu Anda dengan kasus yang lebih rumit.

Apa yang ingin Anda lakukan?

 - [Saya ingin belajar tentang teknik pengawasan lainnya](#start)
 - [Saya pikir kebutuhan saya tidak tercakup dalam bagian ini atau kasus saya terlalu rumit](#help_end)
 - [Saya pikir saya telah menangkap dengan jelas tentang apa yang terjadi pada saya](#resolved_end)


### help_end

> Jika Anda memerlukan bantuan tambahan dalam menangani kasus pengawasan, Anda dapat menghubungi organisasi yang tercantum di bawah ini.
>
> *Catatan: saat Anda menghubungi organisasi mana pun untuk mencari bantuan, silakan bagikan jalur yang telah Anda ikuti di sini serta indikator apa pun yang sudah Anda temukan untuk mempermudah langkah selanjutnya.*

:[](organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

> Jika Anda memerlukan dukungan untuk menuntut seseorang karena telah memata-matai Anda secara ilegal, Anda dapat menghubungi salah satu organisasi di bawah ini.

:[](organisations?services=legal)

### resolved_end

Semoga panduan penyelesaian masalah ini bermanfaat. Silakan beri masukan [via surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Pindahkan bentuk komunikasi apa pun dari saluran tidak aman seperti panggilan telepon dan SMS ke saluran komunikasi terenkripsi yang mengandalkan internet, seperti aplikasi perpesanan yang terenkripsi ujung-ke-ujung dan situs web yang dilindungi melalui HTTPS.
- Jika Anda khawatir lalu lintas internet Anda dipantau dan Anda yakin hal ini bisa menjadi risiko keamanan, pertimbangkan untuk menyambung ke internet melalui alat enkripsi seperti VPN atau Tor.
- Lacak semua perangkat yang ada di ruang Anda, terutama yang terhubung ke internet, dan coba ketahui peruntukannya.
- Hindari berbagi informasi sensitif melalui layanan yang dikendalikan oleh musuh.
- Kurangi berbagi akses akun atau peranti Anda dengan orang lain sebanyak yang Anda bisa.
- Waspadai pesan, situs web, unduhan, dan aplikasi yang mencurigakan.

#### resources

- [Aplikasi Android oleh Apple untuk Memindai AirTag adalah Langkah Maju yang Diperlukan, Tetapi Masih Diperlukan Lebih Banyak Mitigasi untuk *Anti-Stalking* (dalam bahasa Inggris)](https://www.eff.org/es/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking).
- [Privasi untuk Pelajar (dalam bahasa Inggris)](https://ssd.eff.org/module/privacy-students): Dengan referensi teknik pengawasan di sekolah yang dapat diterapkan pada banyak skenario lainnya.
- [Security in a Box: Lindungi privasi komunikasi daring Anda (dalam bahasa Inggris)](https://securityinabox.org/id/communication/private-communication/)
- [Security in a Box: Kunjungi situs web yang diblokir dan jelajahi secara anonim (dalam bahasa Inggris)](https://securityinabox.org/id/internet-connection/anonymity-and-circumvention/)
- [Security in a Box: Rekomendasi untuk alat obrolan terenkripsi (dalam bahasa Inggris)](https://securityinabox.org/id/communication/tools/#more-secure-text-voice-and-video-chat-applications)
