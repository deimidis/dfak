---
layout: page
title: Tor
author: mfc
language: ky
summary: Байланыш жолдору
date: 2018-09
permalink: /ky/contact-methods/tor.md
parent: /ky/
published: true
---

Tor Browser купуялуулукту сактоого багытталган. Бул сизге анонимдүү түрдө веб-сайттарга туташуу мүмкүнчүлүгүн берет жана сайтка киргенде IP дареги боюнча жайгашкан жериңизди көрсөтпөйт.

Пайдалуу ресурстар: [Tor тиркемесине кыска түрдө көз чаптыруу](https://www.torproject.org/about/overview.html.en).
