---
name: Civilsphere Project
website: https://www.civilsphereproject.org/
logo: Civilsphere.png
languages: English, Español
services: assessment, vulnerabilities_malware, forensic
beneficiaries: journalists, hrds, cso, activists, lawyers
hours: "တနင်္လာမှသောကြာ, 09:00-17:00, CET"
response_time: "၃ ရက်"
contact_methods: web_form, email, pgp, mail, telegram
web_form: www.civilsphereproject.org
email: civilsphere@aic.fel.cvut.cz
pgp_key: https://www.civilsphereproject.org/s/Civilsphere_Project_pub.asc
pgp_key_fingerprint: C1FD 513E D50F 00FE 6CBF C72F 52F7 76AD 9B72 6C6D
mail: "Stratosphere Lab (KN-E313), Karlovo náměstí 13, Praha 2, 121 35, Prague, Czech Republic"
telegram: "@civilsphereproject"
initial_intake: no
---

The Civilsphere Project ၏ ရည်ရွယ်ချက်သည် သတင်းထောက်များ၊ တက်ကြွလှုပ်ရှားသူများ၊ လူ့အခွင့်အရေးကာကွယ်သူများ၏ အသက်အန္တရာယ်နှင့် အလုပ်အကိုင်ကို ထိပါးနိုင်သော ဒစ်ဂျစ်တယ်ခြိမ်းခြောက်မှုများကို စောလျင်စွာ ဖော်ထုတ်နိုင်စေမည့် ဝန်ဆောင်မှုနှင့် ကိရိယာများ ပေးနိုင်ရန်ဖြစ်သည်။
