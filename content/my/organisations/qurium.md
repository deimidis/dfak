---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: "8AM-18PM တနင်္လာမှတနင်္ဂနွေ CET"
response_time: "၄ နာရီ"
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation သည် လွတ်လပ်သော သတင်းမီဒီယာများ၊ လူ့အခွင့်အရေး အဖွဲ့အစည်းများ၊ စုံစမ်းစစ်ဆေးရေး ဂျာနယ်လစ်များ နှင့် လူ့အခွင့်အရေးအတွက် တက်ကြွလှုပ်ရှားသူများအား လုံခြုံရေးဆိုင်ရာ ဖြေရှင်းမှုများပေးသည့် (Security Solutions Provider) အဖွဲ့အစည်း ဖြစ်ပါသည်။ Qurium သည် အန္တရာယ်ကျရောက်နေသော အဖွဲ့အစည်း နှင့် လူပုဂ္ဂိုလ်များအား သီးသန့်ပံ့ပိုးမှုဖြင့် ပရော်ဖက်ရှင်နယ်စွာ စိတ်ကြိုက်ပြုပြင်နိုင်ပြီး လုံခြုံစိတ်ချရသော ဖြေရှင်းနည်းများကို ပေးဆောင်ပါသည်။ ပါဝင်သော ဝန်ဆောင်မှုများမှာ -

- အန္တရာယ်ကျရောက်နိုင်သည့် ၀က်ဘ်ဆိုက်များအား DDoS တိုက်ခိုက်မှု လျော့ချရေးဖြင့် လုံခြုံစွာ ထူထောင်ထိန်းသိမ်းခြင်း
- ခြိမ်းခြောက်မှု ချက်ချင်းခံနေရသော အဖွဲ့အစည်းများ နှင့် လူပုဂ္ဂိုုလ်များအား လျင်မြန်သော တုံ့ပြန်ပံ့ပိုးမှုပေးခြင်း
- ဝဘ်ဆိုက်ဝန်ဆောင်မှုများ နှင့် မိုဘိုင်းလ်အပ္ပလီကေးရှင်းများအား လုံခြုံရေးစစ်ဆေးခြင်း
- အင်တာနက် ပိတ်ဆို့ထားသော ဝဘ်ဆိုက်များအား သွယ်ဝိုက်၍ ချိတ်ဆက်ခြင်း
- ဒီဂျစ်တယ် တိုက်ခိုက်မှုများ၊ လိမ်လည်လှည့်စားသော အပ္ပလီကေးရှင်းများ၊ ပစ်မှတ်ထားသော သူခိုးဆော့ဝဲများ နှင့် သတင်း အချက်အလက်အတုများကို မှုခင်းစုံစမ်းစစ်ဆေးခြင်း
