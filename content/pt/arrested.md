﻿---
layout: sidebar.pug
title: "Alguém que eu conheço foi preso"
author: Peter Steudtner, Shakeeb Al-Jabri
language: pt
summary: "Um amigo, colega ou membro da família foi detido pelas forças de segurança. Você deseja limitar o impacto de sua prisão sobre eles e qualquer outra pessoa que possa estar implicada com nisso."
date: 20123-05
permalink: /pt/arrested/
parent: Home
sidebar: >
  <h3>Leia mais sobre o que fazer se alguma pessoa que você conhece for presa:</h3>

  <ul>
    <li><a href="https://coping-with-prison.org">Inspiração e orientações para pessoas encarceradas e suas famílias, advogades e apoiadores</a></li> (em inglês)
    <li><a href="https://www.newtactics.org/search/solr/arrest">Obtendo suporte para campanhas em nome da pessoa detida</a></li> (em inglês e espanhol)
  </ul>
---

# Alguém que eu conheço foi preso

Detenções de defensores de direitos humanos, jornalistas e ativistas os colocam em grande risco, e também das pessoas com quem trabalham e vivem.

Este guia é especialmente orientado para países que não respeitam os direitos humanos e os devidos processos legais, ou onde as autoridades podem, conforme interesse, contornar procedimentos legais. Ou ainda, onde atores não estatais operam livremente as detenções ou prisões representando um risco maior para as vítimas, seus colegas e parentes.

Este guia pretende aliviar os perigos enfrentados pelas ***pessoas presas*** nessas situações e limitar o acesso dos detentores a dados sensíveis que possam incriminar as vítimas e seus colegas, ou que possam ser usados para comprometer outras operações.

Cuidar da pessoa detida, assim como dos impactos digitais dessa detenção, pode ser desgastante e desafiador. Tente encontrar outras pessoas que possam te apoiar e coordenar suas ações com a comunidade envolvida.

Também é importante que você pense no cuidado com você e com outras pessoas afetadas por essa detenção:

- [cuidando do seu bem-estar psicossocial e necessidades](https://digitalfirstaid.org/pt/self-care/),
-  [cuidando de questões legais](https://digitalfirstaid.org/pt/support/)
- [obter apoio para fazer campanha em nome da pessoa detida](https://www.newtactics.org/search/solr/arrest) (em inglês e espanhol)

- [Dica: criando uma petição convincente](https://escoladeativismo.org.br/saiba-como-fazer-uma-boa-peticao-online/)

Se você se sentir tecnicamente ou emocionalmente sobrecarregade ou que (por qualquer motivo) não esteja em condições de seguir as etapas descritas abaixo, entre em contato com o suporte e a orientação das organizações listadas [aqui](https://digitalfirstaid.org/pt/support/) que oferecem, entre seus serviços, a triagem inicial.


## Faça um plano

Antes de agir nas diferentes seções descritas abaixo, siga os seguintes passos:

- Tente ler todo este guia para ter uma visão geral de todas as importantes áreas de impacto antes de agir sobre aspectos específicos. Sugerimos isso porque, cada seção vai destacar diferentes cenários de ameaças que podem se sobrepor, portanto, você precisará criar sua própria sequência de ações.
- Reserve um tempo com amigues ou com uma equipe para realizar as diferentes avaliações de risco necessárias nas diferentes seções.

<a name="harm-reduction"></a>
## Prepare-se antes de agir

Existem motivos para acreditar que essa prisão ou detenção pode levar a repercussões que afetem familiares, amigues ou colegas da pessoa detida, incluindo você?

Este guia irá te orientar através de uma série de etapas que ajudarão a fornecer soluções, buscando reduzir a exposição da pessoa detida, dos detalhes do caso e de qualquer outra pessoa envolvida.

### Dicas para uma resposta coordenada/combinada

Em todas as situações em que uma pessoa for detida, a primeira coisa a ser observada é que, muitas vezes, no calor do momento, várias pessoas amigas ou colegas reagem imediatamente, resultando em esforços duplicados ou contraditórios. Portanto, é importante lembrar que a coordenação e a ação combinada, tanto a nível local quanto internacional, são necessárias para apoiar a pessoa detida e  cuidar de todas as pessoas dentro de suas redes de apoio, família e amigues.

- Estabeleça uma equipe de resposta crítica que coordenará todas as atividades de apoio, cuidado, campanha, etc.
- Envolva os familiares, parceires, etc., tanto quanto possível (respeitando os seus limites e cuidando para não gerar mais sobrecarga).
- Tenha metas bem estabelecidas para sua campanha de apoio (e revise-as frequentemente), por exemplo, priorizar a soltura da pessoa detida para assegurar seu bem estar, ou proteger familiares e apoiadores, garantindo também o se bem estar.
- Estabeleça acordos com canais de comunicação seguros, determine a frequência e os limitações para as comunicações (por exemplo, sem comunicações entre 22h e 8h, exceto em casos de novidades sobre o caso ou emergências).
- Distribua tarefas entre as pessoas da equipe e busque apoio de ajuda externa especializada (forense, advocacy, mídia, documentação, jurídico, etc.)
- Peça por ajuda fora da equipe de resposta crítica para prover necessidades básicas para o grupo da própria equipe durante esta fase (alimentação, contabilidade, etc.).

### Cuidados envolvendo Segurança Digital

Se você tem motivos para temer repercussões para si ou outras pessoas apoiadores, antes de enfrentar qualquer emergência digital relacionada à detenção de companheires, também é crucial tomar algumas medidas preventivas com relação à segurança digital para proteger a si e a outres do perigo imediato.

- Acordar em quais canais de comunicação seguros sua rede de apoiadores irá coordenar estratégias de mitigação e comunicar sobre a detenção e sua repercussão. ***Compartilhe as permissões de administração dos grupos com váries usuáries, tanto nos grupos de mensageiros como em outras ferramentas de comunicação de grupo que estejam utilizando. Considere ainda a possibilidade de ativar a função de mensagens temporárias para ganhar uma segurança adicional.***
- Reduza o tráfego ou o armazenamento de dados em seus dispositivos ao mínimo necessário e proteja esses dados em seus dispositivos com [criptografia](https://ssd.eff.org/pt-br/module/mantendo-seus-dados-seguros).
- Crie [cópias seguras criptografadas (backups)](https://pratododia.org/pt/carne-2/#backup) de todos seus dados e mantenha essas cópias sempre em um local que não seja suscetível a casos de mandatos de busca e apreensões.
- Compartilhe as senhas de dispositivos, contas online etc. com pessoas de confiança que não estejam em perigo imediato. Gerenciadores de senhas [(como o KeePassXC)](https://ssd.eff.org/pt-br/module/como-utilizar-o-keepassxc) podem te ajudar a fazer isso de maneira segura.
- Acordar quais as medidas e primeiras ações devem ser tomadas em caso de novas detenções (como suspensões de conta, formatação de dispositivos à distância, etc.).


## Gestão de riscos
### Redução de possíveis danos causados por suas próprias ações

De uma maneira geral, tente basear suas ações na seguinte questão:

- Quais são os impactos das ações individuais e combinadas sobre a pessoa detida, bem como sobre suas comunidades, companheires ativistas, amigues, familiares, etc., incluindo você?

Cada uma das seções a seguir descreverá os aspectos especiais dessa avaliação de riscos.

As considerações básicas são:

- Antes de apagar contas, dados, postagens de redes sociais, etc., certifique-se de documentar o conteúdo e a informação que serão apagadas, especialmente se for necessário restaurar esse conteúdo ou essas informações posteriormente ou se forem necessárias como provas.
- Se apagar contas ou arquivos, esteja ciente de que:
    - As autoridades podem interpretar isso como destruição ou remoção de provas;
    - Isso poderá dificultar a situação da pessoa detida. Caso o acesso a essas informações for solicitado e as autoridades não puderem encontrá-las, a pessoa detida poderá ser considerada suspeita e ações por omissão podem ser tomadas contra ela;
- Caso você comunique às pessoas que suas informações pessoais estão sendo armazenadas em um dispositivo ou conta solicitada pelas autoridades e esta comunicação for interceptada, isso pode ser usado para implicar essas pessoas como cúmplices da pessoa detida.
- Alterações nos procedimentos de comunicação (incluindo deletar contas) pode despertar a atenção das autoridades.


### Informando contatos

Em geral, é impossível determinar se as autoridades possuem a capacidade de mapear a rede de contatos da pessoa detida e se isso já foi feito ou não. Portanto, é importante assumir o pior cenário, aquele em que as autoridades já fizeram isso ou ***eventualmente farão***.

Antes de começar a informar aos seus contatos, avalie o risco:

 - Você possui uma cópia da lista de contatos da pessoa detida? Tem acesso às listas de contatos em seus dispositivos, contas de e-mail, plataformas de redes sociais? Compile uma lista de possíveis contatos para ter uma visão geral de pessoas que podem ser afetadas. ***[Armazene](https://securityinabox.org/pt/files/secure-file-storage/) (em inglês) e [compartilhe](https://securityinabox.org/pt/communication/private-communication/) (em inglês) essa lista de contatos da maneira mais segura possível, compartilhando apenas com as pessoas que realmente "precisam saber dessa informação".***
- Existe o risco de ao informar os contatos, isso conectar essas pessoas à pessoa detida e potencialmente criminalizá-las?
- Todes devem ser informades, ou apenas um ***grupo específico*** de contatos?
- Quem será responsável por informar os contatos? Quem já está em contato com quem? Qual é o impacto desta decisão?
- Estabeleça o canal de comunicação mais seguro e considere também realizar reuniões presenciais em espaços onde não há câmeras de circuito interno, para informar os contatos implicados.


### Documentar para manter evidências

*Para mais informações sobre como guardar provas digitais, consulte o [Kit de Primeiros Socorros Digitais sobre a documentação de incidentes online](..pt/documentation).*

Antes de excluir qualquer conteúdo de sites, postagens em redes sociais, etc., certifique-se de ter documentado tudo. Uma das razões para ***documentar este conteúdo*** é manter vestígios ou provas de contas invadidas, como conteúdo modificado ou fraude de identidade, além de manter evidências jurídicas.

Dependendo do site ou plataforma de rede social que você deseja documentar, diferentes abordagens podem ser utilizadas para armazenar conteúdo digital:

- Você pode fazer capturas de tela de trechos relevantes (certifique-se de de incluir horários, URLs, etc., nas imagens).
- Verifique se os sites ou blogs relevantes estão indexados na [Wayback Machine](https://archive.org/web), ou faça o download uma ***cópia dos sites*** para a seu dispositivo local.

*Lembre-se de manter as informações que você baixou armazenadas em um dispositivo e uma localização segura*

Você pode encontrar informações mais completas sobre como armazenar provas digitais na [seção de documentação do Kit de Primeiros Socorros Digitais](..pt/documentation).

### Apreensão de dispositivos

Se quaisquer dispositivos da pessoa detida forem confiscados durante ou após a prisão, leia atentamente o tópico [***Perdi meus dispositivos***](https://digitalfirstaid.org/pt/topics/lost-device/), em particular a seção sobre [formatação remota](https://digitalfirstaid.org/pt/topics/lost-device/questions/accounts/), que inclui recomendações em caso de detenção.


### Dados e contas incriminantes

Se os ***dispositivos da pessoa detida possuírem informações que possam incriminar você ou outras pessoas, ou se houver contas e perfis conectados a esses dispositivos*** que representem um risco, pode ser necessário tentar limitar o acesso dos detentores a essas informações.

Antes de tomar qualquer ação, é importante comparar o risco causado por essas informações com o risco decorrente da insatisfação das forças de operação devido à falta de acesso a essas informações (ou o risco de uma ação judicial por destruição de evidência). Se o risco de manter essas informações for maior, remova os dados em questão e remova contas e referências a ela seguindo as instrução abaixo.

#### Suspenda ou encerre contas online

Se tiver acesso às contas que deseja fechar, você pode seguir o passo a passo ***específico de cada plataforma para encerramento de conta***. Certifique-se de ter uma cópia dos dados e conteúdos excluídos! Esteja ciente que, após fechar uma conta, o conteúdo não se tornará imediatamente inacessível: no caso do Facebook, por exemplo, pode levar até duas semanas para que o conteúdo seja excluído de todos os servidores da plataforma.

Não se esqueça de que existem diferentes opções para suspender ou encerrar contas, e cada uma delas têm impactos diferentes no conteúdo e no acesso:

- Bloqueio: Bloquear uma conta impede qualquer pessoa de iniciar uma sessão nela. Quando uma conta é bloqueada, todas as credenciais e tokens de aplicação são removidos, o que significa que não é possível iniciar sessão na conta, mas a conta se mantém ativa e qualquer conteúdo público continuará visível.

- Suspensão temporária: ao solicitar a suspensão temporária, as plataformas tornarão a conta temporariamente indisponível. Isso significa que ninguém poderá iniciar sessão na conta e que a conta também aparecerá como suspensa a qualquer pessoa que tente interagir com ela, mostrando uma mensagem de "conta suspensa" quando visualizada ou acedida.

- Cancelamento: Refere-se ao encerramento da conta, de uma forma que não é recuperável no futuro. Este é normalmente o mecanismo de contenção menos indicado, mas pode ser o único disponível para alguns serviços e plataformas. Dada a impossibilidade de recuperar a conta em questão, é necessário o consentimento do representante legal ou da família antes de solicitar o cancelamento da conta.

Se você não tiver acesso às contas da pessoa detida ou precisar de uma ação mais urgente nas contas das redes sociais, busque o apoio das organizações listadas [aqui](https://digitalfirstaid.org/pt/support/) que oferecem entre seus serviços a segurança de contas.


#### Remover contas de dispositivos

Em alguns casos, você pode querer desconectar as contas dos dispositivos, uma vez que mantê-las conectadas ***pode dar acesso a dados sensíveis a quaisquer pessoas com acesso ao dispositivo***. Para fazer isto, siga as instruções contidas no tópico ["Perdi meu dispositivo"](https://digitalfirstaid.org/pt/topics/lost-device/questions/passwords/).

Não se esqueça de remover também [contas bancárias](#online_bank_accounts) conectadas nestes dispositivos.


#### Trocar as senhas

Caso decida não suspender as contas, ainda pode ser uma boa ideia mudar todas as senhas, seguindo as instruções indicadas no tópico ["Perdi meu dispositivo"](https://digitalfirstaid.org/pt/topics/lost-device/questions/passwords/).

Considere também habilitar a [autenticação em dois fatores](https://digitalfirstaid.org/pt/topics/lost-device/questions/2fa/) para aumentar a segurança das contas da pessoa detida, seguindo as instruções presentes também no tópico "Perdi meu dispositivo".

Caso a mesma senha seja usada em contas diferentes, você precisa mudar todas as senhas das contas em que isso acontece, uma vez que elas já podem estar comprometidas.

**Outras dicas sobre troca de senhas**

- Use um [gerenciador de senha (como o KeepassXC)](https://ssd.eff.org/pt-br/module/como-utilizar-o-keepassxc) para registrar as senhas que foram trocadas e poder utilizá-las posteriormente, ou para entregá-las de volta à pessoa detida quando estiver em liberdade.
- Lembre-se de dizer à pessoa detida sobre a necessidade de troca das senhas após sua soltura, e de devolver a ela o acesso de suas contas.


#### Remoção de grupos e compartilhamentos

Se a pessoa detida estiver em qualquer grupo como, por exemplo, grupos do Facebook, WhatsApp, Signal ou Wire, ou pode acessar pastas compartilhadas online (como no Google Drive), e sua presença nesses grupos dá aos seus detentores acesso a informações privilegiadas e potencialmente perigosas, você deve remover a pessoa detida dos grupos e espaços compartilhados online.

**Instruções sobre como remover membros de grupos em diferentes serviços de mensagem:**

- [WhatsApp](https://faq.whatsapp.com/841426356990637/?locale=pt_BR&cms_platform=android)

- [IOS](https://support.apple.com/pt-br/HT211302)

- [Signal](https://support.signal.org/hc/en-us/articles/360050427692-Manage-a-group#remove)
- Telegram - A pessoa que administra o grupo pode remover participantes: abrindo o grupo> clicando no nome do grupo> na lista de participantes encontrar nome de usuárie que deve ser removide, clicar e segurar por 2 segundos> escolher a opção "Remover do grupo".
- Wire:
    - [ remover alguém de um team ou equipe](https://support.wire.com/hc/en-us/articles/115004219489-Remove-someone-from-a-team) (em inglês)
    - [remover alguém de um grupo](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation- ) (em inglês)

**Instruções de como remover compartilhamento de pastas em diferentes serviços online:**

- [Google Drive](https://support.google.com/drive/answer/2494893?hl=pt-BR&co=GENIE.Platform&sjid=12585829498478754580-SA)- busque pelos arquivos da pessoa usando o operador de busca “para:” ou “to:” (`para:usuario@gmail.com`), e então selecione todos os resultados e clique no ícone Compartilhar. Clique em Avançado, remova o endereço dela da caixa de diálogo e salve.
- [Facebook](https://www.facebook.com/help/211909018842184/)
- [Dropbox](https://www.dropbox.com/help/files-folders/unshare-folder#remove-member)
- [iCloud](https://support.apple.com/pt-br/guide/icloud/mm708256356b/icloud)




### Deletar postagens

Em alguns casos você pode preferir por remover conteúdo das timelines da pessoa detida ou de outros feeds de postagens vinculadas à sua conta, pois podem ser usados como evidência contra a pessoa ou gerar conflitos e desconfiança em relação a sua comunidade.

Alguns serviços facilitam a exclusão de conteúdo. Guias para o Twitter, Facebook e Instagram serão indicados abaixo. Por gentileza, certifique-se de ter documentado o conteúdo que você deseja deletar, caso precise dele como evidência de adulteração e etc:

- No Twitter, você pode usar [Tweet Deleter](https://tweetdeleter.com/pt/).
- Para o Facebook, você pode seguir [este passo a passo](https://teg6.com/61962/noticias/como-remover-postagens-do-facebook-do-seu-feed-sem-perder-todos-os-seus-amigos/) ou o guia  ["Como deletar seu feed de notícias do Facebook", de Louis Barclay](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6) (em inglês), baseado na extensão [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac), para Google Chrome.
- Para o Instagram, você pode seguir as instruções sobre [como editar e deletar postagens](https://help.instagram.com/997924900322403) do perfil (levando em conta que, ainda que delete as postagens, pessoas que tiverem acesso à sua conta terão acesso ao histórico destas interações).


### Apague associações negativas online

Se houver qualquer informação online que possa ter impacto negativo, piorando a situação da pessoa detida ou de seus contatos, é uma boa ideia remover essa informação para evitar danos futuros.

- Faça uma lista de lugares online e informações que precisam ser modificadas ou removidas.
- Se você identificou conteúdo que deve ser modificado ou removido, faça uma cópia antes de prosseguir com a remoção, modificação ou solicitar remoções.
- Avalie se a exclusão da conta da pessoa detida terá impacto negativo em sua situação (por exemplo, remover o nome da pessoa detida da lista de colaboradores de uma organização pode proteger a organização, mas, por outro lado pode também retirar um possível álibi quanto ao trabalho da pessoa dentro desta organização).
- Se você tem acesso às respectivas contas e sites, remova a informação ou conteúdo sensível.
 - Se não possuir acesso, solicite às pessoas com acesso que façam a remoção do conteúdo.
- Você pode encontrar instruções sobre remover conteúdo em serviços do Google [aqui](https://support.google.com/webmasters/answer/6332384?hl=pt-br#get_info_off_web)
- Verifique se os sites que possuem informações foram indexados pela ***[Wayback Machine](https://web.archive.org)*** (em inglês) ou pelo **[Google Cache](https://cachedview.com/)** (em inglês). Se sim, este conteúdo deve ser removido também.
 - ***[Como solicitar remoção de conteúdo do Wayback Machine](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)*** (em inglês)
 - ***[Remover do Google uma página hopedada no seu site](https://developers.google.com/search/docs/crawling-indexing/remove-information)***


<a name="online_bank_accounts"></a>
### Contas bancárias online

Frequentemente, contas bancárias são gerenciadas e acessadas online, e verificações pelo celular são necessárias para transferências ou mesmo acesso à conta. Se a pessoa detida não estiver acessando suas contas online por longos períodos de tempo, descuidar destes acessos pode trazer implicações financeiras no futuro. Nestes casos, certifique-se do seguinte:

- Desvincule aparelhos apreendidos das contas da pessoa detida.
- Busque autorização judicial da pessoa detida para movimentar suas contas em seu nome já nas fases iniciais do processo (em acordo com os familiares).


## Dicas finais

- Certifique-se de retornar toda a propriedade de dados de volta à pessoa após ***sua*** soltura.
- Leia [***estas*** dicas encontradas em "Perdi meu dispositivo"](https://digitalfirstaid.org/pt/topics/lost-device/) sobre como lidar com dispositivos apreendidos após sua devolução pelas autoridades.
