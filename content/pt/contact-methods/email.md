---
layout: page
title: Email
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/email.md
parent: /pt/
published: true
---

O conteúdo das suas mensagens, bem como o fato de você ter feito contato com a organização será de total conhecimento de governos e autoridades nas respectivas jurisdições.
