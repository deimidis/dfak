---
layout: page
title: "Eu perdi meus dados"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: pt
summary: "O que fazer em caso de perda de dados"
date: 2023-04
permalink: /pt/topics/lost-data
parent: /pt/
---

# Eu perdi meus dados


Dados digitais podem ser voláteis e instáveis, e você pode perdê-los de diversas maneiras. Danos físicos aos seus dispositivos, suspensão ou cancelamento de suas contas, apagamento acidental (ou não) e falhas de software no uso diário ou em atualizações são situações que podem ocasionar a perda de seus dados. Além disso, mesmo em casos nos quais haja meios de recuperá-los, você pode acabar esquecendo seus logins ou senhas (credenciais), ou não ter autonomia sobre os sistemas utilizados para fazer o backup.

Esta seção do Kit de Primeiros Socorros Digitais irá conduzir você através de passos simples para entender como você pode ter perdido dados e potenciais estratégias de mitigação e recuperação para eles.

*Nesta seção, vamos focar nos dados baseados em dispositivos. Para conteúdos online e credenciais de contas você pode seguir a [seção do Kit de Primeiros Socorros Digitais sobre questões de acesso à conta](../pt/account-access-issues).*

Siga o questionário para identificar a natureza do problema e achar possíveis soluções.

## Workflow

### entry-point

> Nesta seção focaremos em dados de dispositivos. Para conteúdo online e credenciais de acesso, direcionaremos você para outras seções do Kit de Primeiro Socorros Digitais.
>
> Dispositivos incluem computadores, celulares e tablets, HDs externos, pen drives e cartões de memória SD.

Quais tipos de dados você perdeu?

- [Conteúdo Online](../../pt/account-access-issues)
- [Credenciais](../../pt/account-access-issues)
- [Conteúdo em dispositivos](#content-on-device)

### content-on-device

> Geralmente, os dados "perdidos" foram simplesmente deletados - seja acidentalmente ou intencionalmente. No seu computador, verifique a pasta "Lixeira". Em dispositivos Android, você poderá encontrar alguns dados perdidos na pasta LOST.DIR. Se os arquivos perdidos não puderem ser localizados nas lixeiras do sistema, pode ser que não tenham sido deletados, mas movidos para outro lugar. Tente usar a função de busca do sistema operacional para localizá-los (a operação pode demorar).
>
> Tente buscar pelo nome exato do arquivo que você perdeu. Se isto não funcionar, ou se não tiver certeza do nome exato, pode tentar uma busca coringa na qual você pode buscar por um tipo de arquivo específico. Por exemplo, se perdeu um arquivo do Word mas não sabe o nome, tente buscar `*.docx`, que é a extensão do arquivo. Organize por *Data de Modificação* para localizar mais rapidamente os arquivos mais recentes ao usar este tipo de busca
>
> Para além de buscar o arquivo perdido em seu dispositivo, ***se pergunte se o arquivo pode ter sido enviado por e-mail ou compartilhado com alguém (inclusive com você mesme)*** ou enviado para armazenamento na nuvem em algum momento. Se for o caso, você pode tentar buscar em algum destes lugares e quem sabe recuperar uma cópia ou versão anterior deste arquivo.
>
> Para aumentar as chances de recuperar os arquivos, sugerimos que você pare imediatamente de editar, criar ou baixar arquivos novos no seu dispositivo. Se possível, conecte-o a outro computador (usando, por exemplo, um adaptador USB para HD externo), e utilize este outro computador para buscar e restaurar suas informações.  Criar, baixar ou editar novos arquivos (e etc) pode diminuir bastante as chances de encontrar e restaurar dados perdidos.
>
> Consulte também por arquivos e pastas escondidas, uma vez que seus dados perdidos podem estar lá. Veja como relevar estes arquivos no [Mac](https://tecnoblog.net/249854/exibir-mostrar-pastas-arquivos-ocultos/) e no [Windows](https://support.microsoft.com/pt-br/windows/exibir-pastas-e-arquivos-ocultos-no-windows-10-97fbc472-c603-9d90-91d0-1166d1d9f4b5). Para o Linux, entre no gerenciador de arquivos de sua preferência e digite as teclas de atalho Crtl + H. Há também formas de revelá-los no [Android](https://www.techtudo.com.br/dicas-e-tutoriais/noticia/2013/05/saiba-como-ocultar-arquivos-e-pastas-no-android.html) e no iOS.


Como você perdeu seus dados?

- [O dispositivo no qual os dados estavam sofreu dano físico](#tech-assistance_end)
- [O dispositivo no qual os dados estavam foi roubado/perdido](#device-lost-theft_end)
- [Os dados foram deletados](#where-is-data)
- [Os dados sumiram depois de uma atualização de sistema](#software-update)
- [O sitema ou software parou de funcionar e depois os dados sumiram](#where-is-data)


### software-update

> As vezes, quando você atualiza um software ou o sistema operacional, problemas inesperados podem acontecer, fazendo com que este software ou sistema parem de funcionar como deveriam, ou comportamentos defeituosos passem a acontecer, incluindo dados corrompidos. Se você notar uma perda de dados logo após uma atualização de software ou de sistema, é importante considerar regredir para o estágio anterior. As reversões  são úteis porque significam que o seu software ou base de dados pode ser restaurado para um estado limpo e consistente, mesmo depois de terem ocorrido operações erradas ou falhas de software
>
> É comum que sistemas baseados em software possuam meios de fazer isso, e este procedimento é chamado de rollback. Rollbacks são úteis porque significam que um software ou sistema e sua base de dados pode ser restaurada para um estado consistente antes que falhas tenham as afetado. A facilidade ou dificuldade em executar estes procedimentos depende de como este software foi construído - em alguns casos é mais simples e em outros nem tanto, e em alguns casos requer trabalho mais pesado. Você irá precisar fazer uma busca online para saber como voltar para versões mais antigas de determinados programas. Outro termo para "rollback" que você irá encontrar é "downgrading", então busque por esta alternativa também. Para sistemas operacionais como Windows ou Mac, existem métodos específicos para retornar à versões mais antigas.
>
> - Para sistemas Mac, visite a [base de conhecimento da Central de Suporte Mac](https://support.apple.com/pt-br) e use a palavra-chave "downgrade" com a sua versão de macOS para ler mais e encontrar instruções.
> - Para sistemas Windows, o procedimento varia dependendo se você quer desfazer um update específico ou fazer o rollback do sistema inteiro. Em ambos os casos, você pode encontrar instruções na [Central de Suporte Microsoft](https://support.microsoft.com), por exemplo para o Windows ***11*** você pode procurar por "Como remover uma atualização instalada" nestas [Perguntas Frequentes](https://support.microsoft.com/pt-br/help/12373/windows-update-faq).

O rollback do software ou sistema foi útil?

- [Sim](#resolved_end)
- [Não](#where-is-data)


### where-is-data

> Fazer backup regularmente - a prática de guardar seus dados importantes em pelo menos outros dois dispositivos - é ótimo e altamente recomendado. Às vezes nos esquecemos que temos backups automáticos em alguns casos, então vale a pena verificar seus dispositivos para saber se você tem essa opção habilitada, e usar este backup para restaurar seus dados. ***Caso não tenha***, planejar backups futuros é recomendado, e você deve encontrar mais dicas sobre como configurá-los nas [dicas finais](#resolved_end).

Para verificar se você tem um backup dos dados perdidos, comece se perguntando onde estes dados ficavam armazenados.

- [Em dispositivos de armazenamento externo (HDs, pen drives, cartões SD)](#storage-devices_end)
- [Em um computador](#computer)
- [Em um dispositivo móvel](#mobile)

### computer

Qual sistema operacional você está utilizando em seu computador?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Para verificar se o seu dispositivo MacOS tem uma opção de backup ativada, e utilizá-la para restaurar seus dados, verifique sua [iCloud](https://support.apple.com/pt-br/HT208682) ou [Time Machine](https://support.apple.com/pt-br/HT201250) para ver se há backups automáticos disponíveis para os seus dados.
>
> A lista de itens recentes, que registra apps, arquivos e servidores acessados nas suas últimas sessões de login, pode ser um bom lugar para buscar por estes arquivos. Para tentar reabrí-los, vá para o Menu Apple (ícone da maçã) na parte superior da tela à sua esquerda, selecione Itens Recentes e navegue pela lista de arquivos. Mais informações podem ser encontradas no artigo ***["Se você não consegue localizar um arquivo do Mac"](https://support.apple.com/pt-br/guide/mac-help/mchlp2305/mac).***

Você conseguiu localizar ou restaurar seus dados?

- [Sim](#resolved_end)
- [Não](#macos-restore)

### macos-restore

> Em algumas situações, será possível utilizar ferramentas livres e de código aberto que ajudem a encontrar o conteúdo perdido e recuperá-lo.  Em outras, suas funcionalidades serão limitadas, e as ferramentas mais conhecidas são pagas.
>
> Nestes casos, você poderá tentar usar [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) (em inglês) ou [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/pt/).

Esta informação foi útil para recuperar seus dados (parcial ou totalmente)?

- [Sim](#resolved_end)
- [Não](#tech-assistance_end)

### windows-computer

> Para verificar se você possui um backup habilitado para a sua máquina Windows, confira ***[Backup e restauração no Windows 11](https://support.microsoft.com/pt-br/help/4027408/windows-11-backup-and-restore).***
>
> A partir da atualização de Maio/2019, o Windows 10 conta com a funcionalidade  **Linha do Tempo**, cuja intenção é melhorar sua produtividade mantendo um registro de arquivos utilizados, sites navegados e outras ações feitas durante o uso do computador. Se você não conseguir se lembrar onde guardou um documento, clique no ícone da Linha do Tempo na barra de tarefas do Windows 10 para ter acesso a um registro visual organizado por data, e clique no ícone de visualização desejado para saltar até o local do arquivo. Este procedimento pode te ajudar a localizar um arquivo renomeado, por exemplo. Caso a Linha do Tempo esteja habilitada, algumas atividades do seu PC - como arquivos editados no Microsoft Office - poderão ser sincronizadas com o seu celular ou outro computador Windows onde sua conta esteja sendo utilizada. Sendo assim, você pode ter uma cópia deste arquivo perdido em um destes dispositivos. Leia mais sobre a Linha do tempo e como utilizá-la no artigo ***["Como usar a nova linha do tempo do Windows 10"](https://olhardigital.com.br/2018/04/30/noticias/como-usar-a-nova-linha-do-tempo-do-windows-10/)***.

Você conseguiu localizar ou restaurar seus dados?


- [Sim](#resolved_end)
- [Não](#windows-restore)

### windows-restore

> Em certos casos há ferramentas de código aberto que podem ajudar a encontrar conteúdo perdido e recuperá-lo. Algumas vezes o uso delas será limitado, e a maioria das ferramentas mais reconhecidas custará algum dinheiro.
>
> Exemplos de softwares que você pode utilizar incluem [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_ES) (veja [instruções passo-a-passo](https://www.cgsecurity.org/wiki/PhotoRec_Paso_A_Paso)) (em espanhol) , [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm) (em inglês) , [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/pt/), ou [Recuva](http://www.ccleaner.com/recuva) (em inglês).

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech-assistance_end)


### linux-computer

> Algumas das distribuições de Linux mais populares, como o Ubuntu, possuem ferramentas de backup já integradas, como por exemplo a [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) (em inglês). Se uma ferramenta de backup estiver integrada no sistema operacional, a configuração automática de backup pode ter sido solicitada ao iniciar o computador pela primeira vez. Procure saber se sua distribuição do Linux possui ferramenta de backup integrado, e qual o procedimento para verificar se ela está habilitada e restaurar seus dados.
>
> Leia o artigo [Como usar o Deja Dup para fazer backups no Ubuntu](https://pt.begin-it.com/7357-how-to-back-up-ubuntu-the-easy-way-with-dj-dup) para verificar como habilitar backups automáticos em seu computador. Leia o artigo ["Como fazer backup do Ubuntu de forma fácil com o Déjà Dup"](https://pt.begin-it.com/7357-how-to-back-up-ubuntu-the-easy-way-with-dj-dup) para instruções sobre como restaurar seus dados perdidos através de um backup feito anteriormente.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech-assistance_end)

### mobile

Qual o sitema operacional do seu telefone?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Você pode ter um backup automático configurado na sua conta iCloud ou no iTunes. Leia ***["Restaurar ou configurar seu dispositivo a partir de um backup do iCloud"](https://support.apple.com/pt-br/guide/icloud/mm7e756df7fd/icloud)*** para verificar se você possui backups anteriores e aprender como restaurá-los.

Você encontrou seu backup e recuperou seus dados?

- [Sim](#resolved_end)
- [Não](#phone-which-data)

###  phone-which-data

Qual das situações se aplica à perda dos seus dados?

- [Foram dados gerados por aplicativos (contatos, chats etc.)](#app-data-phone)
- [Foram dados gerados por mim (fotos, vídeos, áudios, notas etc.)](#ugd-phone)

### app-data-phone

> Um aplicativo mobile é uma aplicação de software feita para funcionar no seu dispositivo móvel (celular ou tablet). No entanto, a maior parte destes aplicativos também pode ser acessada através de um navegador de internet em seu computador. Se você sofrer uma perda de dados de aplicativo no seu celular, tente acessar o aplicativo em seu computador a partir de sua interface web (se disponível) com suas credenciais. Pode ser que seus dados estejam disponíveis através desta interface.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech-assistance_end)

### ugd-phone

> Dados gerados por usuárie são aqueles que você gera durante a utilização de certos aplicativos, e em caso de perda de dados você deve verificar se este aplicativo possui configurações de backup ativadas por padrão ou habilita formas de recuperá-lo. Por exemplo, se você usa o WhatsApp no seu celular e perdeu o histórico de conversas, ou um problema que tenha acontecido com o aparelho, você pode recuperar as conversas caso tenha habilitado as configurações de recuperação do Whatsapp. Ou se estiver usando um app para criar notas com conteúdo sensível e informações pessoais por exemplo, provavelmente terá uma opção habilitada de backup que pode não ter sido percebida durante o uso diário.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech-assistance_end)

### android-phone

> O Google tem um serviço integrado no Android, chamado Backup Service. Por padrão, este serviço guarda diversos tipos de dados e associa com o serviço do Google apropriado, que você pode também acessar pela web. Você pode olhar suas configurações de sincronização indo em Configurações > Contas > Google, e então selecionar sua conta do Gmail. Se você perder dados de alguma categoria que esteja sincronizada com sua conta Google, provavelmente terá a possibilidade de recuperá-los acessando sua conta Google através do navegador web.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#phone-which-data)


### tech-assistance_end

> Se a perda dos seus dados foi causada por dano físico ao dispositivo, como quedas no chão ou na água, danos causados por picos de energia ou similares, é provável que o que você precisa seja recuperar os dados armazenados diretamente do hardware. Se você não souber ou não se sentir confortável em executar tal operação, contate uma pessoa com conhecimentos de hardware e eletrônica de sua confiança, para que possa te auxiliar. Dependendo do contexto e da sensibilidade de seus dados, dê preferência por buscar uma pessoa, ou assistência técnica, dentro da sua rede de confiança que pratique cuidados de segurança digital, evitando lojas de assistência desconhecidas, sem referências ou de caráter duvidoso.

### device-lost-theft_end

> Em caso de dispositivos perdidos ou roubados, certifique-se de mudar todas as suas senhas assim que possível e visitar nossa ***seção sobre [o que fazer quando perder um dispositivo](../../pt/I lost my devices).***
>
> Se você integra alguma organização ou atua diretamente coma a sociedade civil e precisa de apoio para adquirir um novo dispositivo, entre em contato com uma das organizações listadas abaixo.

:[](organisations?services=equipment_replacement)


### storage-devices_end

> Para recuperar os dados que você perdeu, lembre-se que a noção de tempo é essencial. Por exemplo, recuperar um arquivo deletado acidentalmente há poucas horas atrás ou no dia anterior terá maior chance de sucesso do que um arquivo deletado há meses.
>
> Você pode considerar o uso de ferramentas de software para tentar recuperar seus dados (causados por apagamento acidental ou corrupção de dados), como por exemplo [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_ES) (para Windows, Mac, ou Linux - veja [o passo-a-passo](https://www.cgsecurity.org/wiki/PhotoRec_Paso_A_Paso)) (em espanhol), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (para Windows, Mac, Android, ou iOS) (em inglês), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/pt/) (para Windows ou Mac), ou [Recuva](http://www.ccleaner.com/recuva) (para Windows) (em inglês). Leve em conta que estas ferramentas não poderão resolver todas as situações, uma vez que seu sistema operacional pode ter escrito novos dados por cima da informação perdida. Por isso, você deve evitar usar seu computador entre o momento do apagamento dos arquivos e a tentativa de recuperação com as ferramentas acima.
>
> Para aumentar suas chances de recuperar dados, pare de usar seus dispositivos imediatamente. Continuar gravando no disco rígido irá diminuir as chances de encontrar e recuperar dados.
>
> Se nenhuma das opções acima funcionar para você, o mais provável é que você precisará recuperar estes dados diretamente do disco rígido. Se você não souber ou não se sentir confortável em executar tal operação, contate uma pessoa com conhecimentos de hardware e eletrônica de sua confiança que possa te auxiliar. Dependendo do contexto e da sensibilidade de seus dados, dê preferência a buscar uma pessoa ou assistência técnica dentro da sua rede de confiança que pratique cuidados de segurança digital, evitando lojas de assistência desconhecidas, se referências ou de caráter duvidoso.


### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Backups - É sempre uma boa ideia, na verdade um excelente prática, garantir que seus backups estejam em dia - tenha mais de uma cópia de seus dados, e em lugares diferentes! Você pode variar entre ter seus dados em serviços de nuvem, ou em dispositivos externos que não fiquem conectados à internet (HD externo, pen drive USB etc). A escolha dependerá do contexto e da sensibilidade dos seus dados.
- Em ambos os casos, é preferível que seus dados estejam sempre protegidos por criptografia. Mantenha backups extras e também cópias integrais para todos os seus dados mais importantes, certifique-se de que estejam sempre disponíveis para emergências, e sempre teste a integridade deles antes de uma atualização crítica de software ou sistema operacional.
- Crie uma estrutura de arquivos organizada. Cada pessoa tem seu próprio método de organizar dados e informações, e não há uma maneira única de fazer isso. No entanto, é essencial considerar a criação de um sistema de pastas que atenda às suas necessidades. Ao estabelecer uma estrutura consistente, você facilita sua vida, sabendo exatamente quais pastas e arquivos devem ser constantemente salvaguardados. Além disso, você terá clareza sobre onde estão armazenadas as informações mais importantes e onde deve manter dados sensíveis e pessoais, tanto seus quanto de pessoas envolvidas em sua área de atuação. Tire um momento para relaxar, respire fundo e observe sua tela. Dedique o tempo necessário para planejar e compreender o tipo de dados que você produz e utiliza em seu dia a dia. Pense em um sistema de arquivos que torne seu processo criativo e produtivo organizado e coerente. Se você não sabe por onde começar, [este guia](https://blog.trello.com/br/como-organizar-arquivos-e-pastas) pode ajudar a ter uma ideia inicial.


#### Resources

- [Access Now Helpline Community Documentation: Secure back up](https://communitydocs.accessnow.org/182-Secure_Backup.html) (em inglês)
- [Prato do Dia: Como fazer backup e quais cuidados ter](https://pratododia.org/pt/como-fazer-backup-e-quais-cuidados-ter-2/)
- [Página Oficial sobre Backup no Mac](https://support.apple.com/mac-backup) (em inglês)
- [Como fazer backup regularmente no Windows 10](https://support.microsoft.com/pt-br/help/4027408/windows-10-backup-and-restore)
- [Faça uma cópia de segurança ou restaure dados no dispositivo Android](https://support.google.com/android/answer/2819582?hl=pt)
- [Como habilitar backup automático no iPhone](https://support.apple.com/pt-br/HT203977)
- [Securily backup your data](https://johnopdenakker.com/securily-backup-your-data/) (em inglês)
- [How to Back Up Your Digital Life](https://www.wired.com/story/how-to-back-up-your-digital-life/) (em inglês)
- [Wikipedia - Recuperação de dados](https://pt.wikipedia.org/wiki/Recupera%C3%A7%C3%A3o_de_dados)
