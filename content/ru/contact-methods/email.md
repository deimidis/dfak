---
layout: page
title: Email
author: mfc
language: ru
summary: Способы связи
date: 2018-09
permalink: /ru/contact-methods/email.md
parent: /ru/
published: true
---

Содержимое ваших сообщений, как и сам факт того, что вы связывались с организацией, могут стать известны государственным учреждениям и правоохранительным органам.