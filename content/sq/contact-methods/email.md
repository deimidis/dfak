---
layout: page
title: E-maili
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/email.md
parent: /en/
published: true
---

Përmbajtja e mesazhit tuaj si dhe fakti që keni kontaktuar organizatën mund të jenë i aksesueshëm nga qeveritë ose agjencitë e zbatimit të ligjit.