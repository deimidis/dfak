---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm EET/EEST
response_time: 2 days
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine është një organizatë joqeveritare me seli në Kiev, e themeluar në vitin 2017 me misionin të mbështesë zbatimin e të drejtave të njeriut në internet, duke ndërtuar kapacitetin e OJQ-ve dhe mediave të pavarura për të adresuar shqetësimet e tyre të sigurisë digjitale dhe duke ndikuar në politikat e qeverisë dhe të kompanive në fushën e të drejtave digjitale.
