---
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7, GMT+4
response_time: 3 hours
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Media Diversity Institute - Armeni është një organizatë jofitimprurëse, joqeveritare që synon të përdorë fuqinë e mediave tradicionale, mediave sociale dhe teknologjive të reja për të mbrojtur të drejtat e njeriut, për të ndihmuar në ndërtimin e një shoqërie demokratike, civile, për t'i dhënë zë të pazëve dhe të thellojë të kuptuarit kolektiv të llojeve të ndryshme të diversitetit shoqëror.

MDI Armeni është e lidhur me Media Diversity Institute me seli në Londër, por është subjekt e pavarur.