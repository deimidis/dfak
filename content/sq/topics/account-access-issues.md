---
layout: page
title: "Nuk mund të hyj në llogarinë time"
author: RaReNet
language: sq
summary: "A keni problem të hyni në e-mail-in tuaj, rrjetet sociale ose llogarinë tuaj në ndonjë ueb-faqe? A tregon llogaria/profili ndonjë aktivitet që nuk e njihni? Ka shumë gjëra që mund të bëni për ta zbutur këtë problem."
date: 2019-03
permalink: /sq/topics/account-access-issues/
parent: Home
---

# Kam humbur qasjen në llogarinë time

Llogaritë në rrjetet sociale dhe në mediat për komunikim përdoren gjerësisht nga anëtarët e shoqërisë civile për të komunikuar, shkëmbyer njohuri dhe për t'i përfaqësuar idetë e tyre. Si pasojë, këto llogari janë në shënjestër të madhe nga aktorët dashakeq, të cilët shpesh përpiqen të komprometojnë këto llogari, duke shkaktuar dëme për anëtarët e shoqërisë civile dhe kontaktet e tyre.

Ky udhëzues shërben për t'ju ndihmuar në rast se keni humbur qasjen në njërën prej llogarive tuaja sepse është komprometuar.

Në vazhdim është një pyetësor për ta identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhje të mundshme.

## Workflow

### Password-Typo

> Ndonjëherë mund të mos jeni në gjendje të identifikoheni në llogarinë tuaj sepse ajo është bllokuar ose çaktivizuar nga platforma për shkak të shkeljeve të Kushteve të Shërbimit ose rregullave të platformës. Kjo mund të ndodhë kur llogaria juaj raportohet masivisht, ose kur mekanizmat e raportimit dhe të mbështetjes së platformës abuzohen me qëllimin për të censuruar përmbajtjen në internet.
>
> Nëse shihni një mesazh se llogaria juaj është bllokuar, kufizuar, çaktivizuar ose pezulluar dhe mendoni se ky është një gabim, ndiqni çdo mekanizëm ankimi që jepet së bashku me mesazhin. Ju mund të gjeni informacione se si të dorëzoni ankesat në lidhjet e mëposhtme:
>
> - [Facebook](https://www.facebook.com/help/185747581553788)
> - [Instagram](https://help.instagram.com/366993040048856)
> - [Twitter](https://help.twitter.com/en/forms/account-access/appeals/redirect)
> - [Youtube](https://support.google.com/youtube/answer/2802168)

A ju ndihmoi sugjerimi i mësipërm të hyni në llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#what-type-of-account-or-service)

### what-type-of-account-or-service

Në cilin lloj të llogarisë ose shërbimit e keni humbur qasjen?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
- [Tiktok](#Tiktok)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

A ka faqja administratorë të tjerë?

- [Po](#Other-admins-exist)
- [Jo](#Facebook-Page-recovery-form)

### Other-admins-exist

A kanë administratorët e tjerë problem me të njëjtën çështje?

- [Po](#Facebook-Page-recovery-form)
- [Jo](#Other-admin-can-help)

### Other-admin-can-help

> Ju lutemi kërkoni nga administratorët e tjerë që t'ju shtojnë përsëri si administrator të faqes.

A u zgjidh problemi në këtë mënyrë?

- [Po](#Fb-Page_end)
- [Jo](#account_end)

### Facebook-Page-recovery-form

> Ju lutemi, hyni në Facebook dhe përdorni [formularin e Facebook-ut për të rikuperuar faqen](https://www.facebook.com/help/contact/164405897002583). ***Nëse nuk mund të hyni në llogarinë tuaj në Facebook, ju lutemi ndiqni [rrjedhën e punës për rikuperimin e llogarisë në Facebook](#Facebook)***
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-google)
- [Jo](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

> Kontrolloni ***kutinë mbërritëse të email-it tuaj të rikuperimit për të parë*** nëse ke marrë një e-mail me titull "Critical security alert for your linked Google Account" ose SMS nga Google.
>
> Kur kontrolloni për e-maile të reja, jini gjithmonë të kujdesshëm ndaj përpjekjeve për “phishing”. Nëse nuk jeni të sigurt për legjitimitetin e një mesazhi, ju lutemi rishikoni [Rrjedhën e punës për mesazhet e dyshimta](../../../suspicious-messages/).

 A keni marrë një e-mail me titull "Critical security alert for your linked Google Account" ose SMS nga Google?

- [Po](#Email-received-google)
- [Jo](#Recovery-Form-google)

### Email-received-google

***Pasi të keni verifikuar legjitimitetin e mesazhit, rishikoni informacionin e dhënë në e-mail.*** Kontrolloni nëse ka një lidhje "rikuperoni llogarinë tuaj". A është atje?

- [Po](#Recovery-Link-Found-google)
- [Jo](#Recovery-Form-google)

### Recovery-Link-Found-google

> Ju lutemi përdorni lidhjen "rikuperoni llogarinë tuaj" për të rikuperuar llogarinë tuaj. ***Kur ndiqni lidhjen, kontrolloni dy herë që URL-ja që po vizitoni është në fakt një adresë "google.com".***

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-google)

### Recovery-Form-google

> Ju lutemi provoni [këtë formular rikuperimi për të rikuperuar llogarinë tuaj](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

A keni qasje në e-mailin/celularin të lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-yahoo)
- [Jo](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

> Kontrolloni ***kutinë mbërritëse të e-mailit tuaj të rikuperimit për të parë*** nëse keni marrë një e-mail me titull "Password change for your Yahoo account" nga Yahoo.
>
> Kur kontrolloni për e-maile të reja, jini gjithmonë të kujdesshëm ndaj përpjekjeve për “phishing”. Nëse nuk jeni të sigurt për legjitimitetin e një mesazhi, ju lutemi rishikoni [Rrjedhën e punës për mesazhet e dyshimta](../../../suspicious-messages/).

 A keni marrë një e-mail me titull "Password change for your Yahoo account" nga Yahoo?

- [Po](#Email-received-yahoo)
- [Jo](#Recovery-Form-Yahoo)

### Email-received-yahoo

***Pasi të keni verifikuar legjitimitetin e mesazhit, rishikoni informacionin e dhënë në e-mail.*** Kontrolloni nëse ka një lidhje "Rikuperoni llogarinë tuaj". A është atje?

- [Po](#Recovery-Link-Found-Yahoo)
- [Jo](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Ju lutemi përdorni lidhjen "Recover your account" ("rikuperoni llogarinë tuaj") për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [No](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Ju lutemi ndiqni [këto udhëzime për të rikuperuar llogarinë tuaj](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-Twitter)
- [Jo](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

> Kontrolloni ***kutinë mbërritëse të e-mailit tuaj të rikuperimit për të parë*** nëse keni marrë një e-mail me titull "Your Twitter password has been changed" nga Yahoo.
>
> Kur kontrolloni për e-maile të reja, jini gjithmonë të kujdesshëm ndaj përpjekjeve për “phishing”. Nëse nuk jeni të sigurt për legjitimitetin e një mesazhi, ju lutemi rishikoni [Rrjedhën e punës për mesazhet e dyshimta](../../../suspicious-messages/).

A keni marrë një e-mail me titull "Your Twitter password has been changed" nga Twitter?

- [Po](#Email-received-Twitter)
- [Jo](#Recovery-Form-Twitter)

### Email-received-Twitter

Ju lutemi kontrolloni nëse porosia përmban lidhjen "recover your account" ("rikuperoni llogarinë tuaj"). A është aty?

- [Po](#Recovery-Link-Found-Twitter)
- [Jo](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Ju lutemi përdorni lidhjen "recover your account" ("rikuperoni llogarinë tuaj") për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Nëse besoni se llogaria juaj në Twitter është komprometuar, provoni të ndiqni hapat në [Ndihmë lidhur me llogarinë time të komprometuar](https://help.twitter.com/en/safety-and-security/twitter-account-compromised).
>
> Nëse llogaria juaj nuk është komprometuar ose nëse keni probleme të tjera me aksesin në llogari, mund të ndiqni hapat në ["Kërkoni ndihmë për rivendosjen e llogarisë tuaj"](https://help.twitter.com/forms/restore).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Ju lutemi ndiqni [këto udhëzime për të rikuperuar llogarinë tuaj](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Ju lutemi vini re se nëse e rivendosni fjalëkalimin tuaj, nuk do të jeni në gjendje të lexoni e-mailet dhe kontaktet tuaja ekzistuese, pasi ato janë të koduara duke përdorur fjalëkalimin si çelës enkriptimi. Të dhënat e vjetra mund të rikuperohen nëse keni akses në një skedar rikuperimi ose frazë rikuperimi duke ndjekur hapat në [Rikupero mesazhet dhe skedarët e enkriptuar](https://proton.me/support/recover-encrypted-messages-files).

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-Hotmail)
- [Jo](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

> Kontrolloni nëse keni marrë një e-mail me titull "Microsoft account password change" nga ***Microsoft***.
>
> Kur kontrolloni për e-maile të reja, jini gjithmonë të kujdesshëm ndaj përpjekjeve për “phishing”. Nëse nuk jeni të sigurt për legjitimitetin e një mesazhi, ju lutemi rishikoni [Rrjedhën e punës për mesazhet e dyshimta](../../../suspicious-messages/).

A keni marrë një e-mail me titull "Microsoft account password change" nga ***Microsoft***?

- [Po](#Email-received-Hotmail)
- [Jo](#Recovery-Form-Hotmail)

### Email-received-Hotmail

***Pasi të keni verifikuar legjitimitetin e mesazhit, rishikoni informacionin e dhënë në e-mail.*** Kontrolloni nëse mesazhi përmban një lidhje "e Rivendosni fjalëkalimin tuaj". A është atje?

- [Po](#Recovery-Link-Found-Hotmail)
- [Jo](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Ju lutemi përdorni lidhjen "Reset your password" (rikuperoni llogarinë tuaj) për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj përmes lidhjes "Reset your password" (resetoni fjalëkalimin tuaj)?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Ju lutemi provoni ["Mjetin ndihmës të hyrjes"](https://go.microsoft.com/fwlink/?linkid=2214157). Ndiqni udhëzimet në këtë mjet, duke përfshirë shtimin e llogarisë që po përpiqeni të rikuperoni dhe përgjigjen ndaj pyetjeve në lidhje me informacionin e disponueshëm për t'u rikuperuar.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja ***përmes formularëve të internetit***. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

### Facebook

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-Facebook)
- [Jo](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

> Kontrolloni nëse keni marrë një e-mail me titull ***"Did you just reset your password?"*** nga Facebook.
>
> Kur kontrolloni për e-maile të reja, jini gjithmonë të kujdesshëm ndaj përpjekjeve për “phishing”. Nëse nuk jeni të sigurt për legjitimitetin e një mesazhi, ju lutemi rishikoni [Rrjedhën e punës për mesazhet e dyshimta](../../../suspicious-messages/).

A keni marrë një e-mail me titull ***"Did you just reset your password?"*** nga Facebook.

- [Po](#Email-received-Facebook)
- [Jo](#Recovery-Form-Facebook)

### Email-received-Facebook

Pasi të keni verifikuar legjitimitetin e mesazhit, rishikoni informacionin e dhënë në e-mail. A përmban e-maili një mesazh që thotë "This wasn't me" me një lidhje?

- [Po](#Recovery-Link-Found-Facebook)
- [Jo](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Ju lutemi përdorni lidhjen ***"This wasn't me"*** në mesazh për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj duke klikuar në lidhjen?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Ju lutemi provoni [këtë formular rikuperimi për të rikuperuar llogarinë tuaj](https://www.facebook.com/login/identify)
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja ***përmes formularëve të internetit***. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

A keni qasje në e-mailin/celularin e rikuperuar të lidhur?

- [Po](#I-have-access-to-recovery-email-Instagram)
- [Jo](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

> Kur kontrolloni për e-maile të reja, jini gjithmonë të kujdesshëm ndaj përpjekjeve për “phishing”. Nëse nuk jeni të sigurt për legjitimitetin e një mesazhi, ju lutemi rishikoni [Rrjedhën e punës për mesazhet e dyshimta](../../../suspicious-messages/).

 A keni marrë një e-mail me titull ***"Your Instagram password has been changed" nga Instagram***?

- [Po](#Email-received-Instagram)
- [Jo](#Recovery-Form-Instagram)

### Email-received-Instagram

***Pasi të keni verifikuar legjitimitetin e mesazhit, rishikoni informacionin e dhënë në e-mail.*** Kontrolloni nëse ka një lidhje "secure your account here". A është atje?

- [Po](#Recovery-Link-Found-Instagram)
- [Jo](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Ju lutemi përdorni lidhjen "***secure*** your account here” për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Ju lutemi ndiqni [këto udhëzime për të rikuperuar llogarinë tuaj](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja ***përmes formularëve të internetit***. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!--- ==================================================================
TiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktok
//================================================================== -->

### Tiktok

A keni akses në e-mailin/celularin e rikuperimit?

- [Po](#I-have-access-to-recovery-email-Tiktok)
- [Jo](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

> Nëse keni akses në e-mailin e rikuperimit, ju lutemi provoni të rivendosni fjalëkalimin tuaj duke ndjekur [Procesin e rivendosjes së fjalëkalimit të Tiktok](https://www.tiktok.com/login/email/forget-password).

A keni mundur të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

> Ju lutemi provoni të ndiqni udhëzimet në ["Llogaria ime është hakuar"](https://support.tiktok.com/en/log-in-troubleshoot/log-in/my-account-has-been-hacked) për të rikuperuar llogarinë tuaj.

A funksionoi procedura e rikuperimit?

- [Po](#resolved_end)
- [Jo](#account_end)

### Fb-Page_end

Ne jemi vërtet të kënaqur që problemi juaj është zgjidhur. Ju lutemi lexoni këto rekomandime për t'ju ndihmuar të minimizoni mundësinë e humbjes së qasjes në faqen tuaj në të ardhmen:

- Aktivizoni ***autentifikimin me 2 faktorë*** (2FA) për të gjithë administratorët në faqe.
- Caktoni role të administratorit vetëm për njerëzit që u besoni dhe që janë të përgjegjshëm.
- Nëse keni dikë të cilit mund t'i besoni, merrni parasysh të keni më shumë se një llogari administratori. Mbani në mend se duhet të aktivizoni 2FA për të gjitha llogaritë e administratorit.  
- Rishikoni rregullisht privilegjet dhe lejet në faqe. Gjithmonë caktoni nivelin minimal të privilegjit të nevojshëm që përdoruesi të kryejë punën e tij.

### account_end

Nëse procedurat e sugjeruara në këtë rrjedhë pune nuk ju kanë ndihmuar të rikuperoni qasjen në llogarinë tuaj, mund t'i drejtoheni organizatave të mëposhtme për të kërkuar ndihmë të mëtejshme:

:[](organisations?services=account)

### resolved_end

Shpresojmë që ky udhëzues i DFAK të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Ju lutemi lexoni këto rekomandime për t'ju ndihmuar të minimizoni mundësinë e humbjes së qasjes në llogaritë tuaja në të ardhmen.

- Është gjithmonë ***një ide e mirë*** të aktivizoni autentifikimin me dy faktorë (2FA) ***për llogaritë tuaja në të gjitha platformat*** që e mbështesin atë.
- Asnjëherë mos e përdorni të njëjtën fjalëkalim për më shumë se një llogari. Nëse jeni duke e bërë këtë, duhet t'i ndryshoni ato, duke përdorur një fjalëkalim unik për secilën prej llogarive tuaja.
- Përdorimi i një menaxheri të fjalëkalimeve do t'ju ndihmojë të krijoni dhe të mbani mend fjalëkalime unike, të forta për të gjitha llogaritë tuaja.
- Jini të kujdesshëm kur përdorni rrjete të hapura publike të pabesueshme Wi-Fi dhe mundësisht ***përdorni një VPN ose Tor kur lidheni përmes tyre***.
#### resources

- [Siguria në një kuti - Krijoni dhe mirëmbani fjalëkalime të forta](https://securityinabox.org/en/guide/passwords/)
- [Vetë-mbrojtje e sigurisë - Mbrojtja e vetvetes në rrjetet sociale](https://ssd.eff.org/en/module/protecting-yourself-social-networks)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Check if you received a "[Password Change Email Subject]" email from service-name. Did you receive it?

- [Yes](#Email-received-service-name)
- [No](#Recovery-Form-service-name

### Email-received-service-name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-service-name)
- [No](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
