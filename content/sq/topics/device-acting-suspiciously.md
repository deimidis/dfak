---
layout: page
title: "Pajisja ime reagon në mënyrë të dyshimtë"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, RaReNet
language: sq
summary: "Nëse kompjuteri ose telefoni juaj reagon në mënyrë të dyshimtë, mund të ketë softuer të padëshiruar ose dashakeq në pajisjen tuaj."
date: 2023-04
permalink: /sq/topics/device-acting-suspiciously
parent: Home
---

# Pajisja ime reagon në mënyrë të dyshimtë

Sulmet malware kanë evoluar dhe janë bërë shumë të sofistikuara me kalimin e viteve. Këto sulme paraqesin kërcënime të ndryshme dhe mund të kenë pasoja serioze në infrastrukturën dhe të dhënat tuaja personale dhe të organizatës.

Sulmet malware vijnë në forma të ndryshme, të tilla si viruse, phishing, ransomware, trojans dhe rootkits. Disa nga kërcënimet janë: ndalja aksidentale e kompjuterëve, vjedhja e të dhënave (gjegjësisht: kredencialet e ndjeshme të llogarive, informacionet financiare, të dhënat për hyrje në llogari bankare), një sulmues që ju shantazhon  për të paguar një shpërblim duke marrë kontrollin e pajisjes tuaj, ose duke marrë kontrollin e pajisjes tuaj dhe duke e përdorur atë për të spiunuar gjithçka që bëni në internet ose për të bërë sulme DDoS.

Disa metoda që përdoren zakonisht nga sulmuesit për t’u komprometuar juve dhe pajisjet tuaja duken si aktivitete të rregullta, si p.sh.

- Një e-mail ose një postim në rrjetet sociale që do t'ju tundojë të hapni një bashkëngjitje ose të klikoni në një lidhje.

- Shtyrja e njerëzve që të shkarkojnë dhe të instalojnë softuer nga një burim i pabesueshëm.

- Shtyrja e dikujt për të futur emrin e përdoruesit dhe fjalëkalimin e tij në një faqe interneti që është krijuar për t'u dukur legjitime, por nuk është e tillë.

- Instalimi i një aplikacioni komercial për spiunim (“spyware”) në pajisjen tuaj ndërsa e lini të pambikëqyrur dhe të shkyçur.

Ky seksion i Veglërisë së Ndihmës së Parë Digjitale do t'ju udhëzojë nëpër disa hapa bazë për të kuptuar nëse pajisja juaj ka të ngjarë të jetë e infektuar apo jo.

Nëse mendoni se kompjuteri ose pajisja juaj celulare ka filluar të reagojë në mënyrë të dyshimtë, fillimisht duhet të mendoni se cilat janë simptomat.

Simptomat që zakonisht mund të interpretohen si aktivitet i dyshimtë i pajisjes, por shpesh nuk janë arsye e mjaftueshme për t'u shqetësuar përfshijnë:

- Zhurmat e klikimit gjatë telefonatave
- Zbrazja e papritur e baterisë
- Mbinxehja kur pajisja nuk është në përdorim
- Një pajisje që funksionon ngadalë

Këto simptoma shpesh keqkuptohen si tregues të besueshëm të aktivitetit shqetësues të pajisjes. Megjithatë, secila prej tyre e marrë më vete nuk është arsye e mjaftueshme për shqetësim.

Simptomat e besueshme të një pajisjeje të komprometuar zakonisht janë:

- Pajisja riniset (ristartohet) shpesh vetvetiu
- Aplikacionet ndërpriten aksidentalisht, veçanërisht pas veprimit të futjes së të dhënave
- Përditësimet e sistemit operativ dhe/ose arnimet e sigurisë dështojnë në mënyrë të përsëritur
- Drita e treguesit të aktivitetit të kamerës është ndezur ndërsa kamera nuk është në përdorim
- Mesazhe të përsëritura ["Ekranet e kaltra të vdekjes"](https://en.wikipedia.org/wiki/Blue_Screen_of_Death) ose “kernel panics” (panik i bërthamës)
- Dritaret vezulluese
- Paralajmërimet e antivirusit

## Workflow

### start

Duke pasur parasysh informacionin e dhënë në hyrje, nëse ende mendoni se pajisja juaj mund të jetë komprometuar, udhëzuesi i mëposhtëm mund t'ju ndihmojë ta identifikoni problemin.

 - [Besoj se pajisja ime celulare reagon në mënyrë të dyshimtë](#phone-intro)
 - [Besoj se kompjuteri im reagon në mënyrë të dyshimtë](#computer-intro)
 - [Nuk mendoj më se pajisja ime mund të jetë e komprometuar](#device-clean)


### device-clean

> Shumë mirë! Megjithatë, mbani në mend se këto udhëzime do t'ju ndihmojnë të bëni vetëm një analizë të shpejtë. Megjithëse duhet të jetë e mjaftueshme për të identifikuar anomalitë e dukshme, softuerë më të sofistikuar për spiunim (spyware) mund të jenë në gjendje të fshihen në mënyrë më efektive.

Nëse ende dyshoni se pajisja mund të jetë komprometuar, mund të:

- [kërkoni ndihmë shtesë](#malware_end)
- [vazhdoni drejtpërdrejt me resetimin e pajisjes](#reset)


### phone-intro

> Është e rëndësishme të mendoni se si pajisja juaj mund të jetë komprometuar.
>
> - Sa kohë më parë keni filluar të dyshoni se pajisja juaj po reagonte në mënyrë të dyshimtë?
> - A ju kujtohet se keni klikuar në ndonjë lidhje nga burime të panjohura?
> - A keni marrë mesazhe nga palë që nuk i njihni?
> - A keni instaluar ndonjë softuer të shkarkuar nga burime të pabesueshme?
> - A ka qenë pajisja jashtë zotërimit tuaj?

Mendoni për këto pyetje me qëllim që të përpiqeni t'i identifikoni rrethanat, nëse ka të tilla, që çuan në komprometimin e pajisjes tuaj.

 - [Kam pajisje Android](#android-intro)
 - [Kam pajisje iOS](#ios-intro)


### android-intro

> Së pari kontrolloni nëse ka ndonjë aplikacion të panjohur të instaluar në pajisjen tuaj Android.
>
> Mund të gjeni një listë të aplikacioneve në seksionin "Aplikacionet" të menysë së cilësimeve. Identifikoni çdo aplikacion që nuk është instaluar paraprakisht në pajisjen tuaj dhe që nuk ju kujtohet se e keni shkarkuar.
>
> Nëse dyshoni në ndonjërin prej aplikacioneve në listë, bëni një kërkim në internet dhe shihni burimet për të parë nëse ka ndonjë raport të besueshëm që e identifikojnë aplikacionin si keqdashës.

A gjetët ndonjë aplikacion të dyshimtë?

 - [Jo, nuk gjeta](#android-unsafe-settings)
 - [Po, kam identifikuar aplikacione potencialisht keqdashëse](#android-badend)


### android-unsafe-settings

> Android u jep përdoruesve mundësinë të aktivizojnë qasjen e nivelit më të ulët në pajisjen e tyre. Kjo mund të jetë e dobishme për zhvilluesit e softuerit, por gjithashtu mund t'i ekspozojë pajisjet ndaj sulmeve shtesë. Ju duhet t'i rishikoni këto cilësime të sigurisë dhe të siguroheni që ato janë vendosur në opsionet më të sigurta. Prodhuesit mund të dërgojnë pajisje me cilësime standarde të pasigurta. Këto cilësime duhet të rishikohen edhe nëse nuk i keni bërë vetë ndryshimet.
>
> #### Aplikacione nga burime jo të besueshme
>
> Android-i normalisht e bllokon instalimin e aplikacioneve që nuk ngarkohen nga Google Play Store. Google ka procese për të shqyrtuar dhe identifikuar aplikacionet keqdashëse në Play Store. Sulmuesit shpesh përpiqen t’i shmangin këto kontrolle duke u ofruar aplikacione keqdashëse direkt përdoruesve, duke e ndarë me ta një lidhje ose një skedar instalimi. Është e rëndësishme të siguroheni që pajisja juaj të mos lejojë instalimin e aplikacioneve nga burime të pabesueshme.
>
> Sigurohuni që instalimi i aplikacioneve nga burime të panjohura është i çaktivizuar në pajisjen tuaj. Në shumë versione Android do ta gjeni këtë opsion në cilësimet e sigurisë, por mund të jetë diku tjetër, në varësi të versionit të Android-it që përdorni, ndërsa në telefonat më të rinj ky opsion mund të jetë në cilësimet e lejeve për secilin aplikacion.
>
> #### Modaliteti i zhvilluesit të aplikacioneve
>
> Android u lejon zhvilluesve të aplikacioneve t’i ekzekutojnë drejtpërdrejt komandat në sistemin ekzistues operativ, ndërsa gjenden në "Modalitetin e zhvilluesit të aplikacioneve".  Kur aktivizohet, kjo i ekspozon pajisjet ndaj sulmeve fizike. Dikush me qasje fizike në pajisje mund të përdorë Modalitetin e zhvilluesit të aplikacioneve për të shkarkuar kopje të të dhënave private nga pajisja ose për të instaluar aplikacione keqdashëse.
>
> Nëse shihni një meny “Modaliteti i zhvilluesit të aplikacioneve” në cilësimet e pajisjes tuaj, atëherë duhet të siguroheni që ai të jetë i çaktivizuar.
>
> #### Google Play Protect
>
> Shërbimi Google Play Protect ofrohet në të gjitha pajisjet e fundit Android. Ai kryen skanime të rregullta të të gjitha aplikacioneve të instaluara në pajisjen tuaj. Play Protect gjithashtu mund të heqë automatikisht çdo aplikacion të njohur keqdashës nga pajisja juaj. Aktivizimi i këtij shërbimi dërgon informacion në lidhje me pajisjen tuaj (si p.sh. aplikacionet e instaluara) te Google.
>
> Google Play Protect mund të aktivizohet nën cilësimet e sigurisë së pajisjes suaj. Më shumë informacion ofrohet në faqen e [Play Protect](https://www.android.com/play-protect/).

A keni identifikuar ndonjë cilësim të pasigurt?

- [Jo, nuk gjeta](#android-bootloader)
- [Po, kam identifikuar cilësime potencialisht të pasigurta](#android-badend)


### android-bootloader

> Android Bootloader është një program kyç që aktivizohet sapo ta ndizni pajisjen tuaj. Bootloader-i i mundëson sistemit operativ të fillojë dhe ta përdorë pajisjen. Një Bootloader i komprometuar i jep sulmuesit qasje të plotë në harduerin e pajisjes. Shumica e prodhuesve i dërgojnë pajisjet e tyre me Bootloader të mbyllur. Një mënyrë e zakonshme për të identifikuar nëse Bootloader-i zyrtar i prodhuesit është ndryshuar është të rindezni pajisjen tuaj dhe ta shihni logon e Bootloaderit. Nëse shfaqet një trekëndësh i verdhë me një pikëçuditëse, atëherë Bootloader-i origjinal është zëvendësuar. Pajisja juaj gjithashtu mund të jetë komprometuar nëse shfaq një ekran paralajmërues të Bootloader-it të zhbllokuar, e ju nuk e keni zhbllokuar atë vetë për të instaluar një ROM të personalizuar të Android-it, si p.sh. LineageOS ose GrapheneOS. Duhet ta resetoni pajisjen tuaj në cilësimet e fabrikës nëse shfaq një ekran paralajmërues të Bootloader-it të zhbllokuar që nuk e prisni.

A është komprometuar Bootloader-i apo pajisja juaj po përdor Bootloader-in origjinal?

- [Bootloader-i në pajisjen time është komprometuar](#android-badend)
- [Pajisja ime po përdor Bootloader-in origjinal](#android-goodend)


### android-goodend

> Nuk duket sikur pajisja juaj është komprometuar.

A jeni ende i shqetësuar se pajisja juaj është komprometuar?

- [Po, do të doja të kërkoja ndihmë profesionale](#malware_end)
- [Jo, i kam zgjidhur problemet e mia](#resolved_end)


### android-badend

> Pajisja juaj mund të jetë komprometuar. Një resetim në cilësimet e fabrikës ka të ngjarë të largojë çdo kërcënim që është i pranishëm në pajisjen tuaj. Megjithatë, kjo nuk është gjithmonë zgjidhja më e mirë. Përveç kësaj, ju mund të dëshironi të hetoni më tej çështjen për të identifikuar nivelin tuaj të ekspozimit dhe natyrën e saktë të sulmit që keni përjetuar.
>
> Mund të dëshironi të përdorni një mjet për vetë-diagnostikim të quajtur [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn) ose [PiRogue](https://pts-project.org/), ose të kërkoni ndihmë nga një organizatë që mund të ndihmojë.

Çfarë do të dëshironit të bënit?

- [Do të doja të resetoja pajisjen time në cilësimet e fabrikës](#rivendos).
- [Do të doja të kërkoja ndihmë profesionale](#malware_end)
- [Kam një rrjet lokal mbështetës me të cilin mund të kontaktoj për ndihmë profesionale](#resolved_end)


### ios-intro

> Ndiqni këto hapa për të [parë se kush ka qasje në iPhone-in ose iPad-in tuaj](https://support.apple.com/en-gb/guide/personal-safety/ipsb8deced49/web). Kontrolloni cilësimet e iOS për të parë nëse ka ndonjë gjë të pazakontë.
>
> Në aplikacionin Cilësimet, kontrolloni që pajisja juaj të jetë e lidhur me ID-në tuaj të Apple. Artikulli i parë i menysë në anën e majtë duhet të jetë emri juaj ose emri që përdorni për ID-në tuaj të Apple. Klikoni mbi të dhe kontrolloni që të shfaqet adresa e saktë e e-mailit. Në fund të kësaj faqeje do të shihni një listë me emrat dhe modelet e të gjitha pajisjeve iOS të lidhura me këtë Apple ID.
>
> Kontrolloni që pajisja juaj të mos jetë e lidhur me një sistem të Menaxhimit të Pajisjeve Celulare (MDM) që ju nuk e njihni. Shkoni te Settings > General > VPN & Device Management dhe kontrolloni nëse keni një seksion për Profilet. Mund të [fshini çdo profil të panjohur konfigurimi](https://support.apple.com/en-gb/guide/personal-safety/ips41ef0e8c3/1.0/web/1.0#ips68379dd2e). Nëse nuk keni profile të listuara, nuk jeni të regjistruar në MDM.

 - [Të gjitha informacionet janë të sakta dhe unë jam ende në kontroll të Apple ID-së time](#ios-goodend)
 - [Emri ose detaje të tjera janë të pasakta ose shoh pajisje ose profile në listë që nuk janë të miat](#ios-badend)


### ios-goodend

> Nuk duket sikur pajisja juaj është komprometuar.

A jeni ende i shqetësuar se pajisja juaj është komprometuar?

- [Po, do të doja të kërkoja ndihmë profesionale](#malware_end)
- [Jo, i kam zgjidhur problemet e mia](#resolved_end)


### ios-badend

> Pajisja juaj mund të jetë komprometuar. Një resetim në cilësimet e fabrikës ka të ngjarë të largojë çdo kërcënim që është i pranishëm në pajisjen tuaj. Megjithatë, kjo nuk është gjithmonë zgjidhja më e mirë. Përveç kësaj, ju mund të dëshironi të hetoni më tej çështjen për të identifikuar nivelin tuaj të ekspozimit dhe natyrën e saktë të sulmit që keni përjetuar.
>
> Mund të dëshironi të përdorni një mjet për vetë-diagnostikim të quajtur [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn) ose [PiRogue](https://pts-project.org/), ose të kërkoni ndihmë nga një organizatë që mund të ndihmojë.

Çfarë do të dëshironit të bënit?

- [Do të doja të resetoja pajisjen time në cilësimet e fabrikës](#rivendos).
- [Do të doja të kërkoja ndihmë profesionale](#malware_end)
- [Kam një rrjet lokal mbështetës me të cilin mund të kontaktoj për ndihmë profesionale](#resolved_end)


### computer-intro

> **Shënim: Në rast se jeni duke u sulmuar nga softuer që kërkon të paguani shpengim (“ransomware), shkoni drejtpërdrejt te kjo faqe [No More Ransom!](https://www.nomoreransom.org/).**
>
> Kjo sekuencë veprimesh do t'ju ndihmojë të hetoni aktivitetet e dyshimta në kompjuterin tuaj. Nëse i ndihmoni ndonjë personi nga distanca, mund të përpiqeni të ndiqni hapat e përshkruar në lidhjet e mëposhtme duke përdorur një softuer të desktopit për punë nga distanca si TeamViewer (https://www.teamviewer.com), ose mund të shikoni kornizat e reagimit ekspert nga distanca,siç është [Google Rapid Response (GRR)](https://github.com/google/grr). Mbani në mend se vonesa dhe besueshmëria e rrjetit do të jenë thelbësore për ta bërë këtë siç duhet.

Ju lutemi zgjidhni sistemin operativ që dëshironi të kontrolloni:

 - [Windows](#windows-intro)
 - [macOS](#mac-intro)


### windows-intro

> Mund të ndiqni këtë udhëzues hyrës për hetimin e aktiviteteve të dyshimta në pajisjet Windows:
>
> - [How to Live Forensic on Windows by Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

A ju ndihmuan këto udhëzime të identifikoni ndonjë aktivitet keqdashës?

 - [Po, mendoj se kompjuteri është i infektuar](#device-infected)
 - [Jo, nuk u identifikua asnjë aktivitet keqdashës](#device-clean)


### mac-intro

> Për të identifikuar një infeksion të mundshëm në një kompjuter macOS, duhet të ndiqni këto hapa:
>
> 1. Kontrolloni se a ka programe të dyshimta që fillojnë automatikisht
> 2. Kontrolloni se a ka procese aktive të dyshimta
> 3. ontrolloni se a ka zgjerime të dyshimta të bërthamës (kernelit)
>
> Faqja e internetit [Objective-See](https://objective-see.com) ofron disa shërbime pa pagesë që e lehtësojnë këtë proces:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) mund të përdoret për të identifikuar të gjitha programet që janë regjistruar për të filluar automatikisht.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) mund të përdoret për të kontrolluar proceset që ekzekutohen dhe për t'i identifikuar ato që duken të dyshimta (për shembull sepse nuk janë nënshkruar, ose për shkak se ato janë shënuar nga VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) mund të përdoret për të identifikuar çdo shtesë të dyshimtë të bërthamës (kernelit) që është ngarkuar në kompjuterin macOS.
>
> Në rast se këto nuk zbulojnë menjëherë asgjë gjë të dyshimtë dhe dëshironi të kryeni kontrolle të mëtejshme, mund të përdorni [pcQF](https://github.com/botherder/pcqf). pcQF është një mjet që thjeshton procesin e mbledhjes së disa informacioneve në sistem dhe ofron një pasqyrë të plotë të memories.
>
> Një mjet shtesë që mund të jetë i dobishëm për të mbledhur detaje të mëtejshme (por që kërkon disa njohuri me komandat e terminalit) është [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) nga kompania amerikane për siguri në internet, CrowdStrike.

A ju ndihmuan këto udhëzime të identifikoni ndonjë aktivitet keqdashës?

 - [Po, mendoj se kompjuteri është i infektuar](#device-infected)
 - [Jo, nuk u identifikua asnjë aktivitet keqdashës](#device-clean)

### device-infected

Oh jo! Pajisja juaj duket se është e infektuar. Për të hequr qafe infeksionin, mund të:

- [kërkoni ndihmë shtesë](#malware_end)
- [vazhdoni drejtpërdrejt me resetimin e pajisjes](#reset).

### reset

> Mund të konsideroni resetimin e pajisjes tuaj si një masë shtesë e kujdesit. Udhëzuesit e mëposhtëm do të japin udhëzime të përshtatshme për llojin tuaj të pajisjes:
>
> - [Android](https://support.google.com/android/answer/6088915?hl=en)
> - [iOS](https://support.apple.com/en-us/HT201252)
> - [Windows](https://support.microsoft.com/en-us/windows/reinstall-windows-d8369486-3e33-7d9c-dccc-859e2b022fc7)
> - [Mac](https://support.apple.com/en-us/HT201314)

A mendoni se keni nevojë për ndihmë shtesë?

- [Po](#malware_end)
- [Jo](#resolved_end)


### malware_end

Nëse keni nevojë për ndihmë shtesë në trajtimin e një pajisjeje të infektuar, mund të kontaktoni organizatat e shënuara më poshtë.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Shpresojmë që kjo sekuencë e veprimeve nga Veglëria e Ndihmës së Parë Digjitale të ishte e dobishme. Ju lutemi na jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Këtu janë disa këshilla për të parandaluar veten të bini pre e përpjekjeve të sulmuesve për të komprometuar pajisjet dhe të dhënat tuaja:

Kontrolloni gjithmonë dy herë legjitimitetin e çdo e-maili që merrni, e skedarit që keni shkarkuar ose të lidhjes që ju kërkon të dhënat e hyrjes në llogarinë tuaj.
- Lexoni më shumë se si të mbroni pajisjen tuaj nga infeksionet me softuer keqdashës (malware) në udhëzuesit, lidhjet e të cilave janë dhënë te burimet.

#### Resources

-Siguria në një kuti: [Mbroni pajisjen tuaj nga sulmet e softuerit keqdashës (malware) dhe vjedhja e të dhënave të ndjeshme (phishing).](https://securityinabox.org/en/phones-and-computers/malware)
  - Këshilla të mëtejshme për mbrojtjen e pajisjeve [Android](https://securityinabox.org/en/phones-and-computers/android), [iOS](https://securityinabox.org/en/phones-and-computers/ios), [Windows](https://securityinabox.org/en/phones-and-computers/windows), [MacOS](https://securityinabox.org/en/phones-and-computers/mac) dhe [Linux ](https://securityinabox.org/en/phones-and-computers/linux)
[Siguria e vetë-mbrojtjes: Si të shmangni sulmet “phishing”](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
- [Siguria pa kufij: Udhëzues për “phishing”](https://github.com/securitywithoutborders/guide-to-phishing/blob/master/SUMMARY.md)
