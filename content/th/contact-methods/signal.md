---
layout: page
title: Signal
author: mfc
language: th
summary: วิธีการติดต่อ
date: 2021-09
permalink: /th/contact-methods/signal.md
parent: /th/
published: true
---
การใช้ Signal สามารถช่วยให้เนื้อหาในข้อความของคุณเข้ารหัสไว้สำหรับองค์กรผู้รับสารเท่านั้น และมีเพียงคุณกับผู้รับสารที่จะสามารถรับรู้ได้ว่ามีการสื่อสารเกิดขึ้น โปรดตระหนักไว้ว่า Signal ใช้เบอร์โทรศัพท์ของคุณเป็นชื่อผู้ใช้ ดังนั้นคุณจะต้องแชร์เบอร์โทรศัพท์ของคุณให้กับองค์กรที่คุณตั้งใจจะติดต่อ

แหล่งข้อมูลเพิ่มเติม:
[วิธีการใช้ Signal สำหรับระบบ Android](https://ssd.eff.org/en/module/how-use-signal-android), [วิธีการใช้ Signal สำหรับระบบ iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [วิธีการใช้ Signal โดยไม่ต้องให้เบอร์โทรศัพท์](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
