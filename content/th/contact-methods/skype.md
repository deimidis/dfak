---
layout: page
title: Skype
author: mfc
language: th
summary: วิธีการติดต่อ 
date: 2021-09
permalink: /th/contact-methods/skype.md
parent: /th/
published: true
---
รัฐบาลหรือหน่วยงานผู้บังคับใช้กฎหมายอาจสามารถเข้าถึงเนื้อหาในข้อความของคุณ รวมถึงข้อมูลว่าคุณได้ติดต่อองค์กรได้ 
