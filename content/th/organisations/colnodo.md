---
name: COLNODO - Escuela de Seguridad Digital
website: https://www.escueladeseguridaddigital.co/
logo: colnodo.png
languages: English, Español
services: in_person_training, triage, org_security, web_hosting, web_protection, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, individual_care, censorship
beneficiaries: journalists, hrds, activists, lgbti, land, women, youth, cso
hours: วันจันทร์ – วันศุกร์ เวลา 8:00 – 18:00 น. UTC -5 
response_time: 1 วันทำการ
contact_methods: email, pgp, mail, phone, whatsapp, signal, telegram
email: info@escueladeseguridaddigital.co
pgp_key: "https://escueladeseguridaddigital.co/ or https://keys.openpgp.org/search?q=0x5DF922C85DF1D91D"
pgp_key_fingerprint: 6FBB 99FC CED8 CFD6 B960 169A 5DF9 22C8 5DF1 D91D
phone: +57 315 602 1376
signal: +57 315 602 1376
whatsapp: +57 315 602 1376
telegram: +57 315 602 1376
mail: Diagonal 40A 14-75, Bogotá Colombia
initial_intake: yes
---

Colnodo ให้บริการ Escuela de Seguridad Digital ซึ่งมุ่งให้ความร่วมมือทั่วไปกับองค์กรภาคประชาสังคม นักข่าว และนักเคลื่อนไหว โดยการให้ความรู้เพื่อพัฒนาปรับปรุงระเบียบการรักษาความปลอดภัยในการลดความเสี่ยงทางดิจิทัลที่ส่งผลกระทบต่อข้อมูลและการสื่อสาร
