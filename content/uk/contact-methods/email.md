---
layout: page
title: Електронна пошта
author: mfc
language: uk
summary: Способи зв'язку
date: 2018-09
permalink: /uk/contact-methods/email.md
parent: /uk/
published: true
---

Зміст вашого повідомлення та факт вашого контакту з організацією можуть стати відомими органам державної влади та правоохоронним органам.