---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: понеділок-п'ятниця, 9-17 EET/EEST
response_time: 2 дні
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Лабораторія цифрової безпеки (Digital Security Lab Ukraine) - це неурядова організація, заснована в 2017 році, штаб-квартира якої розташована в Києві. Місія організації - підтримувати реалізацію прав людини в інтернеті шляхом зміцнення потенціалу неурядових організацій та незалежних засобів масової інформації для розв'язання проблем цифрової безпеки та здійснення впливу на урядову та корпоративну політику у сфері цифрових прав.