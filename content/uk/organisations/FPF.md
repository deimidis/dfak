---
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: понеділок - п'ятниця, робочі години, східний час в США
response_time: 1 день
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Freedom of the Press Foundation (FPF) - це некомерційна неприбуткова організація, заснована в США згідно з 501(c)3, яка захищає, підтримує та зміцнює журналістику, спрямовану на захист інтересів громадськості в 21 столітті. FPF проводить тренінги для всіх - від великих медіа організацій до незалежних журналістів - із широкого спектру питань щодо інструментів та технологій безпеки й приватності для того, щоб вони краще могли захистити себе, свої джерела та свої організації.