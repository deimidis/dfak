---
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7, GMT+4
response_time: 3 години
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Media Diversity Institute - Armenia - це неприбуткова неурядова організація, яка прагне урівноважити силу традиційних засобів масової інформації, соціальних медіа та нових технологій задля захисту прав людини, сприяння в побудові демократичного громадянського суспільства, надання права голосу тим, хто його не має, та поглиблення колективного розуміння різних типів соціального різноманіття.

MDI Armenia афілійований із Media Diversity Institute, штаб-квартира якого розміщена в Лондоні, але при цьому є незалежною установою.
