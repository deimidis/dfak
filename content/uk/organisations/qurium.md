---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8-18 понеділок-неділя CET
response_time: 4 години
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation - це постачальник рішень у сфері безпеки для незалежних засобів масової інформації, правозахисних організацій, журналістів та активістів, які проводять розслідування. Qurium пропонує організаціям та окремим особам, які перебувають у групі ризику, портфель професійних, спеціалізованих та безпечних рішень із можливістю персоналізованої підтримки, до якого входять:

- Безпечний хостинг із захистом від DDoS-атак для вебсайтів із групи ризику
- Оперативна підтримка організацій та окремих осіб, яким безпосередньо загрожує небезпека
- Аудит безпеки вебсервісів та мобільних додатків
- Обхід блокування вебсайтів
- Криміналістичне розслідування цифрових атак, шахрайських додатків, цільового зловмисного програмного забезпечення та дезінформації